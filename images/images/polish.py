import tensorflow as tf
import numpy as np
import skimage.io
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import organs
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import datamanager

import logging
import itertools

#TODO refactor with gan.py
class Polisher:
    def __init__(self, scope, x, enc, adjustment, polished, cyc_enc, dis_true, dis_false):
        self.scope = scope
        self.x = x
        self.enc = x
        self.polished = polished
        self.cyc_enc = cyc_enc
        self.dis_true = dis_true
        self.dis_false = dis_false

        loss_tree = Node('loss')
        self.loss_tree = loss_tree
        style = hm.hypers['discrim_style']
        real_val = tf.zeros([]) 
        if style == 'l2':
            fake_val = tf.reduce_mean(tf.square(dis_false - 1))
        if style == 'rl2':
            ex_real= tf.reduce_mean(dis_true)
            ex_fake = tf.reduce_mean(dis_false)
            real_val = tf.reduce_mean(tf.square(dis_false - ex_real - 1))
            fake_val = tf.reduce_mean(tf.square(dis_true - ex_fake + 1))
        if style == 'ns':
            dec_f = lambda x: -1 * tf.log(tf.sigmoid(x) + 1e-8)
            fake_val = tf.reduce_mean(dec_f(dis_false))
        if style == 'rns':
            dec_f = lambda x, y:tf.sigmoid(x - tf.reduce_mean(y))
            fake_val = tf.reduce_mean(-1 * tf.log(dec_f(dis_false, dis_true) + 1e-8))
            real_val = tf.reduce_mean(-1 * tf.log(1 - dec_f(dis_true, dis_false) + 1e-8))
            

        fool = Node('fool', parent=loss_tree, val=fake_val) 
        real = Node('real', parent=loss_tree, val=real_val) 
        # pca vectors are unit, so the components have large magnitude
        cyclic = Node('cyclic', val=prim.difference_loss(enc, cyc_enc), parent=loss_tree,
                      weight=hm.hypers['polish_cyclic']) 
        polish = Node('adjustment_l2', parent=loss_tree, weight=hm.hypers['polish_l2'],
                      val=prim.difference_loss(adjustment, 0.0))
        regular = Node('regularization', parent=loss_tree, 
                       weight=hm.hypers['l2_factor'], val=prim.weight_penalty(scope))
        self.optimize_op = workout.OptimizeOp(loss_tree, scope)

class PolishChecker:
    def __init__(self, scope, dis_true, dis_false):
        self.scope = scope
        self.dis_true = dis_true
        self.dis_false = dis_false

        loss_tree = Node('loss')
        self.loss_tree = loss_tree

        style = hm.hypers['discrim_style']
        if style == 'l2':
            real_val = tf.reduce_mean(tf.square(dis_true - 1))
            fake_val = tf.reduce_mean(tf.square(dis_false))
        if style == 'rl2':
            ex_real= tf.reduce_mean(dis_true)
            ex_fake = tf.reduce_mean(dis_false)
            real_val = tf.reduce_mean(tf.square(dis_true - ex_fake - 1))
            fake_val = tf.reduce_mean(tf.square(dis_false - ex_real + 1))
        if style == 'ns':
            dec_f = lambda x: tf.sigmoid(x)
            real_val = tf.reduce_mean(-1 * tf.log(dec_f(dis_true) + 1e-8))
            fake_val = tf.reduce_mean(-1 * tf.log(1 - dec_f(dis_false) + 1e-8))
        if style == 'rns':
            dec_f = lambda x, y:tf.sigmoid(x - tf.reduce_mean(y))
            real_val = tf.reduce_mean(-1 * tf.log(dec_f(dis_true, dis_false) + 1e-8))
            fake_val = tf.reduce_mean(-1 * tf.log(1 - dec_f(dis_false, dis_true) + 1e-8))

        real = Node('real', val=real_val, parent=loss_tree, weight=0.5)
        fake = Node('fake', val=fake_val, parent=loss_tree, weight=0.5)
        regular = Node('regularization', parent=loss_tree, 
                       weight=hm.hypers['l2_factor'], val=prim.weight_penalty(scope))        

        self.optimize_op = workout.OptimizeOp(loss_tree, scope)