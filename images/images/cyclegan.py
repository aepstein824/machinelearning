import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import hyper_manager as hm
import primitives as prim
import tissue
import classification as cl
import evaluation as ev
import ml_record
import workout
import datamanager 
import image_tfr

import argparse
import sys 
import random
import time
import os
import logging
import warnings
import string

def paper_encoder(x_image, name):
    f1 = prim.hypers.get('filter_count', 32)

    encode_layers = [
        prim.conv_proto((1, 1, f1), name + "_e_filter_up", act=tf.nn.tanh, strat='act_only'),
        prim.conv_proto((7, 7, f1), name + "_e1", padding='REFLECT'), 
        prim.conv_proto((3, 3, f1 * 2), name + "_e2", stride=2),
        prim.conv_proto((3, 3, f1 * 4), name + "_e3", stride=2),
        ]

    return prim.chain_layers(x_image, encode_layers)

def paper_transcoder(x_features, name):
    f3 = prim.hypers.get('filter_count', 32) * 4
    td = prim.hypers.get('resblock_count', 6)

    transcode_layers = [
        prim.resid_path([prim.conv_proto((3, 3, f3), name + "_ta" + str(i)),
                         prim.conv_proto((3, 3, f3), name + "_tb" + str(i)),])
        for i in range(td)
        ]

    return prim.chain_layers(x_features, transcode_layers)

def paper_decoder(y_features, name):
    f1 = prim.hypers.get('filter_count', 32)

    decode_layers = [
        prim.convT_proto((3, 3, f1 * 2), name + "_d1", stride=2), 
        prim.convT_proto((3, 3, f1), name + "_d2", stride=2), 
        prim.conv_proto((7, 7, 3), name + "_d3", act=prim.hypers['image_actf'], strat='act_only'), 
        ]

    return prim.chain_layers(y_features, decode_layers)

def blind_f(candidate):
    candidate = tf.reduce_mean(candidate, axis=3, keepdims=True) 
    candidate = tf.sqrt(tissue.sobel_edges(candidate) + 1e-8)
    candidate = prim.instance_norm(candidate)
    candidate = tf.abs(candidate)
    return candidate

def paper_discriminator(candidate, name):
    f1 = prim.hypers.get('discrim_filter_count', 64)

    discrim_layers = [
        prim.conv_proto((4, 4, f1), name + "_discrim1", padding='VALID', stride=2), 
        prim.conv_proto((4, 4, f1 * 2), name + "_discrim2", padding='VALID', stride=2),
        prim.conv_proto((4, 4, f1 * 4), name + "_discrim3", padding='VALID', stride=2), 
        prim.conv_proto((4, 4, f1 * 8), name + "_discrim4", padding='VALID'), 
        prim.conv_proto((4, 4, 1), name + "_discrim_decision", padding='VALID', 
                        act=prim.hypers['decision_actf'], strat='act_only')
        ]
    return prim.chain_layers(candidate, discrim_layers)

def paper_plus_fc(candidate, name):
    f1 = prim.hypers.get('discrim_filter_count', 64)

    discrim_layers = [
        prim.conv_proto((4, 4, f1), name + "_discrim1", 
                        padding='VALID', stride=2, strat='act_only'),
        prim.conv_proto((4, 4, f1 * 2), name + "_discrim2", padding='VALID', stride=2),
        prim.conv_proto((4, 4, f1 * 4), name + "_discrim3", padding='VALID', stride=2), 
        prim.conv_proto((4, 4, f1 * 8), name + "_discrim4", padding='VALID'),
        prim.nn_proto(1, name + "_fc", act=prim.hypers['decision_actf'], strat='act_only')
        ]
    return prim.chain_layers(candidate, discrim_layers)[:, :, np.newaxis, np.newaxis] #should be an image

def simple_fc_discrim(candidate, name):
    f1 = prim.hypers.get('discrim_filter_count', 64)
    
    discrim_layers = [
        lambda x: prim.nearest_2x(x),
        prim.nn_proto(f1, name + "_fc1"),
        prim.nn_proto(f1, name + "_fc2"),
        prim.nn_proto(1, name + "_fc_out", act=prim.hypers['decision_actf'], strat='act_only'),
        ]
    return prim.chain_layers(candidate, discrim_layers)

def deep_discrim(candidate, name):
    classifier_hypers = dict(prim.hypers)
    classifier_hypers['padding'] = 'VALID'
    with tf.variable_scope(name + "_deep_resid") as deep_scope:
        deep_resid = cl.deep_residual_conv_gen(candidate, 1, classifier_hypers)
    return deep_resid[..., np.newaxis, np.newaxis]


def every_discriminator(candidate, name):
    paper = tf.reduce_mean(paper_discriminator(candidate, name + "_paper"), axis=[1,2,])
    simple_fc = simple_fc_discrim(candidate, name + "_fc")
    deep_resid = deep_discrim(candidate, name)

    stackable = [simple_fc, deep_resid]

    return tf.stack(stackable, axis=1, name='combined_discrims')[:, np.newaxis, :, :]

def build_generator(x_image, name):
    encoded = paper_encoder(x_image, name)
    transcoded = paper_transcoder(encoded, name)
    decoded = paper_decoder(transcoded, name)

    return decoded

class CycleGAN:
    def __init__(self, enc_f, trans_f, dec_f, dis_f, a_x, b_x, hypers):
        (self.enc_f, self.trans_f, self.dec_f, self.dis_f, self.a_x, self.b_x, self.hypers) = (
            enc_f, trans_f, dec_f, dis_f, a_x, b_x, hypers)

        def two_of(f, a_in, b_in, name):
            return f(a_in, name + "A"), f(b_in, name + "B")

        with tf.variable_scope("CycleGAN_autos") as self.gen_scope:
            self.gen_a, self.gen_b = two_of(build_generator, b_x, a_x, "gen")
            self.cyc_a, self.cyc_b = two_of(build_generator, self.gen_b, self.gen_a, "gen") 

        with tf.variable_scope("CycleGAN_discriminators") as self.dis_scope:
            if hypers['discrim_blind']:
                discrim_f = lambda x, name : self.dis_f(blind_f(x), name)
            else:
                discrim_f = self.dis_f
            self.dis_false_a, self.dis_false_b = two_of(discrim_f, 
                                                        self.gen_a, self.gen_b, "disc")
            self.dis_true_a, self.dis_true_b = two_of(discrim_f, a_x, b_x, "disc")
        self.gen_vars = self.gen_scope.trainable_variables()
        self.dis_vars = self.dis_scope.trainable_variables()

        self.loss_tree = Node('loss')
        self.discrim_root = Node('discrim', parent=self.loss_tree)
        
        #TODO two functions
        def g_a_node(name, parent, guess, answer, weight, diff_f): 
            child = diff_f(guess, answer)
            if isinstance(child, Node):
                holder = Node(name, parent=parent, weight=weight)
                child.parent = holder
                return child
            else:
                return Node(name, parent=parent, weight=weight, val=child)
  
        dis_loss_f = lambda guess, answer, parent: prim.difference_loss(guess, answer)
        self.dis_losses = [g_a_node(name, self.discrim_root, guess, answer,
                                    0.25, prim.difference_loss) for (name, guess, answer) in 
                           [["d_real_a", self.dis_true_a, 1], ["d_real_b", self.dis_true_b, 1],
                           ["d_fake_a", self.dis_false_a, 0], ["d_fake_b", self.dis_false_b, 0]]]
        (self.dis_step, self.dis_cycle, self.dis_clr, self.dis_op, 
         self.dis_combined, self.dis_ops, self.dis_string) = workout.optimize_op(
            self.discrim_root, self.dis_vars, self.hypers, 'discrim_')

        # maybe it should go off the generator cycle, but this is easier to code and eq for now
        #self.dis_loss_factor = tf.cast(hypers['discrim_loss_factor'] * self.dis_cycle, tf.float32)
        self.dis_loss_factor = hypers['discrim_loss_factor']
        self.gen_root = Node('gen', parent=self.loss_tree)
        self.gen_dis_root = Node('dis', weight=self.dis_loss_factor * 0.5, 
                                 parent=self.gen_root)
        self.gd_loss = [g_a_node(name, self.gen_dis_root, guess, answer, 1, prim.difference_loss)
                        for (name, guess, answer) in
                        [["g_fake_a", tf.minimum(self.dis_false_a, 0.5), 0.5], 
                         ["g_fake_b", tf.minimum(self.dis_false_b, 0.5), 0.5]]]
        self.gen_enc_root = Node('enc', parent=self.gen_root)
        self.ge_loss = [g_a_node(name, self.gen_enc_root, og, cyc, 0.5, tissue.perc_diff)
                        for (name, og, cyc) in
                        [["cyclic_a", a_x, self.cyc_a], ["cyclic_b", b_x, self.cyc_b]]]

        (self.gen_step, self.gen_cycle, self.gen_clr, self.gen_op,
         self.gen_combined, self.gen_ops, self.gen_string) = workout.optimize_op(
                self.gen_root, self.gen_vars, self.hypers)


    def training_vars(self):
        return self.gen_scope.trainable_variables() + self.dis_scope.trainable_variables()

    def persistent_vars(self):
        return self.training_vars() 

    def add_to_report(self, recorder):
        self.recorder = recorder 
        recorder.add_function("enc_f", self.enc_f) 
        recorder.add_function("trans_f", self.trans_f) 
        recorder.add_function("dec_f", self.dec_f) 
        recorder.add_function("dis_f", self.dis_f) 


def experiment(name, combined_axes):
    hypers = hm.hypers


    def setup_domain(dir_name):
        des, train_ds, val_ds, train_iter, val_iter = datamanager.mono_name(dir_name, data_shape, hypers)
        d_iter_handle = tf.placeholder(tf.string, shape=[])
        d_iter = tf.data.Iterator.from_string_handle(d_iter_handle, 
                                                     train_ds.output_types, 
                                                     train_ds.output_shapes)
        d_x = d_iter.get_next()
        d_train_handle = sess.run(train_iter.string_handle())
        d_val_handle = sess.run(val_iter.string_handle())
        return (d_x, d_iter_handle, d_train_handle, d_val_handle, train_iter, val_iter)

    a_x, a_iter_handle, a_train_handle, a_val_handle, a_train_iter, a_val_iter = setup_domain(hypers['a_dir'])
    b_x, b_iter_handle, b_train_handle, b_val_handle, b_train_iter, b_val_iter = setup_domain(hypers['b_dir'])
    current_module = sys.modules[__name__]
    discrim_f = getattr(current_module, hypers['discrim_model'])

    model = CycleGAN(paper_encoder, paper_transcoder, paper_decoder, discrim_f, 
                     a_x, b_x, hypers)

    model.add_to_report(recorder)
    record_digest = recorder.digest()
    logger = recorder.get_logger()
    recorder.setup_save_and_board(model.persistent_vars(), model.training_vars(), 
                                  sess.graph, hypers)

    train_gen_feed = { a_iter_handle:a_train_handle, b_iter_handle:b_train_handle}
    train_dis_feed = { a_iter_handle:a_train_handle, b_iter_handle:b_train_handle}
    val_both_feed = { a_iter_handle:a_val_handle, b_iter_handle:b_val_handle}

    tensor_summaries, merged_summaries = tissue.all_summaries(model.training_vars())

    exp_fig = plt.figure()
    lrs, accs = [], []
    picture_op = [a_x, model.gen_b, model.cyc_a, b_x, model.gen_a, model.cyc_b,
                  tf.image.resize_nearest_neighbor(model.dis_true_a, data_shape[:2]),
                  tf.image.resize_nearest_neighbor(model.dis_true_b, data_shape[:2]),
                  tf.image.resize_nearest_neighbor(model.dis_false_a, data_shape[:2]),
                  tf.image.resize_nearest_neighbor(model.dis_false_b, data_shape[:2]),
                  ] 

    blinded_op = [blind_f(a_x), blind_f(model.gen_b), blind_f(model.cyc_a), 
                    blind_f(b_x), blind_f(model.gen_a), blind_f(model.cyc_b)]
    if hypers['discrim_blind']:
        picture_op += blinded_op

    val_op = {'clr': model.gen_clr,
              'dis_clr': model.dis_clr,
              'cycle':model.gen_cycle,
              'loss_reports':model.gen_ops + model.dis_ops,
              'loss_combined':model.gen_combined,
              'dis_loss_combined':model.dis_combined,              
              }
    train_op = dict(val_op)
    train_op['minimize'] = [model.gen_op, model.dis_op]
    #train_op['check_numerics'] = tf.add_check_numerics_ops()

    #training
    sess.run(tf.global_variables_initializer())

    def report_f(tres, vres, i):
        workout.print_results(tres, vres, i, model.gen_string + model.dis_string, logger, hypers)
        lrs.append(reduce_res(tres, xkey))
        accs.append(reduce_res(tres, ykey))
    def picture_f(pres, i):
        img_shape = pres[0].shape[1:]
        normalized = [ev.clip_and_tri(view_img) for view_img in pres]
        wall_scrolls = [np.reshape(img, [-1, img_shape[1], img_shape[2]]) 
                        for img in normalized]
        comparison = np.concatenate(wall_scrolls, axis=1)
        name = recorder.out_dir() + ("comparison_{}_{}_{}_{}.png".format(record_digest, 
                                     hypers['a_dir'], hypers['b_dir'], i))
        return comparison, name
    if hypers['load'] is not None:
        logger.debug("Loading")
        recorder.saver.restore(sess, recorder.save_filename)
    else:
        logger.debug("\n" + RenderTree(model.loss_tree, style=anytree.render.AsciiStyle).by_attr())
        logger.debug("Training")
        sess.run(tf.global_variables_initializer())

        if not hypers['skip_sanity']:
            workout.sanity_check(
                sess, [(model.gen_combined, model.gen_vars), (model.dis_combined, model.dis_vars)],
                train_gen_feed, recorder)
        workout.train_through_cycles(model, sess, train_f, report_f, picture_f, hypers)

        logger.debug("Finished training")




    recorder.close()

if __name__ == '__main__':
    overrides = {
        'a_dir':'pokemon_ruby',
        'b_dir':'pokemon_gold',
        'max_lr':0.0002,
        'discrim_max_lr':0.0001,
        'stride':'nearest',
        'weight_rep':'unit_spectral',
        'clr_epochs':10,
        'max_cycle':5,
        'perc_loss_ssim':0.0,
        'perc_loss_l1':0.8,
        'perc_loss_grad':0.2,
        'batch_size':8,
        'train_chunk':1000,
        'val_chunk':100,
        }
    workout.run_experiments(experiment, overrides)