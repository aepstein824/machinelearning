"""Groups of primitives combined into functional units

Functions:
resid_* - create a chain of
all_summaries - collect all summaries and merge them
gcnet_proto - a brick of 3x3 convolutions
deep_fc_layers - a chain of fully connected layers
sobel_proto - create a function which calculates sobel edges
sobel_edges - calculate sobel edges
"""
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
import time
import logging
import string
import primitives as prim
import hyper_manager as hm
from anytree import Node, RenderTree

import hyper_manager as hm

def resid_squeeze(filter_out, name, width=3):
    """Build a conv spatially shrinks by 2 but doubles features, residually"""
    return lambda x_image: prim.chain_layers(x_image, [ 
        prim.resid_path(
            [prim.bottle_conv_proto((width, width, filter_out), name + '_sq', 
                                    stride=2, skip_act=True),],
            [lambda x: prim.nearest_2x(x, filters2=True)]), 
        prim.act_with_norm(req_strategy='act_only'),
        ]) 

def resid_inflate(filter_out, name, width=3):
    """Build a conv spatially grows by 2 but halves features, residually"""
    return lambda x_image: prim.chain_layers(x_image, [ 
        prim.resid_path(
            [prim.convT_proto((width, width, filter_out), name + '_inf', stride=2, 
                              act='identity'),],
            [lambda x: prim.nearest_2x(x, trans=True, filters2=True)]), 
        prim.act_with_norm(req_strategy='act_only'),
        ])

def resid_chain(filter_count, name, conv_depth, width=3):
    """Build conv_depth residual blocks chained together"""
    layers = []
    for l in string.ascii_lowercase[:conv_depth]:
        layers.append(prim.resid_path([prim.bottle_conv_proto(
            (width, width, filter_count), name + l, skip_act=True),]))
        layers.append(prim.act_with_norm(req_strategy='act_only'))
    return lambda x_image: prim.chain_layers(x_image, layers)

def all_summaries(var_collection):
    """Merge all variable summaries of the given collection"""
    tensor_summaries = []
    for v in var_collection:
        tensor_summaries.append(prim.variable_summaries(v))
    merged_summaries = tf.summary.merge_all()
    return tensor_summaries, merged_summaries

def gcnet_proto(layer_name, output_filters):
    """A brick of convolutions. Delete me tbh"""
    f1 = hm.hypers.get('filter_count', 64)
    cd = hm.hypers.get('conv_depth', 6)

    def gcnet_layer(input_tensor):
        with tf.variable_scope(layer_name, reuse=tf.AUTO_REUSE): 
            layers = [
                prim.conv_proto((5, 5, f1), 'S', stride=2), 
                ]
            for c in string.ascii_lowercase[:(cd - 1)]:
                layers.append(prim.resid_path([
                    prim.conv_proto((3, 3, f1), c + '1'), 
                    prim.conv_proto((3, 3, f1), c + '2'),
                    ]))
            layers.append(prim.conv_proto((3, 3, output_filters), 'L', strat='linear'))
            return prim.chain_layers(input_tensor, layers)

    return lambda x: gcnet_layer(x)

def deep_fc_layers(layer_name, depth, width):
    """Build a chain of fully connected layers"""
    return [prim.nn_proto(width, layer_name + str(i)) for i in range(depth)]

def sobel_proto():
    grad_filter = [-1, 0, 1]
    weight_filter = [1, 2, 1]
    sobel_r = prim.separable_constant_conv_proto(grad_filter, weight_filter)
    sobel_c = prim.separable_constant_conv_proto(weight_filter, grad_filter)
    return lambda x: (sobel_r(x), sobel_c(x))

def sobel_edges(x_image):
    sr, sc = sobel_proto()(x_image)
    return tf.square(sr) + tf.square(sc)

def blur_proto(kernel=3):
    flat = [1 / kernel] * kernel
    return prim.separable_constant_conv_proto(flat, flat)

def u_proto(down_f, up_f, enc_f, dec_f, u_levels=None):
    """Builds a u shaped network. Hypers locked in when protos are called"""
    def down(x_image):
        cd = hm.hypers['u_levels'] if u_levels is None else u_levels
        lefts = [x_image]
        middles = []
        for i in range(cd):
            lefts.append(down_f(lefts[-1], i))
            middles.append(enc_f(lefts[-1], i))
        return middles
    def up(middles):
        cd = hm.hypers['u_levels'] if u_levels is None else u_levels
        decced = []
        rights = []
        for i in range(cd - 1, -1, -1):
            decced.append(dec_f(middles[i], i))
            plus_lower = decced[-1]
            if len(rights) > 0:
                plus_lower = tf.add(rights[-1], plus_lower)
            rights.append(up_f(plus_lower, i))
        return rights[-1]

    return down, up

def perc_diff(a, b):
    """Rework me when upgrading tensorflow"""
    sobelizer = sobel_proto()
    kdiff = hm.hypers['perc_loss_l2']
    kgrad = hm.hypers['perc_loss_grad']
    sar, sac = sobelizer(a)
    sbr, sbc = sobelizer(b)
    
    root = Node('perc_diff')
    mse = Node('mse', weight=kdiff, val=prim.difference_loss(a, b), parent=root)
    if kgrad > 0:
        grad = Node('grad', weight=kgrad, parent=root)
        g_r = Node('rows', weight=0.5, val=prim.difference_loss(sar, sbr), parent=grad)
        g_c = Node('cols', weight=0.5, val=prim.difference_loss(sac, sbc), parent=grad)

    return root