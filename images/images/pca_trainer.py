import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import image_tfr
import datamanager
import researcher

import argparse
import time
import logging
import os
import logging
import warnings

overrides = {
    'batch_size':64,
    'train_chunk':64,
    'max_lr':0,
    }

def experiment(name, combined_axes):
    loads = hm.hyper_loads()

    test_name = hm.hypers['data_dir'] + '_pca'

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hm.hypers['data_dir']), data_shape)
    sess = workout.sess_growable()

    with tf.variable_scope('pca_x') as pca_scope:
        x = src.features['x']

        #recorder
        recorder = ml_record.MlRecord(custom_name = test_name)
        record_digest = recorder.digest()
        logger = recorder.get_logger()

        src.init_feeds(sess)
        k = 3
        ipca = prim.PCA(data_shape, k=k, batch_axis=2, name=test_name)
        encoded = ipca.encode(x, normed=True, elem_shape=True)
        decoded = ipca.decode(encoded, elem_shape=True, normed=True)
        v_feed = dict(src.v_feed)
        t_feed = dict(src.v_feed)
        for flag in prim.get_training_flags():
            t_feed[flag] = True

        def color_tower(img):
            return tf.reshape(img, (-1, 3))
        def covariance_matrix(tower):
            count = tf.to_float(tf.shape(tower)[0])
            mean, var = tf.nn.moments(tower, axes=[0], keep_dims=True)
            unitless = (tower - mean) / tf.sqrt(var)
            return tf.matmul(unitless, unitless, transpose_a=True) / (count - 1.0)

        pic_and_train = [x, encoded, decoded]
        stats = {'step':ipca.step, 'mean':ipca.mean, 'v':(ipca.v / ipca.v_norms()), 
                 'og_cov':covariance_matrix(color_tower(x)),
                 'ipca_cov':covariance_matrix(color_tower(encoded)),             
                 'ipca_std':tf.sqrt(tf.nn.moments(encoded, [0, 1, 2])[1]),
                 }

        recorder.setup_save_and_board(tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES), 
                                      [], sess.graph)
        sess.run(tf.global_variables_initializer())
        for i in range(int(hm.hypers['max_cycle'])):
            for j in range(hm.hypers['train_chunk']):
                vb = sess.run(pic_and_train, t_feed)
                pic, name = ev.picture_f(vb, i * hm.hypers['train_chunk'] + j)
                ev.imsave_ignore(recorder.out_dir() + name, pic)
            sb = sess.run(stats, feed_dict=v_feed)
            logger.debug('--------------')
            for key, val in sb.items():
                logger.debug('{} :\n{}\n'.format(key, val))

        if not hm.hypers['skip_save']:
            try:
                recorder.saver.save(sess, recorder.save_filename)
            except:
                logger.debug('Skipping error in save')
    logger.debug('finished training_pca')

    recorder.close()


if __name__ == '__main__':
    researcher.run_experiments(experiment, overrides)

