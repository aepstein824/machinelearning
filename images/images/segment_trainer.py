import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree, render

import primitives as prim
import tissue
import autoencoding as ae
import evaluation as ev
import segmentation
import ml_record
import workout
import image_tfr
import datamanager

import argparse
import time
import logging
import os
import logging
import warnings

def experiment(combined_axes):
    hypers = prim.hypers
  
    data_shape = (hypers['square_resolution'], hypers['square_resolution'], 3)

    des, train_ds, val_ds, train_iter, val_iter = datamanager.mono_name(
                hypers['data_dir'], data_shape)
    iter_handle = tf.placeholder(tf.string, shape=[])
    iter = tf.data.Iterator.from_string_handle(iter_handle,
        train_ds.output_types, train_ds.output_shapes)
    x = iter.get_next()

    model = segmentation.Segmentation(x)

    global_step, cycle, clr, minimize_op, loss_combined, loss_ops, loss_string = workout.optimize_op(
        model.loss_tree, model.training_vars())

    recorder = ml_record.MlRecord(hypers)
    model.add_to_report(recorder)
    #after this, no modification to the recorder
    record_digest = recorder.digest()
    logger = recorder.get_logger()

    sess = workout.sess_growable()

    train_handle = sess.run(train_iter.string_handle())
    val_handle = sess.run(val_iter.string_handle())

    recorder.setup_save_and_board(model.persistent_vars(), model.training_vars(),
                                  sess.graph)

    val_feed = model.val_dict_base()
    val_feed[iter_handle] = val_handle
    train_feed = model.train_dict_base()
    train_feed[iter_handle] = train_handle
    val_op = {'clr': clr,
              'cycle':cycle,
              'loss_reports':loss_ops,
              'loss_combined':loss_combined,
              }
    train_op = dict(val_op)
    train_op['minimize'] = minimize_op
    #train_op['check_numerics'] = tf.add_check_numerics_ops()

    exp_fig = plt.figure()
    lrs, accs = [], []

    reduce_res = lambda res, name : np.mean([r[name] for r in res], axis=0)
    xkey = 'clr' if hypers['finding_lr'] else 'cycle'
    ykey = 'loss_combined'
    def report_f(tres, vres, i):
        workout.print_results(tres, vres, i, loss_string, logger)
        lrs.append(reduce_res(tres, xkey))
        accs.append(reduce_res(tres, ykey))
    def picture_f(pres, i):
        pictures = ev.clip_and_tri(pres) 
        new_shape = [-1, pictures.shape[2], pictures.shape[3]] 
        comparison = np.reshape(pictures, new_shape)
        name = recorder.out_dir() + "{}_segmentation_{}.png".format(hypers["data_dir"], i)
        return comparison, name
    def train_f():
        return workout.train_one_chunk(sess, train_op, train_feed, val_op, val_feed, 
                                       model.picture)

    if hypers['load'] is not None:
        logger.debug("Loading")
        recorder.saver.restore(sess, recorder.save_filename)
    else:
        logger.debug("\n" + RenderTree(model.loss_tree, style=render.AsciiStyle).by_attr()) 
        logger.debug("Training")
        sess.run(tf.global_variables_initializer())

        if not hypers['skip_sanity']:
            workout.sanity_check(
                sess, [(loss_combined,model.training_vars()),],
                train_feed, recorder)
        workout.train_through_cycles(model, sess, train_f, report_f, picture_f)

        logger.debug("Finished training")

    exp_axes = exp_fig.add_subplot(111)

    def label_axes(axes):
        axes.set_ylabel(ykey)
        axes.set_xlabel(xkey)
        old_lim = axes.get_ylim()[1]
        new_lim = 3 * np.min(accs) + 0.01
        axes.set_ylim(0, np.maximum(old_lim, new_lim))

    exp_axes.set_ylim(0, .00001)
    label_axes(exp_axes)
    label_axes(combined_axes) # will be called repeatedly, shouldn't matter
    plot_f = plt.Axes.semilogx if hypers['finding_lr'] or hypers['discrim_finding_lr'] else plt.Axes.plot
    plot_f(exp_axes, lrs, accs, label=record_digest)
    plot_f(combined_axes, lrs, accs, label=record_digest)
    exp_axes.legend()

    exp_fig.savefig(recorder.out_dir() + "graph_{}x{}_{}_{}.png" 
                    .format(hypers["a_dir"], hypers["b_dir"], xkey, record_digest))


    recorder.close()

if __name__ == '__main__':
    overrides = {
        'data_dir':'the_legend_of_zelda_oracle_of_seasons',
        'batch_size':16,
        'train_chunk':5000,
        'val_chunk':500,
        }
    workout.run_experiments(experiment, overrides)