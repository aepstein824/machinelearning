import primitives as prim
import tissue
import evaluation as ev
import ml_record
import hyper_manager as hm
import workout
import organs

import tensorflow as tf
import numpy as np
import skimage.io
from anytree import Node, RenderTree

import itertools
import string
import collections

Stereo = collections.namedtuple('Stereo', ['l', 'r'])

class DepthPerception:
    def __init__(self, scope, x, disp, recon, loop, conf):
        self.x = x
        self.disp = disp
        self.recon = recon
        self.loop = loop

        loss_tree = Node('loss')        
        conf_totals = Stereo(tf.reduce_mean(conf.l, axis=[1, 2], keepdims=True),
                             tf.reduce_mean(conf.r, axis=[1, 2], keepdims=True))
        conf_scale = lambda x, i: (x * conf[i] / conf_totals[i])

        def g_a_node(name, parent, i, weight, diff_f): 
            child = diff_f(conf_scale(recon[i], i), conf_scale(x[i], i))
            # does the diff_f return a tree node?
            if isinstance(child, Node):
                holder = Node(name, parent=parent, weight=weight)
                child.parent = holder
                return child
            else:
                return Node(name, parent=parent, weight=weight, val=child)

        krecon = hm.hypers['depth_loss_recon']
        kloop = hm.hypers['depth_loss_loop']
        ksmooth = hm.hypers['depth_loss_dsmooth']
        kmdh = hm.hypers['depth_loss_mdh']
    
        perc_node = Node('perception', parent=loss_tree)
        if loop:
            self.perception_losses = [g_a_node(n, perc_node, i, w, tissue.perc_diff) 
                                      for (n, i, w) in [
                                      ['recon_l', 0, krecon * 0.5], 
                                      ['recon_r', 1, krecon * 0.5],
                                      ['loop_l', 0, kloop * 0.5],
                                      ['loop_r', 1, kloop * 0.5], ]]
        else:
            recon_weight = (kloop + krecon) * 0.5
            self.perception_losses = [g_a_node(n, perc_node, i, w, tissue.perc_diff) 
                                      for (n, i, w) in [
                                      ['recon_l', 0, recon_weight], 
                                      ['recon_r', 1, recon_weight], ]]

        max_disparity = hm.hypers['max_d']
        scaled_disp = Stereo(self.disp.l / max_disparity, self.disp.r / max_disparity)
        smooth_node = Node('smoothness', parent=loss_tree, weight=ksmooth)
        self.smooth_losses = [Node(n, parent=smooth_node, weight=0.5,
                                   val=tf.reduce_mean(tissue.sobel_edges(
                                       conf_scale(self.disp[i], i)))) 
                              for (n, i) in [('l', 0), ('r', 1)]]

        #because of the way the disparity is calculated, it cannot be negative
        mdh_node = Node('mdh', parent=loss_tree, weight=kmdh)
        self.mdh_losses = [Node(n, parent=mdh_node, weight=0.5, 
                                val=tf.reduce_mean(conf_scale(self.disp[i], i)))
                           for (n, i) in [('l', 0), ('r', 1)]]

        l2_factor = hm.hypers['l2_factor']
        regular = Node('regularization', parent=loss_tree, 
                       weight=l2_factor, val=prim.weight_penalty(scope))
        self.optimize_op = workout.OptimizeOp(loss_tree, scope)
        self.scope = scope
        video_scaled_disp = (disp.l / hm.hypers['max_d'])[...,tf.newaxis]
        self.video_op = [x.l, video_scaled_disp]

def stereo_model(x_image, extract_layers, disparity_layers):
    max_disparity = hm.hypers['max_d']
    xl, xr = x_image

    features_l = prim.chain_layers(xl, extract_layers) 
    features_r = prim.chain_layers(xr, extract_layers) 
    print("f shape", features_l.get_shape())

    # NHWC -> [ NHWC ] x D
    def slanted_stacks(base_l, base_r, stack_depth):
        maps_l = []
        maps_r = []
        width = base_l.get_shape()[2]
        # pad along horizontal dimension
        for d in range(stack_depth):
            padding = [[0, 0], [0, 0], [d, 0], [0, 0]]
            maps_l.append(tf.pad(base_r[:, :, :width - d, :], padding))
            padding = [[0, 0], [0, 0], [0, d], [0, 0]]
            maps_r.append(tf.pad(base_l[:, :, d:, :], padding))
        return maps_l, maps_r

    # not downsampled
    stack_l, stack_r = slanted_stacks(features_l, features_r, max_disparity)
    #concat along feature dimension
    maps_l = [tf.concat([features_l, d], axis=3) for d in stack_l]
    maps_r = [tf.concat([features_r, d], axis=3) for d in stack_r]

    #NHWDF, feature dimension moves from 3->4
    cv_l = tf.stack(list(maps_l), axis=3)
    cv_r = tf.stack(list(maps_r), axis=3)
    print("cv shape", cv_l.get_shape())

    #NHWD
    dv_l = prim.chain_layers(cv_l, disparity_layers)
    dv_r = prim.chain_layers(cv_r, disparity_layers)
    print("dv shape", dv_l.get_shape())

    def soft_argmax(dv):
        pv = dv * -1
        softmax = tf.nn.softmax(pv)
        lin_range = np.array(range(max_disparity), dtype=np.dtype('float32'))
        lin_factor = lin_range[:, np.newaxis]
        weighted = tf.multiply(lin_range, softmax, name="weighting_sm")
        return softmax, tf.reduce_sum(weighted, axis=3)

    p_l, d_l = soft_argmax(dv_l)
    p_r, d_r = soft_argmax(dv_r)
    print("p shape", p_l.get_shape())
    print("d shape", d_l.get_shape())

    stack_l, stack_r = slanted_stacks(xl, xr, max_disparity)
    # now, to combine NHWD with NHWDC
    weighted_l = tf.multiply(tf.stack(stack_l, 3), p_l[:, :, :, :, tf.newaxis], name="mul_rl")
    weighted_r = tf.multiply(tf.stack(stack_r, 3), p_r[:, :, :, :, tf.newaxis], name="mul_rr")
    recon_l = tf.reduce_sum(weighted_l, axis=3)
    recon_r = tf.reduce_sum(weighted_r, axis=3)
    print("recon shape", recon_l.get_shape())

    stack_l, stack_r = slanted_stacks(recon_l, recon_r, max_disparity)
    weighted_l = tf.multiply(tf.stack(stack_l, 3), p_l[:, :, :, :, tf.newaxis], name="mul_ll")
    weighted_r = tf.multiply(tf.stack(stack_r, 3), p_r[:, :, :, :, tf.newaxis], name="mul_lr")
    loop_l = tf.reduce_sum(weighted_l, axis=3)
    loop_r = tf.reduce_sum(weighted_r, axis=3)
    print("loop shape", loop_l.get_shape())

    return Stereo(d_l, d_r), Stereo(recon_l, recon_r), Stereo(loop_l, loop_r), Stereo(1, 1)

def simple_conv_gen(xl, xr, hypers):
    simple_filters = hypers.get('f1', 64)
    trainers = prim.declare_trainers()
    extract_layers = [
        tissue.gcnet_proto(hypers, "extractor", simple_filters, trainers['bn'])
        ]
    disparity_layers = [
        prim.conv3_proto((1, 1, 1, simple_filters), "dfeature_up"), trainers['bn'],
        prim.resid_path([prim.conv3_proto((3, 3, 3, simple_filters), "dA1"), trainers['bn'],
                         prim.conv3_proto((3, 3, 3, simple_filters), "dA2"), trainers['bn'],]),
        prim.resid_path([prim.conv3_proto((3, 3, 3, simple_filters), "dB1"), trainers['bn'],
                         prim.conv3_proto((3, 3, 3, simple_filters), "dB2"), trainers['bn'],]),        
        prim.resid_path([prim.conv3_proto((3, 3, 3, simple_filters), "dC1"), trainers['bn'],
                         prim.conv3_proto((3, 3, 3, simple_filters), "dC2"), trainers['bn'],]),
        prim.convT3_proto((3, 3, 3, simple_filters//2), "dup", stride=2), trainers['bn'],
        prim.convT3_proto((3, 3, 3, 1), "dv", act=tf.identity),
        lambda x: tf.squeeze(x, axis=[4]),
        ]
    return stereo_model(xl, xr, hypers, extract_layers, disparity_layers) + (trainers,)

def paper_conv(x_image):
    tdm_filters = hm.hypers['filter_count']
    u_levels = hm.hypers['u_levels']
    # The paper doesn't down sample  in the initial feature extraction, so they have a stride
    # of 1 at the end of the tdm. For now, I downsample in gcnet proto and upsample at the
    # end of the tdm.
    extract_layers = [
        tissue.gcnet_proto('extractor', tdm_filters)
        ]

    def tdm_helper(filters, depth):
        L = string.ascii_letters[depth]
        if depth == 0:
            return lambda x: tf.identity(x, "tdm_base")
        shortcut = [
            prim.conv3_proto((3, 3, 3, filters), "short1" + L),
            prim.conv3_proto((3, 3, 3, filters), "short2" + L),
            ]        
        longcut = [
            prim.conv3_proto((3, 3, 3, filters * 2), "ll" + L, stride=2),
            tdm_helper(filters * 2, depth - 1),
            prim.convT3_proto((3, 3, 3, filters), "td" + L, stride=2),
            ]
        return prim.resid_path(longcut, shortcut)

    # the input is tdm_filters * 2, but that shouldn't matter
    tdm_layers = [
        tdm_helper(tdm_filters, u_levels),
        prim.convT3_proto((3, 3, 3, 1), "dup", stride=2, strat='linear'),
        lambda x: tf.squeeze(x, axis=[4]),
        ]

    return stereo_model(x_image, extract_layers, tdm_layers) 

def bottles(x_image):
    tdm_filters = hm.hypers['filter_count']
    u_levels = hm.hypers['u_levels']

    extract_layers = [ 
        organs.u_extract_proto('extractor', tdm_filters),
        ]

    def tdm_helper(filters, depth):
        L = string.ascii_letters[depth]
        if depth == 0:
            return lambda x: tf.identity(x, "tdm_base")
        shortcut = [
            prim.conv3_proto((1, 1, 1, filters // 4), "short_down_" + L),
            prim.conv3_proto((5, 5, 7, filters // 4), "short_around_" + L),
            prim.conv3_proto((1, 1, 1, filters), "short_up_" + L),
            ]        
        longcut = [
            prim.conv3_proto((3, 3, 7, filters * 2), "ll" + L, stride=2),
            tdm_helper(filters * 2, depth - 1),
            prim.convT3_proto((3, 3, 7, filters), "td" + L, stride=2),
            ]
        return prim.resid_path(longcut, shortcut)

    # Since the left and right feature maps are concatenated in the filter dimension, a grouped
    #  convolution might process them independently. First, let's combine with an ungrouped one.
    tdm_layers = [
        prim.convT3_proto((3, 3, 3, tdm_filters), 'combine_lr', strat='act_only', groups=1),
        tdm_helper(tdm_filters, u_levels),
        prim.convT3_proto((3, 3, 7, 1), "dup", strat='linear'),
        lambda x: tf.squeeze(x, axis=[4]),
        ]

    return stereo_model(x_image, extract_layers, tdm_layers) 

def third_eye_model(x_image, extract_layers, disparity_layers):
    max_d = hm.hypers['max_d']
    xl, xr = x_image
    width = int(xl.get_shape()[2])

    features_l = prim.chain_layers(xl, extract_layers) 
    features_r = prim.chain_layers(xr, extract_layers) 
    print("f shape", features_l.get_shape())

    combined = tf.concat([features_l, xl, features_r, xr], axis=3, name='gattai')
    disparity_layers.append(lambda x: tf.nn.softmax(x))
    disparity = prim.chain_layers(combined, disparity_layers) #NHWD
    print("d shape", disparity.get_shape())

    #Add black bars to both sids
    no_pad = [0, 0]
    thick_padding = [no_pad, no_pad, [max_d, max_d], no_pad]
    disparity_thick = tf.pad(disparity, thick_padding, name='disp_cinema_bars')

    def convolve(move_dir, other_og):
        other_color = tf.pad(other_og, thick_padding, name='cinema_bars')
        color = 0.0
        disp = 0.0
        conf = 0.0
        for i in range(max_d): 
            c_start = max_d + move_dir * i
            d_start = max_d + move_dir * i // 2
            proposed_color = other_color[..., c_start:c_start+width, :]
            prob = disparity_thick[..., d_start:d_start+width, i:i+1]
            old_prob = 1 - prob
            color = prob * proposed_color + old_prob * color
            disp =  prob * i              + old_prob * disp
            conf =  prob                  + old_prob * conf
        color = color / (conf + 1e-5)
        disp = disp / (conf + 1e-5)
        return color, disp, conf

    recon_l, disp_l, conf_l = convolve(-1, xr)
    recon_r, disp_r, conf_r = convolve(1, xl)
    print("recon shape", recon_l.get_shape())
    print("disp shape", disp_l.get_shape())
    print("conf shape", conf_l.get_shape())

    #TODO fewer outputs, regularize more efficiently
    return Stereo(disp_l, disp_r), Stereo(recon_l, recon_r), None, Stereo(conf_l, conf_r)

def cyclops_bottles(x_image):
    filter_count = hm.hypers['filter_count']
    max_d = hm.hypers['max_d']

    extract_layers = [organs.u_extract_proto('extractor', filter_count)]
    disparity_layers = [organs.u_extract_proto('disparity', max_d, act='identity')]

    return third_eye_model(x_image, extract_layers, disparity_layers)


def eyepatch_model(x_image, disparity_layers):
    max_d = hm.hypers['max_d']
    xl, xr = x_image
    width = int(xl.get_shape()[2])

    disparity_layers.append(lambda x: tf.nn.softmax(x))
    disp_l = prim.chain_layers(xl, disparity_layers) 
    disp_r = prim.chain_layers(xr, disparity_layers) 
    print("d shape", disp_l.get_shape())

    #Add black bars to both sids
    no_pad = [0, 0]
    thick_padding = [no_pad, no_pad, [max_d, max_d], no_pad]

    def convolve(move_dir, other_og, other_disp):
        other_color = tf.pad(other_og, thick_padding, name='cinema_bars')
        other_d_thick = tf.pad(other_disp, thick_padding, name='cinema_bars')
        color = 0.0
        disp = 0.0
        conf = 0.0
        for i in range(max_d): 
            c_start = max_d + move_dir * i
            d_start = c_start
            proposed_color = other_color[..., c_start:c_start+width, :]
            prob = other_d_thick[..., d_start:d_start+width, i:i+1]
            old_prob = 1 - prob
            color = prob * proposed_color + old_prob * color
            disp =  prob * i              + old_prob * disp
            conf =  prob                  + old_prob * conf
        color = color / (conf + 1e-5)
        disp = disp / (conf + 1e-5)
        return color, disp, conf

    recon_l, d_prediction_l, conf_l = convolve(-1, xr, disp_r)
    recon_r, d_prediction_r, conf_r = convolve(1, xl, disp_l)
    print("recon shape", recon_l.get_shape())
    print("conf shape", conf_l.get_shape())
    print("disp_prediction shape", d_prediction_l.get_shape())

    #TODO fewer outputs, regularize more efficiently
    return (Stereo(d_prediction_l, d_prediction_r), Stereo(recon_l, recon_r), 
            None, Stereo(conf_l, conf_r))

def eyepatch_bottles(x_image):
    max_d = hm.hypers['max_d']

    disparity_layers = [organs.u_extract_proto('extractor', max_d)]

    return eyepatch_model(x_image, disparity_layers)
     







