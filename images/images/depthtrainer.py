import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import autoencoding as ae
import evaluation as ev
import depth_perception as dp
import hyper_manager as hm
import ml_record
import workout
import image_tfr
import datamanager
import researcher

import argparse
import time
import logging
import os
import logging
import warnings

should_load = False 

overrides = {
    'data_dir':'sbs128',
    'model':'bottles',
    # dp takes a long time and has small batches, so lower train chunk and higher clr_epochs
    #  means normal artwork and training quanitity
    'train_chunk':1000, 
    'conv_groups':2,
    'l2_factor':1e-8,
    }

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    my = workout.my_fn_generator(name)

    print("Remember to check sizes")

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    x = dp.Stereo(src.features['x'], src.features['xr'])

    if 'depth' in loads:
        depth_hypers = ml_record.load_hard_hypers(loads['depth']) 
    else:
        depth_hypers = hypers
    with tf.variable_scope('depth') as depth_scope, hm.global_hyper_push(depth_hypers):
        model_class = getattr(dp, hm.hypers['model'])
        disp, recon, loop, conf = model_class(x)
    model = dp.DepthPerception(depth_scope, x, disp, recon, loop, conf)
 
    w = workout.Workout(my('depth'), model, combined_axes)

    def depth_side(depth):
        side = np.zeros([hypers['batch_size'], data_shape[0], hypers['max_d']])
        for i in range(hypers['max_d']):
            distance = np.minimum(1.0, 1.0 / ((i - depth) ** 2 + 0.001))
            side[:, :, i] = np.mean(distance, axis=2)
        return side

    # When hm.hypers['max_d'] is above the fast majority, it's impossible to see.
    # One quarter screen is a reasonable value
    scaled_disp = (disp.l / (hm.hypers['square_resolution'] / 4))
    scaled_disp_r = (disp.r / (hm.hypers['square_resolution'] / 4))
    picture_op = [scaled_disp, scaled_disp * x.l, x.l, recon.l, x.r, recon.r, scaled_disp_r,
                  conf.l * x.l, conf.r * x.r, ] 
    def picture_f(pres, i):
        return ev.picture_f(pres, i)


    w.sess.run(tf.global_variables_initializer())
    if 'depth' in loads:
        w.load('depth')

    w.train_through_cycles(src, picture_op, picture_f)

if __name__ == '__main__':
    researcher.run_experiments(experiment, overrides)