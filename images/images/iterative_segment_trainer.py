import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import image_tfr
import datamanager
import researcher

import argparse
import time
import logging
import os
import logging
import warnings

class IterativeSegmenter:
    def __init__(self, scope, loss_tree):
        self.scope = scope

        l2_factor = hm.hypers['l2_factor']
        regular = Node('regularization', parent=loss_tree, 
                       weight=l2_factor, val=prim.weight_penalty(scope))

        self.optimize_op = workout.OptimizeOp(loss_tree, scope)

def mask_from_accuracy(x, dec, old_mask):
    difference = tf.stop_gradient(x - dec)
    squared_diff = tf.reduce_sum(tf.sqrt(1e-5 + tf.abs(difference)), axis=3, keepdims=True)
    squared_diff = tissue.blur_proto(hm.hypers['segment_blur'])(squared_diff)
    mean_axis = [1, 2, 3] if hm.hypers['segment_instance'] else None
    threshold = tf.reduce_mean(squared_diff, axis=mean_axis, keepdims=True)
    #get the real mean without all of the masked out pixels
    threshold /= tf.reduce_mean(old_mask, axis=mean_axis, keepdims=True)
    mask = tf.where(squared_diff < threshold, tf.ones_like(squared_diff), 
                        tf.zeros_like(squared_diff))
    return mask

def divide_segment(x, mask, loss_tree, picture_op, final_layers, depth=0):
    model_func = getattr(ae, hm.hypers['model'])
    x_mask = tf.concat([x, mask], axis=3)
    enc_f, dec_f = model_func(x_mask)
    enc = enc_f(x_mask)
    dec = dec_f(enc)
    graded_x, graded_dec = x * mask, dec * mask
    perc_node = tissue.perc_diff(graded_x, graded_dec)
    perc_node.parent = loss_tree
    picture_op.append(mask)
    picture_op.append(dec * (mask * 0.5 + 0.5))
    if depth < hm.hypers['segment_divides']:
        new_mask = mask_from_accuracy(graded_x, graded_dec, mask)
        new_easy = new_mask * mask
        new_hard = (1 - new_mask) * mask

        with tf.variable_scope('e'):
            divide_segment(x, new_easy, Node('e', parent=loss_tree), 
                           picture_op, final_layers, depth + 1)
        with tf.variable_scope('h'):
            divide_segment(x, new_hard, Node('h', parent=loss_tree),
                           picture_op, final_layers, depth + 1)
    else:
        final_layers.append((mask, dec))

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    my = workout.my_fn_generator(name)

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    x = src.features['x']
    
    sess = workout.sess_growable()

    #model and loss creation
    if 'seg' in loads:
        seg_hypers = ml_record.load_hard_hypers(loads['seg']) 
    else:
        seg_hypers = hypers
    picture_op = []
    final_layers = []
    with tf.variable_scope('seg') as seg_scope, hm.global_hyper_push(seg_hypers):
        starter_mask = tf.ones([hm.hypers['batch_size']] + data_shape[:-1] + [1])
        loss_tree = Node('loss')
        divide_segment(x, starter_mask, loss_tree, picture_op, final_layers) 
        final_recreation = tf.add_n([p * m for p, m in final_layers])
        final_segments = tf.add_n([m * tf.reduce_mean(x * m, axis=[1, 2], keepdims=True) 
                                   / tf.reduce_mean(m, axis=[1,2], keepdims=True) 
                                   for m, p in final_layers])
        picture_op.append(final_recreation)
        picture_op.append(final_segments)
        Node('segmented_recreation', parent=loss_tree, weight=None, 
             val=prim.difference_loss(x, final_recreation))
        picture_op.append(x)

    model = IterativeSegmenter(seg_scope, loss_tree)

    w = workout.Workout(my('seg'), model, combined_axes, sess)

    if 'seg' in loads:
        w.load('seg')
    else:
        w.sess.run(tf.global_variables_initializer())
    w.train_through_cycles(src, picture_op, ev.picture_f)
    w.recorder.close()


overrides = {
    #'data_dir':'the_legend_of_zelda_oracle_of_seasons',
    'data_dir':'cifar10',
    'square_resolution':32,
    'train_chunk':5120, #chunks for loz tas
    'val_chunk':512,
    'model':'deep_conv_pca',
    }
if __name__ == '__main__':
    researcher.run_experiments(experiment, overrides)