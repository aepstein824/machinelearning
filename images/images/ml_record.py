import tensorflow as tf
import tensorboard
import numpy as np

import primitives as prim
import hyper_manager as hm

import json
import hashlib
import logging
import sys 
import os

board_form = '{}/out/{}/tensorboard/'
log_form = '{}/out/{}/logs/'
check_form = '{}/out/{}/checkpoints/'
out_form = '{}/out/{}/images/'
sanity_form = '{}/out/{}/sanity/'
json_form = '{}/out/{}/hypers.json'
msg_form = '%(asctime)s - %(name)s - %(message)s'
time_form = "%Y-%m-%d %H:%M:%S"

class MlRecord:
    """Hold the settings for one run"""
    def __init__(self, custom_name=None):
        self.settings = { }
        self.should_summarize = False
        self.add_hypers()
        self.custom_name = custom_name

    def add_function(self, func_key, func_val):
        self.settings[func_key] = func_val.__name__

    def add_value(self, key, value):
        self.settings[key] = value

    def add_hypers(self):
        self.settings.update(hm.hypers)

    def __hash__(self):
        return hash(self.settings)

    def contents(self):
        return json.dumps(self.settings)

    def digest(self):
        if self.custom_name is not None:
            return self.custom_name
        contents = self.contents()
        hasher = hashlib.shake_128()
        hasher.update(contents.encode())
        cont_hash = hasher.hexdigest(4)
        return cont_hash

    def export(self):
        digest = self.digest()
        fname = json_form.format(hm.hypers['out_dir'], digest)
        with open(fname, 'w') as f:
            f.write(self.contents())

    def get_logger(self):
        record_digest = self.digest()
        logger_name = record_digest
        if 'workout_name' in self.settings:
            # I'm terminally afraid of spaces, but this will make it easier to read on the 
            # command line. It only affects chooseing loggers and printing.
            logger_name = ' '.join([self.settings['workout_name'], logger_name])
            

        if not hasattr(self, 'logger'):
            out_name = out_form.format(hm.hypers['out_dir'], record_digest)
            os.makedirs(out_name, exist_ok=True)
            sanity_name = sanity_form.format(hm.hypers['out_dir'], record_digest)
            os.makedirs(sanity_name, exist_ok=True)
            self.logger = logging.getLogger(logger_name)
            self.logger.setLevel("DEBUG")
            log_formatter = logging.Formatter(msg_form, time_form)
            log_dir = log_form.format(hm.hypers['out_dir'], record_digest)
            os.makedirs(log_dir, exist_ok=True)
            log_name = log_dir + '{}.txt'.format(record_digest)
            self.file_handler = logging.FileHandler(log_name)
            self.file_handler.setLevel(logging.DEBUG)
            self.file_handler.setFormatter(log_formatter)
            self.logger.addHandler(self.file_handler)
            console_handler = logging.StreamHandler(sys.stdout)
            console_handler.setLevel(logging.DEBUG)
            console_handler.setFormatter(log_formatter)
            self.logger.addHandler(console_handler)
            self.logger.info(self.contents())

        return self.logger

    def setup_save_and_board(self, persistent_vars, training_vars, graph):
        record_digest = self.digest()
        logger = self.get_logger()

        real_count_vars = lambda vlist: np.sum([np.prod(v.get_shape().as_list()) for v in vlist])
        training_count = real_count_vars(training_vars)
        logger.debug("Training Variables: {}".format(int(training_count)))

        if len(persistent_vars) > 0:
            self.saver = tf.train.Saver(persistent_vars)
            self.save_filename = check_form.format(hm.hypers['out_dir'], record_digest)
        if hm.hypers['should_summarize']:
            self.board_dir = board_form.format(hm.hypers['out_dir'], record_digest)
            os.makedirs(self.board_dir, exist_ok=True)
            self.should_summarize = True
            self.board_writer = tf.summary.FileWriter(self.board_dir, graph)
        self.export()

    def close(self):
        #TODO consider context manager paradigm
        self.file_handler.close()
        if self.should_summarize:
            self.board_writer.close()

    #TODO this recomputation business is silly
    def out_dir(self):
        return out_form.format(hm.hypers['out_dir'], self.digest())
    def sanity_dir(self):
        return sanity_form.format(hm.hypers['out_dir'], self.digest())

def load_hard_hypers(digest):
    fname = json_form.format(hm.hypers['out_dir'], digest)
    with open(fname, 'r') as f:
        contents = json.load(f)
    new_hard = dict(hm.hypers)
    for k in hm.hard_keys:
        if k in contents:
            new_hard[k] = contents[k]
    return new_hard



