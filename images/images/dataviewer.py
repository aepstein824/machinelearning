import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import image_tfr
import datamanager
import researcher

import argparse
import time
import logging
import os
import logging
import warnings

overrides = {
    'batch_size':64,
    'train_chunk':64,
    'max_lr':0,
    }

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()

    test_name = hypers['data_dir']

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    sess = workout.sess_growable()

    #recorder
    recorder = ml_record.MlRecord()

    def upsample(x):
        return prim.nearest_2x(x, True)
    def downsample(x):
        return prim.nearest_2x(x, False)
    def sobel(x):
        return tissue.sobel_edges(x)
    def reflect_padding(x):
        # dims are 7,7 for visibility, channels don't matter
        return prim.pad_for_conv(x, [1, 1, 1, 1], [hypers['batch_size'],] + data_shape, 
                                 (7, 7, 0), False, 'REFLECT')[0]
    def symmetric_padding(x):
        # dims are 7,7 for visibility, channels don't matter
        # when striding, the output should be smaller
        return prim.pad_for_conv(x, [1, 2, 2, 1], (hypers['batch_size'],) + (64, 64, 3), 
                                 (7, 7, 0), False, 'SYMMETRIC')[0]
    def instance_norm_sanity_check(x):
        for i in range(10):
            x = prim.instance_norm(x)
        return x
    def pca_encode(x):
        sparse = hm.hypers['sparse']
        in_shape, is3d, in_channel_i = prim.dims_2or3(x)
        params = np.prod(in_shape[1:])
        shaped = tf.reshape(x, [-1, params])
        s, u, v = tf.svd(shaped, compute_uv=True)
        sigma_diag = tf.matrix_diag(s)
        cut_sigma = sigma_diag[..., :sparse]
        cut_v = v[..., :sparse]
        pcs = tf.matmul(u, cut_sigma)
        recon_features = tf.matmul(pcs, cut_v, transpose_b=True)
        recon_features = tf.Print(recon_features, [tf.shape(s), tf.shape(u), tf.shape(v)])
        recon_features = tf.Print(recon_features, [tf.shape(cut_sigma), tf.shape(cut_v)])
        recon_features = tf.Print(recon_features, [tf.shape(pcs), tf.shape(recon_features)])
        in_shape[0] = -1
        recon_img = tf.reshape(recon_features, in_shape)
        return recon_img 

    transformations = [tf.identity, sobel]
    
    for i, t in enumerate(transformations):
        recorder.add_function('f' + str(i), t)
    #after this, no modification to the recorder
    record_digest = recorder.digest()
    logger = recorder.get_logger()

    transformed = [t(src.features['x']) for t in transformations]

    src.init_feeds(sess)
    view_batch = sess.run(transformed, src.v_feed)
    for i, v in enumerate(view_batch):
        clipped = ev.clip_and_tri(v)
        tower = np.reshape(clipped, [-1, clipped.shape[2], 3])
        # remember not to change the size
        squareish = ev.tower_to_squareish(tower, hypers['batch_size'])
        name = '{}t{}.png'.format(recorder.out_dir(), i)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            skimage.io.imsave(name, squareish)

    #compares = [view_batch[0]]
    #compares.append(ev.hoz_adapt(view_batch[0], hm.hypers['sparse']))
    #comparison, name = ev.picture_f(compares, len(transformations))
    #with warnings.catch_warnings():
    #    name = '{}t{}.png'.format(recorder.out_dir(), name)
    #    warnings.simplefilter("ignore")
    #    print (name)
    #    skimage.io.imsave(name, comparison)

    #PCA test
    k = 3
    testICPA = prim.PCA(data_shape, k=k, batch_axis=2, name='test')
    encoded = testICPA.encode(src.features['x'], normed=True, always_train=True, elem_shape=True)
    #shape_for_print = lambda x:tf.reshape(x, [-1] + data_shape)
    shape_for_print = ev.expand_scalars_pile
    decoded = testICPA.decode(encoded, elem_shape=True, normed=True)
    feed = dict(src.v_feed)
    for flag in prim.get_training_flags():
        feed[flag] = True
    op = [encoded, #0
          testICPA.mean, 
          decoded, 
          testICPA.step, 
          testICPA.v, 
          testICPA.n_factor, #5
          src.features['x'], 
          tf.sqrt(tf.nn.moments(src.features['x'], [0, 1, 2])[1]), 
          tf.sqrt(tf.nn.moments(encoded, [0, 1, 2])[1]), 
          tf.sqrt(tf.nn.moments(decoded, [0, 1, 2])[1]), 
          ]
    comp_op = [shape_for_print(tf.expand_dims(testICPA.v[i], 0)) for i in range(k)]
    tf.summary.tensor_summary('v', testICPA.v)
    merged_summaries = tf.summary.merge_all()
    recorder.setup_save_and_board([], [], sess.graph)
    sess.run(tf.global_variables_initializer())
    for i in range(int(hm.hypers['max_cycle'])):
        for j in range(hm.hypers['train_chunk']):
            vb = sess.run(op, feed)
        comparison, name = ev.picture_f([vb[6], vb[2], vb[0]], i)
        skimage.io.imsave(recorder.out_dir() + name, comparison)
        comps = sess.run(comp_op, feed)
        comparison, name = ev.picture_f(comps, 2 * i + 1)
        comparison = ev.rescale_unit(comparison)
        skimage.io.imsave(recorder.out_dir() + 'components{}.png'.format(i), comparison)        
        logger.debug('Iter: {}, Step {}, N {}\nMean {}\nV:\n{}\nStdX:{}\nStdE:{}\nStdD:{}\n'.format(
            i, vb[3], vb[5], vb[1], vb[4][:4], vb[7], vb[8], vb[9]))
        summary = sess.run(merged_summaries, feed)
          
    logger.debug('finished viewing')
    recorder.close()

if __name__ == '__main__':
    researcher.run_experiments(experiment, overrides)