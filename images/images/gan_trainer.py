import tensorflow as tf
import numpy as np
import skimage.io
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import organs
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import datamanager
import gan 
import researcher

import logging
import itertools

overrides = {
    'square_resolution':32,
    'sparse':128,
    'model':'resid32',
    'train_chunk':5120, #chunks for loz tas
    'val_chunk':512,
    'optimizer':'Adam',
    'actf':'leaky_relu',
    }

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    my = workout.my_fn_generator(name)

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    x = src.features['x']
    sess = workout.sess_growable()

    #model and loss creation
    if 'gen' in loads:
        gen_hypers = ml_record.load_hard_hypers(loads['gen']) 
    else:
        gen_hypers = dict(hypers)
    gen_hypers['testing_cap'] = False

    with tf.variable_scope('pca_x') as pca_scope, hm.global_hyper_push(gen_hypers):
        if hm.hypers['pca_x']:
            pca_name = hm.hypers['data_dir'] + '_pca'
            pca = prim.PCA(data_shape, k=3, batch_axis=2, name=pca_name)
            x = pca.encode(x, elem_shape=True)
            pca_vars = tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, pca_scope.name)
            pca_saver = tf.train.Saver(pca_vars) 

    with tf.variable_scope('gen') as gen_scope, hm.global_hyper_push(gen_hypers):
        latent = tf.random_normal([hm.hypers['batch_size'], 1, 1, hm.hypers['sparse']])
        gen_func = getattr(gan, hypers['model'] + '_gen')
        generated = gen_func(latent)
    if 'dis' in loads:
        dis_hypers = ml_record.load_hard_hypers(loads['dis']) 
    else:
        dis_hypers = dict(hypers)
    dis_updates = {}
    for k, v in dis_hypers.items():
        if 'discrim_' in k and v:
            normal_k = k[len('discrim_'):]
            if normal_k in dis_hypers:
                dis_updates[normal_k] = v
    dis_hypers.update(dis_updates)

    with tf.variable_scope('dis') as dis_scope, hm.global_hyper_push(dis_hypers):
        dis_func = getattr(gan, hypers['model'] + '_dis')
        noise_f = lambda: hm.hypers['discrim_noise'] * tf.truncated_normal([hm.hypers['batch_size']] 
                                                                    + data_shape)
        real = dis_func(x + noise_f())
        fake = dis_func(generated + noise_f())
        dis_train_model = gan.Discriminator(dis_scope, real, fake)
        dw = workout.Workout('dis_train', dis_train_model, combined_axes, sess)
    with tf.variable_scope('gen') as gen_scope, hm.global_hyper_push(gen_hypers):
        gen_train_model = gan.Generator(gen_scope, real, fake)
        gw = workout.Workout('gen_train', gen_train_model, combined_axes, sess)

    if hm.hypers['pca_x']:
        d_picture_op = [x, pca.decode(x), ev.expand_scalars_pile(real), 
                        generated, pca.decode(generated), ev.expand_scalars_pile(fake),]
        g_picture_op = [generated, pca.decode(generated), 
                        ev.expand_scalars_pile(tf.squeeze(latent))]
    else:
        d_picture_op = [x, ev.expand_scalars_pile(real), 
                        generated, ev.expand_scalars_pile(fake),]
        g_picture_op = [generated, ]
 

    sess.run(tf.global_variables_initializer())
    if hm.hypers['pca_x']:
        pca_saver.restore(sess, ml_record.check_form.format(hm.hypers['out_dir'], pca_name))
    if 'dis' in loads:
        #TODO do NOT push hypers. Instead check for agreement.
        dw.load('dis')
    if 'gen' in loads:
        #TODO do NOT push hypers. Instead check for agreement.
        gw.load('gen')

    inter_iter = itertools.zip_longest(dw.train_cycle_iter(src, d_picture_op, ev.picture_f),
                                       gw.train_cycle_iter(src, g_picture_op, ev.picture_f),)
    [None for i in inter_iter]

    dw.recorder.close()
    gw.recorder.close()


if __name__ == '__main__':

    researcher.run_experiments(experiment, overrides)