"""Train a network.

Classes:
Workout - Handles coordination of training, testing, and reporting.
OptimizeOp - An optimizer with cyclical LR

Functions:
loss_tree_to_ops - Reduce losses to total, ops, format_string
triangular_rate - Build a cylical learning rate from hypers
sess_growable - Get a tf session with growable GPU memory
my_fn_generator - Return a function which appends a name if there is one
"""

import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
import anytree
from anytree import Node, RenderTree

import primitives as prim
import hyper_manager as hm
import tissue
import autoencoding as ae
import evaluation as ev
import ml_record
import workout

import time
import logging
import warnings
import os
import subprocess
import logging
import warnings
import string

class Workout:
    """Owns training one model, and reporting the results.

    Hyperparamters (for all methods):
    Everything lr related, *_chunk, sanity_*
    """
    def __init__(self, name, model, combined_axes, sess=None):
        self.combined_axes = combined_axes #should be called regimen
        self.model = model
        if sess is None:
            self.sess = sess_growable()
        else:
            self.sess = sess

        self.recorder = ml_record.MlRecord()
        self.recorder.add_value('workout_name', name)
        # locks in the name
        self.logger = self.recorder.get_logger()
        self.record_digest = self.recorder.digest()
        if not name:
            self.name = self.record_digest
        else:
            self.name = '_'.join([name, self.record_digest])
        persistent_vars = (self.model.optimize_op.training_vars
                           + tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES,
                                               self.model.optimize_op.train_scope.name))
        self.recorder.setup_save_and_board(persistent_vars, 
                                           self.model.optimize_op.training_vars, self.sess.graph)
        self.xkey = 'clr' if hm.hypers['finding_lr'] else 'cycle'
        self.ykey = 'loss_combined'
        self.prime_chunk = hm.hypers['prime_chunk']

    def load(self, key):
        loads = hm.hyper_loads()
        self.recorder.saver.restore(self.sess, ml_record.check_form.format(hm.hypers['out_dir'],
                                                                           loads[key]))

    def reduce_res(res, name):
        return np.mean([r[name] for r in res], axis=0)

    def print_results(self, tres, vres, i, loss_string):
        msg = [''] # want first line to be blank
        if hm.hypers['finding_lr'] or hm.hypers['discrim_finding_lr']:
            msg.append("Learning Rate {:5}: {:>12.5G}".format(i, tres['clr']))
        if not hm.hypers['train_chunk'] == 0:
            msg.append("Train Loss {:8}: {:>12.5G}".format(i, tres['loss_combined']))
            msg.append(loss_string.format(*tres['loss_reports']))
        if not hm.hypers['val_chunk'] == 0:
            msg.append("Val Loss   {:8}: {:>12.5G}".format(i, vres['loss_combined']))
            msg.append(loss_string.format(*vres['loss_reports']))
        msg.append('') # leave one blank line
        joined = '\n'.join(msg)
        self.logger.debug(joined)

    def sanity_check(self, when, src):
        """ Plot weights, activations, and gradients to ensure propogation """
        should_plot = hm.hypers['sanity_plots']
        should_art = hm.hypers['sanity_artwork']
        sanity_op = {}
        scope_name = self.model.scope.name
        sanity_op['averages'] = tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, scope_name)
        sanity_op['weights'] = tf.get_collection(tf.GraphKeys.WEIGHTS, scope_name)
        sanity_op['activations'] = tf.get_collection(tf.GraphKeys.ACTIVATIONS, scope_name)
        # Using a basic optimizer here rather than Adam or the like is a speculative choice
        # If I want to capture the effects of an advanced optimizer, I think I should watch it
        #  in the steady state. Here, I just want to see gradient flow without the potentially
        #  obscuring factors of momentum or per weight adjustments.
        vanilla = tf.train.GradientDescentOptimizer(1.0, name='test_optimizer')
        grad_vars = vanilla.compute_gradients(self.model.optimize_op.loss_combined, 
                                              self.model.optimize_op.training_vars)
        sanitize = prim.sanitize
        # if gradients have good names
        grad_op = [tf.identity(grad, name=sanitize(v.name)) 
                   for grad, v in grad_vars if grad is not None]
        sanity_op['gradients'] = grad_op
        check_values = self.sess.run(sanity_op, feed_dict=src.t_feed)
        digest = self.recorder.digest()
        out_dir = self.recorder.sanity_dir()
        for cat, vals in check_values.items():
            if len(vals) == 0:
                continue
            depth_stds = []
            if should_plot:
                cat_total = len(vals)
                cat_row = ev.ceil_root(cat_total)
                cat_col = (cat_total - 1) // cat_row + 1
                cat_fig = plt.figure(figsize=(6 * cat_col, 6 * cat_row))

            stats = [['max_mean', '', 0, lambda x, y: np.abs(x) <= np.abs(y), 0],
                     ['min_std', '', np.inf, lambda x, y: x >= y, 1],
                     ['max_std', '', 0, lambda x, y: x <= y, 1]]
            for i, v in enumerate(vals):
                var_name = sanity_op[cat][i].name
                if not hm.hypers['sanity_bn'] and 'batch' in var_name:
                    continue
                sane_name = sanitize(var_name)
                if not np.isfinite(v).all():
                    continue
                moments = [np.mean(v), np.std(v)]
                depth_stds.append(moments[1])
                if should_plot:
                    cat_axes = cat_fig.add_subplot(cat_row, cat_col, i + 1)
                    cat_axes.hist(np.reshape(v, [-1])) 
                    cat_axes.set_title('{} m {:2.2g} s {:2.2g}'.format(
                        sane_name, moments[0], moments[1]))
                for s in stats:
                    metric= moments[s[4]]
                    if s[3](s[2], metric):
                        s[1] = sane_name
                        s[2] = metric 
                pic_v = v
                # TODO turn 5 into 4, then handle 4 and 2
                if len(pic_v.shape) == 5:
                    folding_shape = (pic_v.shape[0] * pic_v.shape[1],) + pic_v.shape[2:]
                    pic_v = np.reshape(pic_v, folding_shape)
                # NOT an else, but rest is an if else chain
                if len(pic_v.shape) == 4:
                    if cat == 'weights' or cat == 'gradients':
                        pic_v = np.pad(pic_v, [[1], [1], [0], [0]], 'constant')
                        pic_v = np.transpose(pic_v, [2, 0, 3, 1])
                    else: # act or moving averages
                        pic_v = np.pad(pic_v, [[0], [1], [1], [0]], 'constant')
                        pic_v = np.transpose(pic_v, [0, 1, 3, 2])
                    pic_v = np.reshape(pic_v, [pic_v.shape[0] * pic_v.shape[1],
                                               pic_v.shape[2] * pic_v.shape[3]])
                elif len(pic_v.shape) == 3:
                    pic_v = np.pad(pic_v, [[0], [1], [0]], 'constant')
                    pic_v = np.transpose(pic_v, [0, 2, 1])
                    pic_v = np.reshape(pic_v, [pic_v.shape[0] * pic_v.shape[1],
                                               pic_v.shape[2]])
                elif len(v.shape) == 2:
                    pic_v = np.pad(pic_v, [[2], [1]], 'constant')
                elif len(v.shape) == 1:
                    pic_v = np.expand_dims(pic_v, axis=1)
                elif len(v.shape) == 0:
                    pic_v = np.expand_dims(pic_v, axis=1)
                    pic_v = np.expand_dims(pic_v, axis=1)
                else:
                    raise Exception('invalid number of dimensions in sanity' + str(v.shape))
                pic_v = ev.rescale_unit(pic_v)
                pic_save = (out_dir + 'sanity_{}_{}_{}_{}.png'
                            .format(cat, sane_name, when, digest))
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    skimage.io.imsave(pic_save, skimage.img_as_ubyte(pic_v)) 

            for s in stats:
                self.logger.debug('{:<15} {:<12} {:<70} = {:>12.5G}'.format(cat, s[0], s[1], s[2]))

            depth_fig, depth_ax = plt.subplots()
            depth_ax.semilogy(depth_stds)
            depth_fig.savefig(out_dir + "by_depth_{}_{}_{}.png" 
                                .format(cat, when, digest))
            plt.close(depth_fig)

            if should_plot:
                cat_fig.subplots_adjust(hspace=0.2)
                cat_fig.tight_layout()
                cat_fig.savefig(out_dir + "histogram_{}_{}_{}.png" 
                                .format(cat, when, digest))
                plt.close(cat_fig)

    def train_one_chunk(self, src):
        train_ops = self.model.optimize_op.train_ops
        val_ops = self.model.optimize_op.val_ops
        t_flags = prim.get_training_flags(self.model.optimize_op.train_scope)
        ignore_keys = ['minimize', 'check_numerics'] # nothing to print
        t_acc, v_acc = [], []
        should_stop = False
        src.advance_feeds(self.sess)
        t_feed = dict(src.t_feed)
        v_feed = dict(src.v_feed)
        for flag in t_flags:
            t_feed[flag] = True

        for i in range(self.prime_chunk):
            # do not run optimize op, but do run with training flags set to true
            self.sess.run(val_ops, feed_dict=t_feed)
        self.prime_chunk = 0
        for i in range(hm.hypers['train_chunk'] // hm.hypers['batch_size']):
            t_batch = self.sess.run(train_ops, feed_dict=t_feed)
            for k, v in t_batch.items():
                if k in ignore_keys:
                    continue
                if np.isnan(v).any():
                    error = "Iteration {} key {} found a nan\n {}".format(i, k, v)
                    raise prim.UnstableError(error)
            # append after nan check, but stopping should still return valid
            t_acc.append(t_batch)
            if t_batch['cycle'] >= hm.hypers['max_cycle']:
                should_stop = True
                break
            yield
        for i in range(hm.hypers['val_chunk'] // hm.hypers['batch_size']):
            v_batch = self.sess.run(val_ops, feed_dict=v_feed) 
            v_acc.append(v_batch)

        t_res, v_res = {}, {}
        def reduce_all(acc, res):
            for k in acc[0].keys():
                if k in ignore_keys:
                    continue
                res[k] = Workout.reduce_res(acc, k)
        if not hm.hypers['train_chunk'] == 0:
            reduce_all(t_acc, t_res)
        if not hm.hypers['val_chunk'] == 0:
            reduce_all(v_acc, v_res)

        self.t_res = t_res
        self.v_res = v_res
        self.should_stop = should_stop

    def train_through_cycles(self, src, picture_op, picture_f):
        [None for i in self.train_cycle_iter(src, picture_op, picture_f)]

    def train_cycle_iter(self, src, picture_op, picture_f):
        should_stop = False
        i = 0

        if hm.hypers['should_summarize']:
            tensor_summaries, merged_summaries = tissue.all_summaries(
                self.model.optimize_op.training_vars)
        self.logger.debug('Init Feeds')
        src.init_feeds(self.sess)
        self.logger.debug('Sanity Check')
        self.sanity_check('pre', src)
        self.logger.debug("Loss Tree:\n" + RenderTree(self.model.optimize_op.loss_tree, 
                                                      style=anytree.render.AsciiStyle).by_attr())
        self.logger.debug('Began Training ' + self.name)

        xvals, train_losses, val_losses = [], [], []
        while not should_stop: 
            # since train one chunk reloads the training variables, there aren't any stale copies
            try:
                yield from self.train_one_chunk(src)
                tres = self.t_res
                vres = self.v_res
                should_stop = self.should_stop
            except(prim.UnstableError) as e:
                self.logger.debug(str(e))
                # The result of breaking should be that this iter never happened, including
                #  sending new values to disk. However, it's up to me to not use the results
                #  in future experiments, since the iteration just before is probably garbage
                #  as well.
                break
            self.print_results(tres, vres, i, self.model.optimize_op.loss_string)
            xvals.append(tres[self.xkey])
            train_losses.append(tres[self.ykey])
            val_losses.append(vres[self.ykey])
            if not hm.hypers['skip_artwork']:
                pres = self.sess.run(picture_op, feed_dict=src.v_feed)
                pics = picture_f(pres, i)
                pic, pic_save = pics
                pic_save = self.recorder.out_dir() + pic_save
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    skimage.io.imsave(pic_save, skimage.img_as_ubyte(pic))
            if hm.hypers['should_summarize']:
                summary = self.sess.run(merged_summaries, val_feed)
                model.recorder.board_writer.add_summary(summary, i)
            i += 1
            if not hm.hypers['skip_save']:
                try:
                    self.recorder.saver.save(self.sess, self.recorder.save_filename)
                except:
                    self.logger.debug('Skipping error in save')

            yield

        self.sanity_check('post', src)

        if len(train_losses) > 0:
            exp_fig = plt.figure()
            exp_axes = exp_fig.add_subplot(111)
            exp_axes.set_ylim(0, .00001)
            def label_axes(axes):
                axes.set_ylabel(self.ykey)
                axes.set_xlabel(self.xkey)
                old_lim = axes.get_ylim()[1]
                new_lim = 3 * np.min(train_losses) + 0.01
                axes.set_ylim(0, np.maximum(old_lim, new_lim))        
            label_axes(exp_axes)
            label_axes(self.combined_axes) # will be called repeatedly, shouldn't matter
            use_exp_plot = hm.hypers['exp_rate'] and not self.xkey == 'cycle'
            plot_f = plt.Axes.semilogx if use_exp_plot  else plt.Axes.plot
            plot_f(exp_axes, xvals, train_losses, label=self.name + '_training')
            plot_f(exp_axes, xvals, val_losses, label=self.name + '_validation')
            plot_f(self.combined_axes, xvals, train_losses, label=self.name + '_training')
            plot_f(self.combined_axes, xvals, val_losses, label=self.name + '_validation')
            exp_fig.legend(loc='upper center')
            exp_fig.savefig(self.recorder.out_dir() + "graph_{}_{}_{}.png".format(
                hm.hyper_data_name(), self.xkey, self.name))
            plt.close(exp_fig)

        self.logger.debug('Finished Training ' + self.name)

class OptimizeOp:
    """Combines loss op, update ops, and  triangular lr into one min op"""
    optimizer_unique_id = 0
    def __init__(self, tree, scope):
        self.loss_tree = tree
        self.training_vars = scope.trainable_variables()
        self.train_scope = scope
        self.loss_combined, self.loss_ops, self.loss_string = loss_tree_to_ops(self.loss_tree)
        with tf.variable_scope('optimizer' 
                               + str(OptimizeOp.optimizer_unique_id)) as self.op_scope:
            self.global_step, self.cycle, self.clr = workout.triangular_rate()
            optimizer_name = hm.hypers['optimizer'] + 'Optimizer'
            optimizer = getattr(tf.train, optimizer_name)
            with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope.name)):
                self.minimize_op = optimizer(learning_rate=self.clr).minimize(
                    self.loss_combined, var_list=(self.training_vars,), 
                    global_step=self.global_step )
            OptimizeOp.optimizer_unique_id += 1
        self.val_ops = {'clr': self.clr, 'cycle':self.cycle, 
                       'loss_reports':self.loss_ops, 'loss_combined':self.loss_combined, }
        self.train_ops = dict(self.val_ops)
        self.train_ops['minimize'] = self.minimize_op
        if hm.hypers['check_numerics']:
            self.train_ops['check_numerics'] = tf.add_check_numerics_ops()

def loss_tree_to_ops(loss_tree):
    """Reduce losses to total, ops, format_string"""
    format_lines = []
    ops = []
    real_loss = []
    number_form = '{:>12.5g}'
    def loss_tree_helper(node, weight_acc=1.0):
        weight = getattr(node, 'weight', 1.0)
        val = getattr(node, 'val', None)
        # whether a val is present and whether descendants are present are independent
        if val is not None:
            ops.append(val)
        if weight is not None and weight_acc is not None:
            deep_weight = weight * weight_acc
            if val is not None:
                deep_val = deep_weight * val
                ops.append(deep_val)
                fline = ("{:<50}| {} | {} | {} | {}"
                         .format('_'.join([n.name for n in node.path]), number_form.format(weight), 
                                 number_form.format(deep_weight), number_form, number_form))
                format_lines.append(fline)
                real_loss.append(deep_val)
        else:
            deep_weight = None # in case someone makes a child for this node
            if val is not None:
                fline = ("{:<50}|                                            | {}"
                         .format(node.name, number_form)) 
                format_lines.append(fline)
        if not node.is_leaf:
            for d in node.children:
                loss_tree_helper(d, deep_weight)
    loss_tree_helper(loss_tree)
    #only even ops have weights applied
    combined = tf.add_n(real_loss)

    return combined, ops, "\n".join(format_lines)

def triangular_rate():
    epoch_size = hm.hypers['train_chunk'] 
    global_step = tf.get_variable(name='global_step', trainable=False, 
                                  initializer=0, dtype=tf.int32)
    ex_counter = global_step * hm.hypers['batch_size'] 
    half_cycle = ex_counter / epoch_size / hm.hypers['clr_epochs']
    full_cycle = half_cycle / 2 
    int_full_cycle = tf.floor(full_cycle)
    cycle_wave = 1 - tf.abs(half_cycle - 1.0 - 2 * int_full_cycle) 
    one = tf.constant(1.0, tf.float64, name='one64')
    zero = tf.constant(0.0, tf.float64, name='zero64')
    if hm.hypers['exp_rate']:
        # e^x brought down to 0 to 1
        limit = tf.constant(hm.hypers['exp_limit'] , tf.float64, name='lim64')
        cycle_wave = ((tf.exp(cycle_wave * limit) - one) / (tf.exp(limit) - one))
    base_lr = hm.hypers['base_lr']
    max_lr = hm.hypers['max_lr']
    clr = base_lr + ((max_lr - base_lr) * tf.maximum(zero, cycle_wave))

    return global_step, full_cycle, clr

def sess_growable():
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.90)
    config=tf.ConfigProto(log_device_placement=False, gpu_options=gpu_options)
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    return sess

def my_fn_generator(name):
    """Return a function which appends a name if there is one"""
    if name == '':
        return lambda n: n
    else:
        return lambda n: '_'.join([name, n])
