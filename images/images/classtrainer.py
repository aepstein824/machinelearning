import tensorflow as tf
import numpy as np

import classification
import ml_record
import workout
import datamanager
import researcher
import hyper_manager as hm
import evaluation as ev
import primitives as prim

overrides = {
    'model':'vanilla_conv',
    'square_resolution':'32',
    'data_dir':'cifar10',
    }

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    my = workout.my_fn_generator(name)

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), 
                                        data_shape)
    x = src.features['x']
    labels = src.features['labels']

    if 'classifier' in loads:
        class_hypers = ml_record.load_hard_hypers(loads['classifier'])
    else:
        class_hypers = hypers
    with tf.variable_scope('pca_x') as pca_scope, hm.global_hyper_push(class_hypers):
        if hm.hypers['pca_x']:
            pca_name = hm.hypers['data_dir'] + '_pca'
            pca = prim.PCA(data_shape, k=3, batch_axis=2, name=pca_name)
            x = pca.encode(x, elem_shape=True)
            pca_vars = tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, pca_scope.name)
            pca_saver = tf.train.Saver(pca_vars) 
    with tf.variable_scope('classifier') as class_scope, hm.global_hyper_push(class_hypers):
        model_func = getattr(classification, hypers['model'])
        class_f = model_func(x, src.label_quantity)
        y = class_f(x)
        model = classification.Classifier(class_scope, x, y, labels)


    w = workout.Workout(my('classifier'), model, combined_axes)
    def expand_label(L):
        return ev.expand_scalars_pile(L[:, :, tf.newaxis, tf.newaxis])
    picture_op = x, expand_label(labels), expand_label(model.y)
    w.sess.run(tf.global_variables_initializer())

    if hm.hypers['pca_x']:
        pca_saver.restore(w.sess, ml_record.check_form.format(hm.hypers['out_dir'], pca_name))
    if 'classifier' in loads:
        w.recorder.saver.restore(w.sess, ml_record.check_form.format(loads['classifier']))

    w.train_through_cycles(src, picture_op, ev.picture_f)
    w.recorder.close()

if __name__ == '__main__':
    researcher.run_experiments(experiment, overrides)



