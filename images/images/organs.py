import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
import time
import logging
import string
import primitives as prim
import tissue
import hyper_manager as hm
from anytree import Node, RenderTree

def spaghetti_conv_proto(name, output_filters, act=None):
    def spaghetti_conv(x_image):
        f = hm.hypers['filter_count']
        depth = hm.hypers['resblock_count']

        layers = [
            prim.conv_proto((5, 5, f), name + '_up', strat='act_only')
            ]
        for c in string.ascii_lowercase[:depth]:
            layers.append(prim.resid_path([
                prim.bottle_conv_proto((3, 3, f), '_'.join([name, c]))
                ]))
        layers.append(prim.conv_proto((5, 5, output_filters), name + '_down',
                      strat='act_only', act=act))
        return prim.chain_layers(x_image, layers)
    return spaghetti_conv
            
def fusilli_conv_proto(name, output_filters, act=None):
    def fusilli_conv(x_image):
        f = hm.hypers['filter_count']
        depth = hm.hypers['resblock_count']

        layers = [
            prim.conv_proto((5, 5, f), name + '_up1', strat='act_only', stride=2),
            prim.conv_proto((5, 5, f), name + '_up2', strat='act_only', stride=2),
            ]
        for c in string.ascii_lowercase[:depth]:
            layers.append(prim.resid_path([
                prim.conv_proto((3, 3, f * 2), '_'.join([name, c, 'beep']), stride=2),
                prim.convT_proto((3, 3, f), '_'.join([name, c, 'boop']), stride=2),
                ]))
        layers.append(prim.convT_proto((5, 5, f), name + '_down2',
                      strat='act_only', act=act, stride=2))
        layers.append(prim.convT_proto((5, 5, output_filters), name + '_down1',
                      strat='act_only', act=act, stride=2))
        return prim.chain_layers(x_image, layers)
    return fusilli_conv 

def u_extract_proto(name, output_filters, act=None):
    filters1 = hm.hypers['filter_count']
    u_levels = hm.hypers['u_levels']
    resblock_count = hm.hypers['resblock_count']

    def u_extract(x_image):
        L = lambda i: string.ascii_uppercase[i]
        F = lambda i: (filters1 * 2 ** (i + 1))
        down_f = lambda x, i: tissue.resid_squeeze(F(i), name + '_downS' + L(i))(x)
        enc_f = lambda x, i: tissue.resid_chain(F(i), name + '_process' + L(i), 
                                                resblock_count)(x)
        dec_f = lambda x, i: tf.identity(x)
        up_f = lambda x, i: tissue.resid_inflate(F(i - 1), name + '_upS' + L(i))(x)
        reduce_f = lambda x: None

        down, up = tissue.u_proto(down_f, up_f, enc_f, dec_f)

        features = prim.chain_layers(x_image, [
            prim.conv_proto((3, 3, filters1), name + "_filter_up", strat='act_only', groups=1),
            down, up,
            prim.conv_proto((1, 1, output_filters), name + '_down', strat='act_only', act=act,
                            groups=1),
            ])
        return features
    return u_extract
