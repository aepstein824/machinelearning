
import tensorflow as tf
import numpy as np
import skimage.io
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import organs
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import datamanager

import logging
import itertools

#TODO for non saturating GANS, combine gen and discrim with flag
def generator_losses(dis_true, dis_false):
    style = hm.hypers['discrim_style']
    real_val = tf.zeros([]) 
    if style == 'l2':
        fake_val = tf.reduce_mean(tf.square(dis_false - 1))
    if style == 'rl2':
        ex_real= tf.reduce_mean(dis_true)
        ex_fake = tf.reduce_mean(dis_false)
        real_val = tf.reduce_mean(tf.square(dis_false - ex_real - 1))
        fake_val = tf.reduce_mean(tf.square(dis_true - ex_fake + 1))
    if style == 'ns':
        dec_f = lambda x: -1 * tf.log(tf.sigmoid(x) + 1e-8)
        fake_val = tf.reduce_mean(dec_f(dis_false))
    if style == 'rns':
        dec_f = lambda x, y:tf.sigmoid(x - tf.reduce_mean(y))
        fake_val = tf.reduce_mean(-1 * tf.log(dec_f(dis_false, dis_true) + 1e-8))
        real_val = tf.reduce_mean(-1 * tf.log(1 - dec_f(dis_true, dis_false) + 1e-8))
    return real_val, fake_val

def discrim_losses(dis_true, dis_false):
    style = hm.hypers['discrim_style']
    if style == 'l2':
        real_val = tf.reduce_mean(tf.square(dis_true - 1))
        fake_val = tf.reduce_mean(tf.square(dis_false))
    if style == 'rl2':
        ex_real= tf.reduce_mean(dis_true)
        ex_fake = tf.reduce_mean(dis_false)
        real_val = tf.reduce_mean(tf.square(dis_true - ex_fake - 1))
        fake_val = tf.reduce_mean(tf.square(dis_false - ex_real + 1))
    if style == 'ns':
        dec_f = lambda x: tf.sigmoid(x)
        real_val = tf.reduce_mean(-1 * tf.log(dec_f(dis_true) + 1e-8))
        fake_val = tf.reduce_mean(-1 * tf.log(1 - dec_f(dis_false) + 1e-8))
    if style == 'rns':
        dec_f = lambda x, y:tf.sigmoid(x - tf.reduce_mean(y))
        real_val = tf.reduce_mean(-1 * tf.log(dec_f(dis_true, dis_false) + 1e-8))
        fake_val = tf.reduce_mean(-1 * tf.log(1 - dec_f(dis_false, dis_true) + 1e-8))
    return real_val, fake_val
        

class Generator:
    def __init__(self, scope, dis_true, dis_false):
        self.scope = scope
        self.dis_true = dis_true
        self.dis_false = dis_false

        loss_tree = Node('loss')
        self.loss_tree = loss_tree
        real_val, fake_val = generator_losses(dis_true, dis_false)


        fool = Node('fool', parent=loss_tree, val=fake_val) 
        real = Node('real', parent=loss_tree, val=real_val) 
        regular = Node('regularization', parent=loss_tree, 
                       weight=hm.hypers['l2_factor'], val=prim.weight_penalty(scope))
        self.optimize_op = workout.OptimizeOp(loss_tree, scope)

class Discriminator:
    def __init__(self, scope, dis_true, dis_false):
        self.scope = scope
        self.dis_true = dis_true
        self.dis_false = dis_false

        loss_tree = Node('loss')
        self.loss_tree = loss_tree
        real_val, fake_val = discrim_losses(dis_true, dis_false)

        real = Node('real', val=real_val, parent=loss_tree)
        fake = Node('fake', val=fake_val, parent=loss_tree)
        regular = Node('regularization', parent=loss_tree, 
                       weight=hm.hypers['l2_factor'], val=prim.weight_penalty(scope))        

        self.optimize_op = workout.OptimizeOp(loss_tree, scope)

def dc32_gen(latents):
    gen_layers = [
        prim.convT_proto((4, 4, 512), 'gen0', stride=4, padding='VALID'), #4
        prim.convT_proto((5, 5, 256), 'gen1', stride=2), #8
        prim.convT_proto((5, 5, 128), 'gen2', stride=2), #16
        prim.convT_proto((5, 5, 3), 'gen3', stride=2, act=hm.hypers['image_actf'], 
                         strat='act_only'), #32
        ]
    return prim.chain_layers(latents, gen_layers)

def dc32_dis(x_image):
    dis_layers = [
        prim.conv_proto((4, 4, 64), 'dis0', stride=2), #16
        prim.conv_proto((5, 5, 128), 'dis1', stride=2), #8
        prim.conv_proto((5, 5, 256), 'dis2', stride=2), #4
        prim.conv_proto((4, 4, 1), 'dis4', padding='VALID', strat='linear'), #1
        lambda x: tf.reshape(x, [-1, 1])
        ]
    return prim.chain_layers(x_image, dis_layers)

def resid32_gen(latents):
    f = lambda i: hm.hypers['filter_count'] * (2 ** i)
    gen_layers = [
        prim.convT_proto((4, 4, f(2)), 'gen0', stride=4, padding='VALID'), #4
        tissue.resid_inflate(f(1), 'gen1'), #8
        tissue.resid_inflate(f(0), 'gen2'), #16
        prim.convT_proto((5, 5, 3), 'gen3', stride=2, act=hm.hypers['image_actf'], 
                         strat='act_only'), #32
        ]
    return prim.chain_layers(latents, gen_layers)

def resid32_dis(x_image):
    f = lambda i: hm.hypers['filter_count'] * (2 ** (i+2))
    dis_layers = [
        prim.conv_proto((5, 5, f(0)), 'dis0', stride=2), #16
        tissue.resid_squeeze(f(1), 'dis1', width=5),
        tissue.resid_squeeze(f(2), 'dis2', width=5),
        prim.conv_proto((4, 4, 1), 'dis3', padding='VALID', strat='linear'), #1
        lambda x: tf.reshape(x, [-1, 1])
        ]
    return prim.chain_layers(x_image, dis_layers)

def warped32_gen(latents):
    gen_layers = tissue.deep_fc_layers('warper', 8, 512) + [
        lambda x: x[:, tf.newaxis, tf.newaxis, :],
        prim.convT_proto((4, 4, 512), 'gen0', stride=4, padding='VALID'), #4
        prim.convT_proto((5, 5, 256), 'gen1', stride=2), #8
        prim.convT_proto((5, 5, 128), 'gen2', stride=2), #16
        prim.convT_proto((5, 5, 3), 'gen3', stride=2, act=hm.hypers['image_actf'], 
                         strat='act_only'), #32
        ]
    return prim.chain_layers(latents, gen_layers)

warped32_dis = dc32_dis