import tensorflow as tf
import numpy as np
import skimage.io
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import organs
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import datamanager
import polish
import researcher

import logging
import itertools


def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    my = workout.my_fn_generator(name)

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    x = src.features['x']

    sess = workout.sess_growable()
    with tf.variable_scope('polish', reuse=tf.AUTO_REUSE) as polish_scope:
        pca = prim.PCA(data_shape)
        pca_components = pca.encode(x)
        pca_recovered = pca.decode(pca_components)

    with tf.variable_scope("auto", reuse=tf.AUTO_REUSE) as auto_scope:
            #auto encoder loading
        if 'auto' in loads:
            auto_hypers = ml_record.load_hard_hypers(loads['auto'])
            with hm.global_hyper_push(auto_hypers):
                auto_model_func = getattr(ae, 'u_conv')
                enc_f, dec_f = auto_model_func(x)
                enc = enc_f(x)
                dec = dec_f(enc)
        else:
            auto_hypers = hypers
            enc = pca_components
            dec = pca_recovered
     
        if 'auto' in loads:
            auto_model = ae.Autoencoder(auto_scope, x, enc, dec, x)
            w = workout.Workout(my('auto_load'), auto_model, combined_axes, sess)
            w.load('auto')
            w.recorder.close()

    #discrim 
    def discrim_f(x_image):
        return prim.chain_layers(x_image - pca.mean, [
            organs.u_extract_proto('discrim', 1, act='identity'),
            lambda x: tf.reduce_mean(x, axis=[1, 2])
            ])

    #polisher 
    polish_f = lambda x: (organs.u_extract_proto('polisher', 3, act='tanh')(x - pca.mean))

    with tf.variable_scope(polish_scope):
        decode_ratio = hm.hypers['decode_ratio']
        unpolished = decode_ratio * dec + (1 - decode_ratio) * x
        adjustment = polish_f(unpolished)
        polished = adjustment + unpolished
    with tf.variable_scope('discrim', reuse=tf.AUTO_REUSE) as discrim_scope:
        real = discrim_f(x)
        fooled = discrim_f(polished)
    with tf.variable_scope(auto_scope), hm.global_hyper_push(auto_hypers):
        cyc_enc = pca.encode(polished)

    sess.run(tf.variables_initializer(tf.global_variables(auto_scope.name)))

    with tf.variable_scope(discrim_scope): 
        discrim_train_model = polish.PolishChecker(discrim_scope, real, fooled)
    with tf.variable_scope(polish_scope):
        polish_train_model = polish.Polisher(polish_scope, x, enc, adjustment, polished, 
                                             cyc_enc, real, fooled)
    d_picture_op = [x, ev.expand_scalars_pile(real), polished, ev.expand_scalars_pile(fooled),]
    p_picture_op = [x, dec, unpolished, polished, (adjustment + 1) / 2,
                    ev.expand_scalars_pile(enc),
                    ev.expand_scalars_pile(cyc_enc), 
                     ]


    dw = workout.Workout('discrim_train', discrim_train_model, combined_axes, sess)
    pw = workout.Workout('polish_train', polish_train_model, combined_axes, sess)

    sess.run(tf.variables_initializer(tf.global_variables(discrim_scope.name)))
    if 'discrim' in loads:
        #TODO do NOT push hypers. Instead check for agreement.
        dw.load('discrim')

    sess.run(tf.variables_initializer(tf.global_variables(polish_scope.name)))
    if 'polish' in loads:
        #TODO do NOT push hypers. Instead check for agreement.
        pw.load('polish')

    inter_iter = itertools.zip_longest(dw.train_cycle_iter(src, d_picture_op, ev.picture_f),
                                       pw.train_cycle_iter(src, p_picture_op, ev.picture_f),)
    [None for i in inter_iter]

    dw.recorder.close()
    pw.recorder.close()


if __name__ == '__main__':
    overrides = {
        'data_dir':'cifar10',
        'square_resolution':32,
        'train_chunk':5120, #chunks for loz tas
        'val_chunk':512,
        }
    researcher.run_experiments(experiment, overrides)