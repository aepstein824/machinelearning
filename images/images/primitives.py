"""Configurable primitives like convolutional layers and weights.

Variables:
TRAINING - a custom tf graph key for training flags
actf_choices - conversion from string to function for activations
norm_choices - conversion from string to normalizing function

Classes:
PCA - a layer for batched iterative pca

Functions:
sanitize - alphanumeric version of a string
add_to_set - add a variable to a tf collection without duplicating
get_training_flag - get a training variable for this exact scope
get_training_flags - get all training variables under a scope
groupable_weights - a weight matrix split into distinct groups
noisy_weight - weights with different representations and inits
biased_bias - bias with initial value
instance_norm - normalize mean and std across spatial dimension
pca_norm - convert feature dimension to uncorellated, unit data w/ pca
batch_norm - normalize variable across batch dimension, wraps tf layer
honor_request - helper for fetching unspecified parameters from hypers
choose_actf - fetch actf from a table
act_with_norm - apply activation function and normalization
batch_to_count - count elements in static portion of a tensor shape
batch_flatten - flatten into batches and features at a specified axis
variable_summaries - add common summaries for a variable
chain_layers - evaluate a list of functions
nn_proto - a fully connected layer generator
dims_2or3 - analyze a tensor's dimensions
pad_for_conv - pad a tensor to the correct size for a convolution
concat_coords - append a list of features corresponding to spatial pos
arch_conv - archetypal convolution layer around which others are molded
conv_proto - settings for 2d conv
conv3_proto - settings for 3d conv
convT_proto - settings for 2d transposed conv
convT3_proto - settings for 3d transpose conv 
bottle_conv_proto - three layer conv with spatial in a lower dim space
resid_path - skip connection with optional shortcut layers
maxpool_proto - 2x2 pooling layer that takes max value
avgpool_proto - 2x2 pooling layer that takes mean value
weight_penalty - penalize the magnitude of all weights in a scope
classification_loss - loss between two sets of labels
difference_loss - l2 loss between images
abs_diff_loss - l1 loss between images
separable_constant_conv_proto - build a spatially separable conv
nearest_2x - spatially down or upsample a tensor

Exceptions:
UnstableError - training has reached an instability

Notes:
All internal state variables that require saving are placed in
    tf.GraphKeys.MOVING_AVERAGE_VARIABLES .
Training flags are in the collection TRAINING.
All weights are added to the WEIGHTS collection.
All activations are added to the ACTIVATIONS collection.
"""

import tensorflow as tf
import numpy as np

import hyper_manager as hm

# tensorflow graph key
TRAINING = 'is_training'

def sanitize(s):
    """Replace all non alphanumeric characters with an _"""
    return ''.join((e if e.isalnum() else '_') for e in s)

def add_to_set(k, v):
    """Add v to collection k, but only if it's not already there"""
    col = tf.get_collection_ref(k)
    if not v in col:
        tf.add_to_collection(k, v)

def get_training_flag():
    """Get the training flag for this scope level and add it to the 
    training collection. During training, this variable should be fed a
    true value. If unassigned, as during testing, it will be False.
    """
    # get variable in this exact scope, with existing scope reuse
    with tf.variable_scope("training_flags", reuse=tf.AUTO_REUSE):
        flag = tf.get_variable('t', dtype=bool, initializer=False)
    add_to_set(TRAINING, flag)
    return flag 

def get_training_flags(scope=None):
    """Get all variables in TRAINING collection under a scope."""
    if scope is None:
        scope = tf.get_variable_scope()
    # could be more than one due to lower level flags
    return tf.get_collection(TRAINING, scope.name)

def groupable_weights(shape, groups=1, non_grouped=0):
    """Create a weight matrix of a specific shape and grouping level.

    shape - of the matrix, len(shape) >= 2, [spatial..., in, out]
    groups - number of independent groups; should divide shape[-1], but
        any remainder will be added to non_grouped
    non_grouped - each group will have a non zero weight for this many
        of the last input channels

    This function implements convolutional groups by building a weight 
        matrix where each output only has nonzero weights for one group
        of inputs, as well as the non_grouped set of inputs (which may
        include convolutional coordinates, for instance). The matrix is
        meant to be used unchanged in existing tf functions. While this
        method of grouping does not reduce the computation in the
        forward pass, it does reduce the number of parameters, 
        simplifying gradients and optimization. Hopefully, tensorflow
        will expose a more natural grouping mechanism in the future.
    """
    og_shape = shape
    og_grouped_in = og_shape[-2] - non_grouped
    og_out = og_shape[-1]

    # If there isn't enough to make two groups, then make one group.
    if groups == 1 or og_out < 2 * groups or og_grouped_in < 2 * groups:
        return noisy_weight(shape)

    remainder = og_grouped_in % groups
    grouped_in = og_grouped_in - remainder
    non_grouped += remainder

    group_size = grouped_in // groups
    weight_in = group_size + non_grouped
    out_group_base = og_out // groups
    out_remainder = og_out % groups
    
    out_group_size = [out_group_base + (1 if g < out_remainder else 0) for g in range(groups)]

    # Each weight group is a rectangular block obtained from noisy_weight.
    outs = []
    for g in range(groups):
        g_off = g * group_size
        section_shape = list(shape)
        section_shape[-2] = weight_in
        section_shape[-1] = out_group_size[g]
        section_w = noisy_weight(section_shape, str(g))
        c_group = section_w[...,:group_size,:]
        paddings = [[0, 0]] * len(shape)
        paddings[-2] = [g_off, (groups - 1) * group_size - g_off]
        c_padded = tf.pad(c_group, paddings)
        if non_grouped > 0:
            c_padded = tf.concat([c_padded, section_w[...,-non_grouped:,:]], axis=-2)
        outs.append(c_padded)
    w = tf.concat(outs, axis=-1)
    return w

def noisy_weight(shape, suffix=''):
    """Return initialized weights and add them to the collection.

    Arguments:
    shape - of the matrix, this function is agnostic to semantics
    suffix - for the names of the matrix and supporting variables

    Hyperparameters:
    weight_init - which initializer to use
    weight_std - multiplier for some inits
    weight_rep - independent variables, mag + direction, etc.
    """
    # first choose an initializer
    stddev = hm.hypers['weight_std']
    if hm.hypers['weight_init'] == 'glorot_uniform':
        initial = tf.glorot_uniform_initializer()
    elif hm.hypers['weight_init'] == 'glorot_normal':
        initial = tf.glorot_normal_initializer()    
    elif hm.hypers['weight_init'] == 'he_uniform':
        initial = tf.variance_scaling_initializer(mode='fan_in', distribution='uniform')    
    elif hm.hypers['weight_init'] == 'he_normal':
        initial = tf.variance_scaling_initializer(mode='fan_in')    
    elif hm.hypers['weight_init'] == 'orthogonal':
        initial = tf.orthogonal_initializer(gain=stddev)
    elif hm.hypers['weight_init'] == 'naive':
        initial = tf.truncated_normal_initializer(stddev=stddev)
    else: 
        raise ValueError(hm.hypers['weight_init'])

    # then choose a weight style
    rep = hm.hypers['weight_rep']
    # For unit vector,  the weight magnitude and direction are separate trainables.
    if rep == 'unit_vector':
        w_dir = tf.get_variable('weights_dir'+suffix, shape=shape, initializer=initial)
        axis = list(range(len(shape) - 1))
        if len(axis) == 1:
            axis = axis[0]
        w_norm = tf.sqrt(tf.reduce_mean(tf.square(w_dir), axis=axis, keepdims=True))
        w_mags_initial = tf.constant(1.0 / shape[-1], shape=w_norm.get_shape().as_list(), 
                                     dtype=tf.float32)
        w_mag = tf.get_variable('weights_mag'+suffix, initializer=w_mags_initial)
        w = w_mag * w_dir / (w_norm + 1e-5)
        add_to_set(tf.GraphKeys.WEIGHTS, w_dir)
        add_to_set(tf.GraphKeys.WEIGHTS, w_mag)
    # For spectral norm, the highest eigenvalue of the weights is set to 1
    elif rep == 'unit_spectral':
        w_og = tf.get_variable('weights'+suffix, shape=shape, initializer=initial)
        w_mat = tf.reshape(w_og, shape=[-1, shape[-1]])
        u = tf.get_variable('spectral_u'+suffix, shape=[shape[-1], 1], 
                            initializer=tf.truncated_normal_initializer(stddev=1), trainable=False)
        def power_iter(u):
            v = tf.matmul(w_mat, u)
            v_hat = tf.nn.l2_normalize(v)
            u_iter = tf.matmul(w_mat, v_hat, transpose_a=True)
            u_hat = tf.nn.l2_normalize(u_iter)
            return u_hat, v_hat
        u_hat = u
        for i in range(hm.hypers['spectral_power_iters']):
            u_hat, v_hat = power_iter(u_hat)
 
        intermediate = tf.matmul(w_mat, v_hat, transpose_a=True)
        sigma = tf.matmul(u_hat, intermediate, transpose_a=True)

        w_mat = w_mat / (sigma + 1e-5)
        with tf.control_dependencies([u.assign(u_hat)]):
            w = tf.reshape(w_mat, shape)
        add_to_set(tf.GraphKeys.WEIGHTS, w)
        add_to_set(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, u)
    # Unfinished - approximate the direct cosine transform
    elif rep == 'dct':
        removed = [i for i, s in enumerate(shape) if s == 1]
        keep = [s for i, s in enumerate(shape) if s != 1]
        k = np.pi
        if len(keep) == 1:
            ini = np.arange(-1, 1, num=keep[0])
        if len(keep) == 2:
            coords = np.mgrid[:keep[0], :keep[1]]
            # c0 / s0  * c1 * s0/s1
            ini = np.cos(k * coords[0] *  coords[1] / keep[1])
            ini /= np.maximum(np.sum(ini, axis=0), 1)
        if len(keep) == 3:
            #fold 1 into 0
            coords = np.mgrid[:keep[0], :keep[1], :keep[2]]
            # c01 / s01 * c1 * s01 / s1
            ini = np.cos(k * (coords[0] + coords[1]) * coords[2] / keep[2])
            ini /= np.maximum(np.sum(ini, axis=(0, 1)), 1)
        if len(keep) == 4:
            coords = np.mgrid[:keep[0], :keep[1], :keep[2], :keep[3]]
            # dct of frequency c2/s2 * max0, c2 * sqrt(c2) / s2 % 1 * max1, phase of c3
            #r_phased = np.remainder(coords[2] * coords[3] / keep[3], 1)
            #c_phased = np.remainder(r_phased * np.sqrt(keep[2]), 1)
            #ini = np.cos(np.pi * (coords[0] * r_phased + coords[1] * c_phased))
            if keep[2] > keep[3] / 3 and keep[3] > keep[2] / 3:
                magnitude = coords[2] / keep[2]
                angle = 2 * np.pi * coords[3] / keep[3]
            else:
                if keep[2] > keep[3]:
                    com_coord = coords[3] * keep[3] + coords[2]
                else:
                    com_coord = coords[2] * keep[2] + coords[3]
                com_total = keep[2] * keep[3]
                magnitude = com_coord / com_total
                angle = 2 * np.pi * np.remainder(magnitude * np.sqrt(com_total), 1)
            r_freq = magnitude * np.cos(angle)
            c_freq = magnitude * np.sin(angle)
            ini = np.cos(k * (coords[0] * r_freq + coords[1] * c_freq)) 
            ini /= np.maximum(np.sum(ini, axis=(0, 1)), 1)
            ini *= (coords[2] % 2) * 2 - 1
        if len(keep) == 5:
            coords = np.mgrid[:keep[0], :keep[1], :keep[2], :keep[3], :keep[4]]
            magnitude = coords[3] / keep[3]
            angle = 2 * np.pi * coords[4] / keep[4]
            r_freq = magnitude * np.cos(angle)
            c_freq = magnitude * np.sin(angle)
            d_freq = magnitude
            ini = np.cos(k * (coords[0] * d_freq + coords[1] * r_freq + coords[2] * c_freq))
            ini /= np.maximum(np.sum(ini, axis=(0, 1, 2)), 1)
        for index in removed:
            ini = np.expand_dims(ini, index)
        ini *= np.sqrt(2.0 / (np.prod(shape[:-1]) + shape[-1]))
        ini = np.moveaxis(ini, -1, 0)
        #np.random.shuffle(ini)
        ini = np.moveaxis(ini, 0, -1)
        w = tf.get_variable('weights'+suffix, initializer=ini.astype(np.float32))
        add_to_set(tf.GraphKeys.WEIGHTS, w)
    # Direct refers to the traditional, vanilla weight matrix.
    elif rep == 'direct': 
        w = tf.get_variable('weights'+suffix, shape=shape, initializer=initial)
        add_to_set(tf.GraphKeys.WEIGHTS, w)
    else:
        raise ValueError(rep)

    return w

def biased_bias(shape, constant=0.0):
    """Constant initialized biases"""
    initial = tf.constant(constant, shape=shape)
    return tf.get_variable(name='biases', initializer=initial)

def honor_request(key, requested):
    """Select a value from a request or from the global hypers"""
    if requested is None:
        return hm.hypers[key]
    else:
        return requested

def instance_norm(x_image, eps=1e-8):
    """Normalize a feature map to unit std over spatial dimensions."""
    x_shape = x_image.get_shape().as_list()
    if len(x_shape) < 4 or x_shape[1:3] == [1, 1]:
        return x_image
    dim = x_shape[-1]
    param_disable = True 
    axes = list(range(len(x_shape))[1:-1])
    # Note that moments returns variance.
    mean, std2 = tf.nn.moments(x_image, axes, name='instance_moments', keep_dims=True)
    std = tf.sqrt(std2 + eps)
    return (x_image - mean) / (std + eps)

def pca_norm(x, always_train=False):
    """ Unfinished
    Normalize a feature vector to orthogonal principle components.

    Arguments:
    x - Input is a tensor of [b, f], [b, w, h, f], or b[b, ..., f]
    always_train - if True, update PCA on all inputs
    
    See the PCA class for how this function uses hyper parameters.
    """
    shape = x.get_shape().as_list()
    data_shape = shape[1:]
    k = shape[-1]
    layer = PCA(data_shape, k=k, batch_axis=(len(shape) - 2))
    encoded = layer.encode(x, normed=True, always_train=always_train) 
    conditional = tf.cond(layer.step < k, lambda :x, lambda:encoded)
    return tf.reshape(encoded, [-1] + data_shape)

def batch_norm(x, renorm=True):
    """Wrap tf layer's batch normalization, adding variables to sets.

    Arguments:
    x - Input tensor ending in feature dimension.
    renorm - True for batch_renorm, False for traditional

    Hyper parameters:
    param_center, param_scale, bn_spatial 
    """
    shape = x.get_shape().as_list()
    param_center = hm.hypers['param_center'] 
    param_scale = hm.hypers['param_scale'] 
    if hm.hypers['bn_spatial']:
        # everything but batch dimension
        axis = list(range(1, len(shape)))
        param_center = param_scale = False
    else:
        # only each feature gets its own
        axis = -1
    axis=-1
    layer = tf.layers.BatchNormalization(center=param_center, scale=param_scale,
                                         renorm=renorm, axis=axis, momentum=hm.hypers['bn_decay'])
    layer.build(shape)
    for v in layer.non_trainable_variables:
        add_to_set(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, v)
    for v in layer.trainable_variables:
        add_to_set(tf.GraphKeys.WEIGHTS, v)
    flag = get_training_flag()
    return layer(x, flag)

actf_choices = {
    'identity':tf.identity,
    'sigmoid':tf.sigmoid,
    'leaky_relu':tf.nn.leaky_relu,
    'relu':tf.nn.relu,
    'crelu':tf.nn.crelu,
    'tanh':tf.nn.tanh,
}

norm_choices = {
    'instance':instance_norm,
    'batch':lambda x: batch_norm(x, renorm=False),
    'renorm':lambda x: batch_norm(x, renorm=True),
    'pca':pca_norm,
}

def choose_actf(requested):
    """Choose an actf from the table.

    Arguments:
    requested - None or key in actf table

    Hyper parameters:
    actf
    """
    return actf_choices[honor_request('actf', requested)]

def act_with_norm(req_act=None, req_strategy=None):
    """Apply activation function and normalization to tensor.

    Arguments:
    req_act - None or demanded activation function
    req_strategy - None or demanded normalization strategy

    Notes:
    This function also implements the currently unfinished testing cap.

    Hyper parameters:
    norm_strat, norm_time, 
    Unfinished: testing_cap, cap_scale, cap_threshold
    """
    actf = choose_actf(req_act)
    if req_strategy is None:
        strat = hm.hypers['norm_strat']
    else:
        strat = req_strategy
    when = hm.hypers['norm_time']
    if strat == 'linear':
        return lambda x: x
    if strat == 'act_only':
        return actf

    normf = norm_choices[strat]
    is_training = get_training_flag()
    cap_enable = hm.hypers['testing_cap']
    cap_scale = hm.hypers['cap_scale']
    cap_thresh = hm.hypers['cap_threshold']
    def act_norm(x):
        if when == 'pre':
            val = actf(normf(x))
        if when == 'post':
            val = normf(actf(x))
        if cap_enable:
            should_cap = tf.logical_and(tf.logical_not(is_training), 
                                        tf.greater(tf.abs(val), cap_thresh))
            #should_cap = tf.greater(tf.abs(val), cap_thresh)
            offset = cap_thresh * tf.sign(val) 
            capped = (val - offset) * cap_scale + offset
            val = tf.where(should_cap, capped, val)
        return val
    return act_norm

def batch_to_count(x, batch_axis=0):
    """Count elements in a batch, splitting at any batch axis

    Arguments:
    x - tensor consisting of batches of elements 
    batch_axis - All axes less than or equal to this are considered
        multidimensional batch indices. All axes greater than this are
        part of the multidimensional features.
    """
    return np.prod(x.get_shape().as_list()[(batch_axis + 1):])

def batch_flatten(input_tensor, batch_axis=0):
    """Flatten elements and batches, with arbitrary batch axis

    Arguments:
    x - tensor consisting of batches of elements 
    batch_axis - All axes less than or equal to this are considered
        multidimensional batch indices. All axes greater than this are
        part of the multidimensional features.
    """
    input_dim = batch_to_count(input_tensor, batch_axis)
    if len(input_tensor.get_shape()) > 2:
        input_tensor = tf.reshape(input_tensor, (-1, input_dim))
    return input_tensor, input_dim

def variable_summaries(tvar):
    """Add tensorflow summaries for a variable.

    Notes:
    Currently adds mean, std, a historgram of values and the whole 
        tensor. This is almost certainly more than I'll want for most
        variables, but I haven't used these enough to hone my usage.
    """
    fixed_var_name = sanitize(tvar.name)
    with tf.variable_scope('{}'.format(fixed_var_name)):
        mean = tf.reduce_mean(tvar)
        tf.summary.scalar('mean', mean)
        stddev = tf.sqrt(tf.reduce_mean(tf.square(tvar - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.histogram('histogram', tvar)
        tf.summary.tensor_summary('tensor', tvar)
        return fixed_var_name, mean, stddev


class PCA:
    """Incremental PCA layer

    Notes:
    Incremental PCA has a few state variables, which this adds to the
        MOVING_AVERAGE_VARIABLES collection.
    This class provides a linked encode and decode method. While in some
        applications the decode might be unnecessary, the encode is
        always required for updating.
    To modify the original IPCA variable for batching, I simply
        calculate the update for each instance, then apply the mean
        update. I have no rigorous justification for this, but it
        seems to converge consistently enough.

    Hyper parameters (for all methods):
    sparse, pca_calibration, pca_amnesiac, 
    """

    def __init__(self, d, k=None, name='pca', logify=False, batch_axis=0):
        """Create a PCA layer.

        Arguments:
        d - The original shape of each element, not including outermost
            batch axis, AKA tensor.shape[1:] 
        k - number of components to calculate
        name - used to separate multiple objects in the same scope
        batch_axis - where to split batches vs features
        """
        self.name = name
        if k is None:
            k = hm.hypers['sparse']
        self.eps = 1e-12
        self.og_d = [-1] + list(d)
        self.d = np.prod(d[batch_axis:])
        self.k = k
        self.step = tf.get_variable(name + '_step', initializer=0, trainable=False,
                                    dtype=tf.int32)
        self.mean = tf.get_variable(name + '_mean', initializer=0.0, trainable=False, 
                                    dtype=tf.float32)
        self.v_shape = [k, self.d]
        self.v = tf.get_variable(name + '_v', initializer=tf.eye(*self.v_shape), 
                                 trainable=False, dtype=tf.float32)
        self.batch_axis = batch_axis
        L = hm.hypers['pca_amnesiac']
        self.n_factor = (1.0 + L) / (1.0 + L + tf.cast(tf.minimum(
            self.step, hm.hypers['pca_calibration']), tf.float32))
        self.logify = logify

        add_to_set(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, self.v)
        add_to_set(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, self.step)
        add_to_set(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, self.mean)

    def add_sample_batch(self, x):
        """Update v, return a version that depends on the assignments"""
        v_next = []
        # After the assignment, self.mean is the new_mean
        new_mean = self.mean * (1 - self.n_factor) + self.n_factor * tf.reduce_mean(x) 
        u = x - tf.assign(self.mean, new_mean, name='{}_assign_mean'.format(self.name))
        assigned_step = tf.assign_add(self.step, 1, name='{}_assign_step'.format(self.name)) 
        batch_size = tf.shape(x)[0]
        for i in range(self.k):
            def starter(): 
                return tf.reduce_mean(u, axis=0)
            def eigen():
                vi = self.v[i, :]
                normed_vi = vi / (tf.norm(vi) + self.eps)
                new_v_inner = tf.tensordot(u, normed_vi, [[1], [0]])
                new_v_batched = tf.multiply(u, new_v_inner[...,tf.newaxis]) 
                new_v = tf.reduce_mean(new_v_batched, axis=0, keepdims=False)
                update_v = (1 - self.n_factor) * vi + self.n_factor * new_v 
                return update_v
            # step starts at 1 because it is assigned before as a dependency
            vi = tf.cond(tf.greater_equal(i, batch_size * (assigned_step - 1)), 
                         starter, eigen, name='{}_cond_{}'.format(self.name, str(i)))
            #assign an ordering
            vi = tf.cond(tf.greater_equal(vi[0], 0), lambda:vi, lambda:(vi * -1))
            normed_vi = vi / (tf.norm(vi) + self.eps)
            uvi_dot = tf.tensordot(u, normed_vi, [[1], [0]])
            uvi_scaled = tf.multiply(tf.expand_dims(uvi_dot, axis=-1),  
                                   tf.expand_dims(normed_vi, axis=0)) 
            u = u - uvi_scaled
            v_next.append(vi)
        v2 = tf.stack(v_next)
        # return v2 after it is assigned into self.v
        return tf.assign(self.v, v2, name='{}_assign_v'.format(self.name))

    def encode(self, x, normed=True, always_train=False, elem_shape=None):
        """Calcluate principle components from x.

        Arguments:
        x - input tensor
        normed - should the components be divided by their std
        always_train - can update on every iteration rather than setting
            a training flag
        elem_shape - can output components in an arbitrary shape
        """
        x, input_dim = batch_flatten(x, self.batch_axis)
        if not input_dim == self.d:
            err_str = ('The dimensionality of the new data components ({}) does not match d ({})' 
                       + ' supplied in constructor.').format(input_dim, self.d) 
            raise ValueError(err_str)
        if self.logify:
            x = tf.log(1 + x)
        if always_train:
            v = self.add_sample_batch(x)
        else:
            flag = get_training_flag()
            v = tf.cond(flag, lambda:self.add_sample_batch(x), 
                        lambda:tf.identity(self.v))
        v_norm = tf.norm(v, axis=1, keepdims=True) + self.eps
        v = v / v_norm
        encoded = tf.matmul(x - self.mean, v, transpose_b=True) # k x d * d -> k x 1
        if normed:
            #sqrt because the norm is the variance
            encoded /= tf.sqrt(v_norm[...,0])
        if elem_shape is not None:
            if elem_shape == True:
                # this only makes sense if k == d
                new_shape = self.og_d
            else:
                new_shape = [-1] + list(elem_shape)
            encoded = tf.reshape(encoded, new_shape)
        return encoded

    def decode(self, x, elem_shape=True, normed=True):
        """Reconstruct input from components.

        Arguments:
        x - components produced by corresponding encode
        elem_shape - whether decoded should be reshaped to og
        normed - whether the encoded components were normed
        """
        if len(x.shape) != 2:
            x, input_dim = batch_flatten(x, self.batch_axis)
        # b x k * k x d
        v_norm = self.v_norms()
        v = self.v / v_norm
        if normed:
            x *= tf.sqrt(v_norm[..., 0])
        decoded = tf.matmul(x, v)
        decoded += self.mean
        if self.logify:
            decoded = tf.exp(decoded) - 1
        if elem_shape is not None:
            if elem_shape == True:
                new_shape = self.og_d
            else:
                new_shape = [-1] + list(elem_shape)
            decoded = tf.reshape(decoded, new_shape)
        return decoded

    def v_norms(self):
        """The norms of v, AKA the explained variances, plus eps"""
        return tf.norm(self.v, axis=1, keepdims=True) + self.eps

def chain_layers(input_tensor, layers):
    """Chain a series of functions"""
    output_tensor = input_tensor
    for l in layers:
        output_tensor = l(output_tensor)
    return output_tensor 

def nn_proto(output_dim, layer_name, act=None, strat=None):
    """Return a layer function.

    Arguments:
    output_dim - how many output channels
    layer_name - all weights will be placed under a scope with this name
    act - force a specific activation function
    strat - force a specific norm strategy

    Notes:
    Stores activations in the activations collection.
    
    Hyper parameters:
    norm_time, see act_with_norm
    """
    def nn_layer(input_tensor):
        input_tensor, input_dim = batch_flatten(input_tensor)
        with tf.variable_scope(layer_name, reuse=tf.AUTO_REUSE):
            weights = groupable_weights((input_dim, output_dim))
            with tf.variable_scope('Wx_plus_b'):
                weighted_sum = tf.matmul(input_tensor, weights)
            if not hm.hypers['norm_time'] == 'pre':
                biases = biased_bias((output_dim,))
                weighted_sum += biases
            activations = act_with_norm(act, strat)(weighted_sum)
        tf.add_to_collection(tf.GraphKeys.ACTIVATIONS, activations)
        return activations
    return (lambda x: nn_layer(x))

def dims_2or3(x_image):
    """Analyze tensor shape for static information"""
    in_shape = x_image.get_shape().as_list()
    is3d = len(in_shape) == 5 # check for a depth spacial dimension
    in_channel_i = 4 if is3d else 3
    return in_shape, is3d, in_channel_i

def pad_for_conv(intermediate, strides, o, dims, trans, padding):
    """Pad a tensor for convolution based on mode.

    Arguments:
    intermediate - input tensor
    strides - strides for the convolution
    o - output shape
    dims - dimension of the convolution
    trans - is it a transposed convolution
    padding - Tensorflow conv mode or tf pad mode 

    Returns: (padded, mode)
    padded - the tensor padded by the required amount
    mode - the mode to pass to the tf conv function

    Hyper parameters:
    padding
    """
    mode = honor_request('padding', padding)
    if mode == 'VALID' or mode == 'SAME':
        return intermediate, mode 
    int_shape = intermediate.get_shape().as_list()
    paddings = [[0, 0]] #batch dimension
    for i, d in enumerate(dims[:-1]):
        valid = (int_shape[i + 1] - (d - 1)) // strides[i + 1]
        required = o[i + 1]
        # required - valid is in the post stride space
        to_add = (required - valid) * strides[i + 1]
        half = to_add // 2
        paddings.append([half, half + to_add % 2])
    paddings.append([0, 0]) #channel dimension
    if not trans:
        one_mask = tf.ones_like(intermediate)
        full_mask = tf.pad(one_mask, paddings, 'CONSTANT')
        inv_mask = 1.0 - full_mask
        unmasked = tf.pad(intermediate, paddings, mode)
        # gradients should only go to the original
        masked = full_mask * unmasked + tf.stop_gradient(inv_mask * unmasked)
        return masked, 'VALID'
    else:
        # A convolution with same or reflect padding requires additional
        #  padding around the sides. A trans requires that padding be 
        #  cut away. Since elements are being removed, it doesn't matter
        #  whether its reflect, same, etc. Just let conv handle it.
        return intermediate, 'SAME'

def concat_coords(input_tensor):
    """Concatenate spatial coordinates to feature dimension.

    Hyper parameters:
    conv_coords
    """
    to_concat = [input_tensor]
    in_shape, is3d, in_channel_i = dims_2or3(input_tensor)
    if 'i' in hm.hypers['conv_coords']:
        grid = np.mgrid[-1:1:in_shape[1] * 1j][:, np.newaxis]
        tile_dims = [hm.hypers['batch_size'], 1, in_shape[2]]
        if is3d:
            grid = grid[...,np.newaxis]
            tile_dims.append(in_shape[3])
        tiled = np.tile(grid, tile_dims)[...,np.newaxis]
        to_concat.append(tiled)
    if 'j' in hm.hypers['conv_coords']:
        grid = np.mgrid[-1:1:in_shape[2] * 1j][np.newaxis, :]
        tile_dims = [hm.hypers['batch_size'], in_shape[1], 1]
        if is3d:
            grid = grid[...,np.newaxis]
            tile_dims.append(in_shape[3])
        tiled = np.tile(grid, tile_dims)[...,np.newaxis]
        to_concat.append(tiled)
    if 'k' in hm.hypers['conv_coords'] and is3d:
        grid = np.mgrid[-1:1:in_shape[3] * 1j][np.newaxis, np.newaxis, :]
        tile_dims = [hm.hypers['batch_size'], in_shape[1], in_shape[2], 1]
        tiled = np.tile(grid, tile_dims)[...,np.newaxis]
        to_concat.append(tiled)
    if len(to_concat) == 1:
        return input_tensor, 0
    additional_channels = (len(to_concat) - 1)
    return tf.concat(to_concat, axis=-1), additional_channels

def arch_conv(dims_req, layer_name, stride=1, trans=False, padding=None, act=None, strat=None, 
              force_out_shape=None, group_req=None):
    """Create a convolutional layer of any kind.

    Arguments:
    dims_req - dimensions of the convolution, omitting input channels
    layer_name - all variables will be placed in this tensorflow scope
    stride - stride across all spatial dimensions
    trans - is this a transposed convolution
    padding - requested padding mode
    act - requested act function
    strat - request normalization strategy
    force_out_shape - specific out shape for transposed convolutions

    Notes:
    Handles all aspects of convolutional layer creation. 
    Adds activations to the collection. 

    Hyper parameters:
    conv_groups, stride, batch_size, norm_time, see act_with_norm,
        see pad_for_conv
    """
    def conv_layer(input_tensor):
        dims = list(dims_req)
        groups = honor_request('conv_groups', group_req)

        in_shape, is3d, in_channel_i = dims_2or3(input_tensor)
        if is3d:
            # tf nn conv3d puts depth first
            cd, cr, cc, out_ch = dims
        else:
            cr, cc, out_ch = dims

        input_tensor, non_grouped = concat_coords(input_tensor)
        in_shape[-1] += non_grouped
        in_ch = in_shape[in_channel_i]

        def apply_strided(conv_f, o, trans=False):
            stride_strat = hm.hypers['stride']
            no_stride = (1, 1, 1, 1) if not is3d else (1, 1, 1, 1, 1)
            yes_stride = ((1, stride, stride, 1) 
                          if not is3d else (1, stride, stride, stride, 1))
            if stride == 1:
                return conv_f(input_tensor, no_stride, o)
            if stride_strat == 'nearest' and stride == 2:
                if trans:
                    upsampled = nearest_2x(input_tensor, True)
                    # ouput_shape is a tensor
                    return conv_f(upsampled, no_stride, o)
                else:
                    # ouput_shape is a constant, but with [None,...]
                    pre_down = [o[0]]
                    pre_down.extend([out_dim * 2 for out_dim in o[1:]])
                    conved = conv_f(input_tensor, no_stride, pre_down)
                    return nearest_2x(conved, False)
            else:
                return conv_f(input_tensor, yes_stride, o)

        def apply_pad(intermediate, strides, o):
            return pad_for_conv(intermediate, strides, o, dims, trans, padding)

        with tf.variable_scope(layer_name, reuse=tf.AUTO_REUSE):
            dims_w_channels = [cr, cc, in_ch, out_ch]
            if is3d:
                dims_w_channels.insert(0, cd)
            weights = groupable_weights(dims_w_channels, groups=groups, non_grouped=non_grouped)
            if trans:
                dim_names = list(range(len(dims_w_channels)))
                perm = dim_names[:-2] + [dim_names[-1], dim_names[-2]]
                weights = tf.transpose(weights, perm)
            if trans and is3d:
                if not force_out_shape is None:
                    drc = force_out_shape
                else:
                    drc = (in_shape[1] * stride, in_shape[2] * stride, in_shape[3] * stride,)
                #output_shape = tf.stack([tf.shape(input_tensor)[0],
                #                        drc[0], drc[1], drc[2], out_ch])
                output_shape = [hm.hypers['batch_size'], drc[0], drc[1], drc[2], out_ch]
                conv_f = lambda x, s, o, p: tf.nn.conv3d_transpose(x, weights, o,
                                                             strides=s, padding=p)
            elif trans and not is3d:
                if not force_out_shape is None:
                    rc = force_out_shape
                else:
                    rc = (in_shape[1] * stride, in_shape[2] * stride,)
                #output_shape = tf.stack([tf.shape(input_tensor)[0], rc[0], rc[1], out_ch])
                output_shape = [hm.hypers['batch_size'], rc[0], rc[1], out_ch]
                conv_f = lambda x, s, o, p: tf.nn.conv2d_transpose(x, weights, o, 
                                                             strides=s, padding=p)
            elif not trans and is3d:
                output_shape = [hm.hypers['batch_size'], in_shape[1] // stride, 
                                in_shape[1] // stride, in_shape[2] // stride, out_ch]
                conv_f = lambda x, s, o, p: tf.nn.conv3d(x, weights, strides=s, padding=p)
            elif not trans and not is3d:
                output_shape = [hm.hypers['batch_size'], 
                                in_shape[1] // stride, in_shape[1] // stride, out_ch]
                conv_f = lambda x, s, o, p: tf.nn.conv2d(x, weights, strides=s, padding=p)
            prepad_f = lambda x, s, o: apply_pad(x, s, o)
            def full_conv_f(x, s, o):
                padded, pad_mode = prepad_f(x, s, o)
                weighted = conv_f(padded, s, o, pad_mode)
                if not hm.hypers['norm_time'] == 'pre':
                    biases = biased_bias([out_ch])
                    weighted += biases
                return weighted
            convolved = apply_strided(full_conv_f, output_shape, trans)
            activations = act_with_norm(act, strat)(convolved)
        tf.add_to_collection(tf.GraphKeys.ACTIVATIONS, activations)
        return activations
    return lambda x: conv_layer(x)

def conv_proto(dims, layer_name, stride=1, padding=None, act=None, strat=None, groups=None):
    """Create a 2d conv. See arch_conv"""
    return arch_conv(dims, layer_name, stride, False, padding, act, strat, group_req=groups)

def conv3_proto(dims, layer_name, stride=1, padding=None, act=None, strat=None, groups=None):
    """Create a 3d conv. See arch_conv"""
    return arch_conv(dims, layer_name, stride, False, padding, act, strat, group_req=groups)

def convT_proto(dims, layer_name, stride=1, padding=None, act=None, 
                strat=None, force_out_shape=None, groups=None):
    """Create a 2d Transposed conv. See arch_conv"""
    return arch_conv(dims, layer_name, stride, True, padding, act, strat, 
                     force_out_shape, group_req=groups)

def convT3_proto(dims, layer_name, stride=1, padding=None, act=None, strat=None, 
                 force_out_shape=None, groups=None):
    """Create a 3d Transposed conv. See arch_conv"""
    return arch_conv(dims, layer_name, stride, True, padding, act, strat, force_out_shape, 
                     group_req=groups)
                     

def bottle_conv_proto(dims, layer_name, stride=1, restriction=4, act=None, strat=None, 
                      skip_act=False):
    """Create a bottlenecked convolution.

    Arguments:
    dims, layer_name, stride, act, strat - see arch_conv
    restriction - the factor to reduce channels before big conv
    skip_act - the last activation can be skipped (for residual block)

    Notes:
    A bottlenecked convolution uses a 1x1 convolution to transform the
        features to a lower dimensional space, does a spatial conv in
        that space, then uses another 1x1 convolution to transform back
        to the higher dimensional space.

    Hyper parameters:
    bottle_actf - an activation specifically for the inside of bottles
    """
    def bottle_conv_layer(input_tensor):
        cw, ch, filters = dims
        restricted = filters // restriction
        down = (1, 1, restricted)
        around = (cw, ch, restricted)
        up = (1, 1, filters)
        bottle_act = act
        if bottle_act is None and hm.hypers['bottle_actf'] is not None:
            bottle_act = hm.hypers['bottle_actf']
        final_act = 'identity' if skip_act else bottle_act 

        internals= [
            conv_proto(down,   layer_name + "_down", 1, act=bottle_act, strat=strat, groups=1),
            conv_proto(around, layer_name + "_around", stride, act=bottle_act, strat=strat),
            conv_proto(up,     layer_name + "_up", 1, act=final_act, strat=strat, groups=1),
            ]
        return chain_layers(input_tensor, internals)
    return (lambda x: bottle_conv_layer(x))


def resid_path(layers, shortcut=None):
    """Add a skip connection to layers, w/ or w/o shortcut layers"""
    chain_down = (lambda x: chain_layers(x, layers))

    if shortcut is None:
        return lambda x: tf.add(x, chain_down(x))
    else:
        return lambda x: tf.add(chain_layers(x, shortcut), chain_down(x))

def maxpool_proto(layer_name):
    """Create a max pooling layer"""
    def maxpool_layer(input_tensor):
        with tf.variable_scope(layer_name):
            poolStrides = (1, 2, 2, 1)
            pooled = tf.nn.max_pool(input_tensor, ksize=poolStrides, 
                                    strides=poolStrides, padding='SAME') 
        return pooled 
    return (lambda x:maxpool_layer(x))

def avgpool_proto(width, layer_name):
    """Create an average pooling layer"""
    def avgpool_layer(input_tensor):
        with tf.variable_scope(layer_name):
            poolKernel = (1, width, width, 1)
            pooled = tf.nn.avg_pool(input_tensor, ksize=poolKernel,
                                    strides=(1, 1, 1, 1), padding='VALID')
            with tf.variable_scope('summaries'):
                tf.summary.histogram('pooled', pooled)
        return pooled
    return (lambda x:avgpool_layer(x))

def weight_penalty(scope):
    """Return the weight penalty value for all weights in the scope
    
    Hyper parameters:
    reg_style
    """
    all_vars = [tf.reshape(w, [-1]) for w in tf.get_collection(tf.GraphKeys.WEIGHTS, scope.name)]
    if len(all_vars) == 0:
        return 0
    if not len(all_vars) == 1:
        all = tf.concat(all_vars, axis=0)
    else:
        all = all_vars[0]
    style = hm.hypers['reg_style'] 
    if style == 'l2':
        total_weight = tf.reduce_sum(tf.square(all))
    elif style == 'l1':
        total_weight = tf.reduce_sum(tf.abs(all))
    return total_weight 

def classification_loss(net_out, real_out):
    """Calculate the cross entropy between two distributions"""
    with tf.variable_scope('cross_entropy'):
        diff =  tf.nn.softmax_cross_entropy_with_logits_v2(labels=real_out, logits=net_out)
        cross_entropy = tf.reduce_mean(diff)
    return cross_entropy

def difference_loss(correct, decoded):
    """Calculate the l2 loss between images"""
    return tf.reduce_mean(tf.squared_difference(correct, decoded))

def abs_diff_loss(correct, decoded):
    """Calculate the l1 loss between images"""
    return tf.reduce_mean(tf.abs(correct - decoded))

def separable_constant_conv_proto(c_r, c_c):
    """Create a layer which convolves a spatially factored constant

    Arguments:
    c_r - how to conv the rows, 1 dimensional
    c_c - how to conv the columns, 1 dimensional
    """
    c_r = np.array(c_r, dtype=np.dtype('float32'))
    c_c = np.array(c_c, dtype=np.dtype('float32'))
    if c_r.ndim == 1:
        c_r = c_r[np.newaxis, :]
    if c_c.ndim == 1:
        c_c = c_c[:, np.newaxis]
    def sep_const_conv_layer(x_image):
        in_shape, is3d, in_channel_i = dims_2or3(x_image) 
        in_channels = in_shape[in_channel_i]
        def replicate_filter(f2d):
            f4d = f2d[:, :, np.newaxis, np.newaxis]
            r4d = np.repeat(f4d, in_channels, axis=2)
            return r4d
        uc_r = replicate_filter(c_r)
        uc_c = replicate_filter(c_c)
        no_stride = (1,1,1,1)
        r_in = pad_for_conv(x_image, no_stride, in_shape,
                            [1, c_r.shape[1], in_channels], False, 'REFLECT')[0]

        conved_r = tf.nn.depthwise_conv2d(r_in, uc_r, no_stride, padding='VALID', name='const_r')
        c_in = pad_for_conv(conved_r, no_stride, in_shape,
                            [c_c.shape[0], 1, in_channels], False, 'REFLECT')[0]
        conved_c = tf.nn.depthwise_conv2d(c_in, uc_c, no_stride, padding='VALID', name='const_c')
        return conved_c
    return lambda x: sep_const_conv_layer(x)

def nearest_2x(x_image, trans=False, filters2=False):
    """Create a layer which down or up samples by two

    Arguments:
    trans - is this a transposed convolution? if so, upsample
    filters2 - whether the filters should increase for downsampling or 
        decrease for upsampling. Samples filters bilinearly.
    """
    in_shape, is3d, in_channel_i = dims_2or3(x_image)
    in_ch = in_shape[in_channel_i]
    dims = 3 if is3d else 2
    factor = 1.0 if trans else 2 ** -dims
    for i in range(dims):
        factor = [factor] * 2
    filter_flat = np.array(factor, dtype=np.dtype('float32'))
    out_ch = in_ch
    if filters2:
        if trans:
            in_ch //= 2
        else:
            out_ch = in_ch * 2
    filter_shape = [2] * dims + [in_ch, out_ch]
    filter = np.zeros(filter_shape, dtype=np.dtype('float32'))
    for i in range(in_ch):
        if filters2:
            if trans:
                filter[..., i, i // 2] =  0.5 * filter_flat 
            else:
                filter[..., i, 2 * i] = filter_flat 
                filter[..., i, 2 * i + 1] = 0.5 * filter_flat 
                # -1 index will wrap to last
                filter[..., i, 2 * i - 1] = 0.5 * filter_flat
        else:
            filter[..., i, i] = filter_flat
    s = (1, 2, 2, 2, 1) if is3d else (1, 2, 2, 1)
    if trans and is3d:
        output_shape = tf.stack([hm.hypers['batch_size'],
                                 in_shape[1] * 2, in_shape[2] * 2, in_shape[3] * 2, in_ch])
        return tf.nn.conv3d_transpose(x_image, filter, output_shape, strides=s, 
                                      padding='VALID', name='nearest2x')
    elif trans and not is3d:
        output_shape = tf.stack([hm.hypers['batch_size'],
                                 in_shape[1] * 2, in_shape[2] * 2, in_ch])
        return tf.nn.conv2d_transpose(x_image, filter, output_shape, strides=s, 
                                      padding='VALID', name='nearest2x')
    elif not trans and is3d:
        return tf.nn.conv3d(x_image, filter, strides=s, padding='VALID')
    elif not trans and not is3d:
        return tf.nn.conv2d(x_image, filter, strides=s, padding='VALID')

class UnstableError(Exception):
    """An error for when training results in NaN, Inf, etc."""
    def __init__(self, message):
        """Carry a message"""
        self.message = message
