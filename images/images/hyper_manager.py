""" Managing the hyper parameters dictionary.

Notes:
This module parses command lines and produces the hyper manager 
dictionary. The dictionary is meant to be read globally, but can be 
assigned through the use of context managers to change the globals
within a scope. These context managers work as a stack, since each one
stores the previous dictionary.
Some keys in the parameter dictionary are hard, while the rest are
flexible. Flexible parameters effect the training or viewing process,
but don't change how a model is constructed. Hard parameters control
features like model size, and must be consistent when loading a model.

Variables:
hypers - read only access to hyper parameters
hard_keys - the parameters which must stay consistent when loading

Functions:
global_hyper_push - push a new hyper global dictionary onto the stack
global_hyper_assign - push new hypers while changing only one value
hypers_from_parser - create the hypers dictionary from cmd args
hyper_data_name - get data_dir or a and b dirs if used
hyper_loads - extract a dictionary of scopes to load from hypers 
git_version - get the current git commit
"""
from contextlib import contextmanager
import argparse
import os
import subprocess

#NOTE: Not threadsafe. I'm sure there are 90 other more engineered ways 
# of doing this that are less error prone. However, this method should 
# be easy to replace (look for anything calling these functions) 

hypers = {}
@contextmanager
def global_hyper_push(new_hypers):
    """Push a new hyper global dictionary onto the stack."""
    global hypers
    old_hypers = hypers
    hypers = new_hypers
    yield
    hypers = old_hypers

@contextmanager
def global_hyper_assign(k, v):
    """Create a new hyper dictionary by changing one value, then push"""
    global hypers
    old_val = hypers.get(k, None)
    hypers[k] = v
    yield
    hypers[k] = old_val

hard_keys = [] 

def hypers_from_parser(overrides):
    """Fill the hypers dictionary from arguments.

    Arguments:
    overrides - new default values for argparse
    """
    global hard_keys

    parser = argparse.ArgumentParser()
    def add_arg(is_hard, arg_name, **kwargs):
        if is_hard:
            hard_keys.append(arg_name)
        if 'help' in kwargs:
            kwargs['help'] += ' (hard)' if is_hard else ' (flex)'
        parser.add_argument('--' + arg_name, **kwargs)
    def add_flex(arg_name, **kwargs):
        add_arg(False, arg_name, **kwargs)
    def add_hard(arg_name, **kwargs):
        add_arg(True, arg_name, **kwargs)
    #meta
    add_flex('data_dir', type=str, default='cifar10', help='data set name and directory')
    #  default is None to make it easier to ignore when unused
    add_flex('a_dir', type=str, default=None, help='for working with two datasets') 
    add_flex('b_dir', type=str, default=None, help='for working with two datasets')
    #  the chunks are divisible by 1024
    add_flex('train_chunk', type=int, default=49152, help='number of examples in an epoch')
    add_flex('val_chunk', type=int, default=5120, help='number of examples to validate per epoch')
    add_flex('device', type=str, default="0", help='which gpu to run on, assigned to env')
    add_flex('out_dir', type=str, default='../..', help='location to store records')
    add_flex('comment', type=str, default='', help='a message to store with the record')
    add_flex('square_resolution', type=int, default=32, help='width and height to resize to')
    add_flex('resize', action='store_true', help='resize images before processing')
    #special modes
    add_flex('finding_lr', action='store_true', help='scan over increasing learning rate')
    add_flex('discrim_finding_lr', action='store_true', help='see finding_lr')
    add_flex('should_summarize', action='store_true', help='write tensorboard summaries')
    add_flex('skip_save', action='store_true', help='forgo checkpoint saving')
    add_flex('check_numerics', action='store_true', help='insert tf check numerics')
    add_flex('sanity_plots', action='store_true', help='create histograms for sanity values')
    add_flex('sanity_artwork', action='store_true', help='create pictures of sanity values')
    add_flex('sanity_bn', action='store_true', help='include batch norm internals in sanity')
    add_flex('skip_artwork', action='store_true', help='skip the main art output')
    add_flex('dry_run', action='store_true', help='test with few iterations end to end')
    add_flex('search', action='store_true', help='run through search lists in researcher.py')
    add_flex('video', action='store_true', help='unfinished: read sequential images')
    add_flex('analyze_latent', action='store_true', help='run extra autoencoder analysis')
    add_flex('latent_change', type=float, default=1.0, help='multiplier for latent space delta')
    add_flex('prime_chunk', type=int, default=None, help='initial iterations without trains')
    #load pretrained
    add_flex('load', type=str, nargs=2, default=[], action='append', 
             help='each entry is a scope name followed by the name of the dir to load into it')
    #learning
    add_flex('max_cycle', type=float, default=1, help='number of learning rate cycles')
    add_flex('clr_epochs', type=int, default=10, help='number of epochs in a half cycle')
    add_flex('optimizer', type=str, default='Adam', help='optimizer to use')
    add_flex('base_lr', type=float, default=None, help='minimum learning rate')
    add_flex('max_lr', type=float, default=None, help='maximum learning rate') #no default
    add_flex('exp_rate', action='store_true', help='switch to exponential lr cycle')
    add_flex('exp_limit', type=float, default=10.0, help='how exponential')
    add_flex('batch_size', type=int, default=64, help='examples in a batch')
    add_hard('pca_x', action='store_true', help='use a pca colors instead of default')
    add_flex('pca_calibration', type=int, default=100, help='ipca calibration iterations')
    add_flex('pca_amnesiac', type=int, default=2, help='extra bias for new pca features')
    #model - generic
    add_hard('model', type=str, default='paper_conv', help='name of architecture to use')
    add_hard('actf', type=str, default='relu', help='name of activation function')
    add_hard('bottle_actf', type=str, default=None, help='actf for middle of bottlenecks')
    add_hard('decision_actf', type=str, default='sigmoid', help='actf for classifier, discrim')
    add_hard('image_actf', type=str, default='sigmoid', help='actf for image outputs')
    add_hard('rep_actf', type=str, default='identity', help='actf for latent representations')
    add_hard('filter_count', type=int, default=32, help='base feature map count')
    add_hard('u_levels', type=int, default=2, help='number of levels in u')
    add_flex('weight_init', type=str, default='he_normal', help='weight initializer')
    add_flex('weight_std', type=float, default=1.0, help='weight standard deviation')
    add_hard('weight_rep', type=str, default='direct', help='weight representation')
    add_flex('spectral_power_iters', type=int, default=3, 
             help='iters for approxmating spectral norm for weight_rep unit_spectral')
    add_hard('padding', type=str, default='SAME', help='type of padding')
    add_hard('stride', type=str, default='nearest', help='method for strided convolutions')
    add_hard('resblock_count', type=int, default=2, help='number of blocks in each residual chain')
    add_hard('conv_depth', type=int, default=2, help='number of stride, chain layers')
    add_hard('conv_groups', type=int, default=1, help='number of indepndent groups in conv maps')
    add_hard('conv_coords', type=str, default='', help='additional coordinates to conv inputs')
    #model - autoencoding
    add_hard('sparse', type=int, default=128, help='size of constrained latent representation')
    add_hard('learned', type=int, default=None, help='extra parameters optimized')
    #model - ensemble specific
    add_hard('mid_total', type=int, default=4, help='unfinished')
    add_hard('mid_choices', type=int, default=2, help='unfinished')
    #model - stereo
    add_hard('max_d', type=int, default=32, help='disparity limit')
    #model - gan
    add_hard('discrim_filter_count', type=int, default=16, help='see above')
    add_hard('discrim_model', type=str, default='resid32', help='see above')
    add_flex('discrim_loss_factor', type=float, default=0.20, help='see above')
    add_hard('discrim_style', type=str, default='ns', help='see above')
    add_flex('discrim_noise', type=float, default=0.4, help='see above')
    add_flex('discrim_base_lr', type=float, default=None, help='see above')
    add_flex('discrim_max_lr', type=float, default=None, help='see above')
    #model - segments
    add_hard('segment_divides', type=int, default=2, help='tree depth')
    add_flex('segment_instance', action='store_true', help='segment thresholds are per example')
    add_flex('segment_blur', type=int, default=9, help='size of segment blur kernel')
    #model - polish
    add_flex('polish_cyclic', type=float, default=0.01, help='coefficient of cyclic consistency')
    add_flex('polish_l2', type=float, default=0.01, help='penalize size of polishing adjustment')
    #regularize
    add_flex('l2_factor', type=float, default=1e-7, help='weight penalty')
    add_flex('reg_style', type=str, default='l2', help='type of weight penalty')
    add_flex('input_drop', type=float, default=1.0, help='p of keeping input with dropout')
    add_flex('pool_drop', type=float, default=1.0, help='p of keeping pool layer with dropout')
    add_flex('bn_decay', type=float, default=0.5, help='batch norm momentum')
    add_hard('norm_strat', type=str, default='renorm', help='strategy for normalizing')
    add_hard('norm_time', type=str, default='post', help='before or after activation')
    add_flex('testing_cap', type=bool, default=False, help='unfinished')
    add_flex('cap_scale', type=float, default=0.001, help='unfinished')
    add_flex('cap_threshold', type=float, default=3, help='unfinished')
    add_hard('param_center', type=bool, default=True, help='whether norm can train its mean')
    add_hard('param_scale', type=bool, default=True, help='whether norm can train its std')
    add_hard('bn_spatial', action='store_true', help='batch norm for each spatial point')
    add_flex('perc_loss_l2', type=float, default=1.0, help='weight of l2 loss in perception')
    add_flex('perc_loss_grad', type=float, default=0.0, help='weight of gradients in perception')
    #depth
    add_flex('depth_loss_recon', type=float, default=0.7, help='weight of reconstruction')
    add_flex('depth_loss_loop', type=float, default=0.3, help='weight of cyclic consistency')
    add_flex('depth_loss_dsmooth', type=float, default=0.0005, help='weight of smooth grad')
    add_flex('depth_loss_mdh', type=float, default=0.0003, help='weight of disparity penalty')

    #allow specific trainers to override
    parser.set_defaults(**overrides)
    #turn to dict
    FLAGS = parser.parse_args()
    hypers = vars(FLAGS)

    #handle special modes
    if hypers['finding_lr'] or hypers['discrim_finding_lr']:
        hypers['discrim_loss_factor'] = 0.0
        hypers['max_cycle'] = 0.5
        hypers['exp_rate'] = True
        hypers['skip_save'] = True
        #hypers['skip_artwork'] = True
    if hypers['prime_chunk'] is None:
        if 'pca' in hypers['model']:
            hypers['prime_chunk'] = 10
        else:
            hypers['prime_chunk'] = 0

    #record keeping
    hypers['git_commit'] = git_version()
    
    return hypers

def hyper_data_name():
    """Get the data name, whether one or two datasets are used"""
    if hypers.get('a_dir', None) is not None:
        data_name = '{}_x_{}'.format(hypers['a_dir'], hypers['b_dir'])
    else:
        data_name = hypers['data_dir']
    return data_name

def hyper_loads():
    """Extract the scope loading dictionary"""
    loads = {}
    for k, v in hypers['load']:
        loads[k] = v
    return loads

# stolen from numpy
# Return the git revision as a string
def git_version():
    """Read the current git commit"""
    def _minimal_ext_cmd(cmd):
        # construct minimal environment
        env = {}
        for k in ['SYSTEMROOT', 'PATH']:
            v = os.environ.get(k)
            if v is not None:
                env[k] = v
        # LANGUAGE is used on win32
        env['LANGUAGE'] = 'C'
        env['LANG'] = 'C'
        env['LC_ALL'] = 'C'
        out = subprocess.Popen(cmd, stdout = subprocess.PIPE, env=env).communicate()[0]
        return out

    try:
        out = _minimal_ext_cmd(['git', 'rev-parse', 'HEAD'])
        GIT_REVISION = out.strip().decode('ascii')
    except OSError:
        GIT_REVISION = "Unknown"

    return GIT_REVISION