import os
import os.path
import urllib.request
import time

dank = '../tasvideos.txt'
out_form = '../datasets/{}'
dataset_file = '../tassets.txt'

with open(dank, 'r') as f, open(dataset_file, 'a') as o:
    for line in f:
        parsed = line.split(' ')
        if not len(parsed) == 4:
            print('Could not parse ' + line)
            continue

        loc = out_form.format(parsed[0])
        video_name = loc + '.video'
        if os.path.exists(video_name):
            print ('Already did ' + loc)
            continue
        try:
            urllib.request.urlretrieve(parsed[3], video_name)
        except Exception as e:
            print('Failed ' + loc)
            print(e)
            print(line)
            continue


        o.write('Description("{}", "{}", "mono", "images", 0),\n'
                .format(parsed[0], parsed[1]))

        
        print('Attempted to download ' + video_name)

        time.sleep(10)