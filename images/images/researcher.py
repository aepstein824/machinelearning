"""Run a multitude of experiments and plot the total results.

Functions:
run_experiments - run the trainer described experiment with configured hypers
"""
import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
import anytree
from anytree import Node, RenderTree

import primitives as prim
import hyper_manager as hm
import tissue
import autoencoding as ae
import evaluation as ev
import ml_record
import workout

import time
import logging
import warnings
import os
import subprocess
import logging
import warnings
import string
import json

def run_experiments(exp_function, overrides=None, skip_questions=False):
    """Run the trainer provided experiment function with hypers.

    exp_function - function to train and eval network
    overrides - defaults for hypers that override global defaults but not
        values provided on the command line

    Deals with dry_run modifications, selecting a GPU, searching over
        parameters, asking the user for a purpose, and plotting the
        combined results.
    """
    # This function is a strong candidate for breaking up.
    if overrides is None:
        overrides = {}
    hypers = hm.hypers_from_parser(overrides)

    #restrict to one GPU
    os.environ["CUDA_VISIBLE_DEVICES"] = hypers['device']

    combined_fig, (combined_axes, lx) = plt.subplots(ncols=2, figsize=(12,6))
    combined_axes.set_ylim(0, .00001)

    def one_exp(name, h):
        exp_graph = tf.Graph()
        if h['finding_lr']:
            if h['base_lr'] is None:
                h['base_lr'] = 0
            if h['max_lr'] is None:
                h['max_lr'] = 1.0 
        if h['discrim_finding_lr']:
            if h['discrim_base_lr'] is None:
                h['discrim_base_lr'] = 0
            if h['discrim_max_lr'] is None:
                h['discrim_max_lr'] = 1.0 
        if h['base_lr'] is None:
            h['base_lr'] = hypers['max_lr'] / 5
        if h['discrim_base_lr'] is None and hypers['discrim_max_lr'] is not None:
            h['discrim_base_lr'] = hypers['discrim_max_lr'] / 5
        if h['dry_run']:
            h['train_chunk'] = h['batch_size'] * 2
            h['val_chunk'] = h['train_chunk']
            h['clr_epochs'] = 4
            h['max_cycle'] = 0.5
        if h['pca_x']:
            h['image_actf'] = 'identity'

        with exp_graph.as_default(), hm.global_hyper_push(h):
            exp_function(name, combined_axes)

    searches = [('', hypers)] 
    def search(key, values):
        if key not in hypers:
            print('WARNING: Searching over {} which was not previously in hypers.'.format(key))
        new_searches = []
        for s in searches:
            for v in values:
                ns = dict(s[1])
                ns[key] = v
                nn = '_'.join([str(key), str(v), s[0]])
                new_searches.append((nn, ns)) 
        return new_searches 

    #Search List
    #searches = search('conv_groups', [1, 2, 4, 8])
    #searches = search('sparse', [4, 16, 64])
    #searches = search('norm_strat', ['batch', 'pca', 'act_only', 'renorm', 'instance'])
    #searches = search('pca_x', [True, False])
    #searches = search('optimizer', ['GradientDescent', 'Adam'])
    #searches = search('weight_rep', ['direct', 'unit_vector', 'unit_spectral'])
    #searches = search('norm_time', ['pre', 'post'])
    #searches = search('u_levels', range(1, 5))
    #searches = search('l2_factor', [1, 0.01, 1e-5, 1e-8, 0])
    #searches = search('data_dir', ['cifar10', 'the_legend_of_zelda_oracle_of_seasons'])
    #searches = search('resblock_count', [2, 4, 8])
    searches = search('depth_loss_dsmooth', [0.002, 0.0002, 0])
    searches = search('depth_loss_mdh', [0.001, 0.0001, 0])

    if hypers['dry_run']:
        skip_questions = True
    if not skip_questions:
        quest = input('What are you trying to learn?\n')

    if hypers['search']:
        for s in searches:
            one_exp(*s)
    else:
        one_exp('', hypers)

    with hm.global_hyper_push(hypers):
        data_name = hm.hyper_data_name()

    h, l = combined_axes.get_legend_handles_labels()
    lx.legend(h, l, borderaxespad=0)
    lx.axis('off')
    combined_fig.savefig("{}/out/{}_combined_{}.png".format(hypers['out_dir'],
                                                            data_name, hypers['comment']))

    with hm.global_hyper_push(hypers):
        if not skip_questions:
            answer = ""
            while not answer or answer.isspace():
                answer = input('You started with\n{}\nWhat did you learn?\n'.format(quest))
            qa_out = '{}/out/qa/{}.txt'.format(hm.hypers['out_dir'], 
                                               prim.sanitize(time.strftime(ml_record.time_form)))
            with open(qa_out, 'w') as qa_f:
                qa_f.write(quest + '\n' + answer + '\n' + json.dumps(hypers))

    plt.close(combined_fig)
