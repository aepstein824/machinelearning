import tensorflow as tf
import numpy as np

import workout
import datamanager
import researcher
import hyper_manager as hm
import evaluation as ev

import dataviewer
import classtrainer
import autotrainer
import depthtrainer
import gan_trainer
import gan_finisher_trainer
import pca_trainer
import iterative_segment_trainer


if __name__ == '__main__':
    modules = [pca_trainer, dataviewer, classtrainer, autotrainer, depthtrainer, gan_trainer, ]
    for md in modules:
        overrides = dict(md.overrides)
        overrides['finding_lr'] = True
        overrides['comment'] ='_testing_' + str(md.__name__)
        overrides['resize'] = True
        researcher.run_experiments(md.experiment, overrides, skip_questions=True)