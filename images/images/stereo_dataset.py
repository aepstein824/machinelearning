import tensorflow as tf
import numpy as np

file_pattern_form = "../{}/*.png"

def dataset_from_record_name(fpattern, shape, hypers):
    filename_dataset = tf.data.Dataset.list_files(fpattern)

    def image_load(fname):
        encoded = tf.read_file(fname)
        sbs = tf.cast(tf.image.decode_png(encoded), dtype=tf.float32)
        midpoint = shape[1]
        # TODO load shape from somewhere
        left = tf.reshape(sbs[:, 0:midpoint, :], shape)
        right = tf.reshape(sbs[:, midpoint:2 * midpoint, :], shape)
        return left / 255.0, right / 255.0

    image_dataset = (filename_dataset.cache().repeat().shuffle(buffer_size=20000)
                     .map(image_load, num_parallel_calls=16) 
                     .batch(hypers['batch_size'])
                     .prefetch(10)
                     )

    return image_dataset

def set_and_iter(data_shape, hypers):
    name_pattern = file_pattern_form.format(hypers['data_dir'])
    ds = dataset_from_record_name(name_pattern, data_shape, hypers)
    iter = ds.make_one_shot_iterator()

    return ds, iter

