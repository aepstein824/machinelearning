import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import autoencoding as ae
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import image_tfr
import datamanager
import researcher

import argparse
import time
import logging
import os
import logging
import warnings

overrides = {
    #'data_dir':'the_legend_of_zelda_oracle_of_seasons',
    'data_dir':'cifar10',
    'square_resolution':32,
    'train_chunk':5120, #chunks for loz tas
    'val_chunk':512,
    'model':'deep_conv_pca',
    'perc_loss_ssim':0.0,
    'perc_loss_l1':1.0,
    'perc_loss_grad':0.0,
    }

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    my = workout.my_fn_generator(name)

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    #src = datamanager.VideoSource(hypers['data_dir'], data_shape)
    x = src.features['x']
    is_tight = 'info' in src.features
    if is_tight:
        info = src.features['info']
    #model and loss creation
    if 'auto' in loads:
        auto_hypers = ml_record.load_hard_hypers(loads['auto']) 
    else:
        auto_hypers = hypers
    with tf.variable_scope('pca_x') as pca_scope, hm.global_hyper_push(auto_hypers):
        if hm.hypers['pca_x']:
            pca_name = hm.hypers['data_dir'] + '_pca'
            pca = prim.PCA(data_shape, k=3, batch_axis=2, name=pca_name)
            x = pca.encode(x, elem_shape=True)
            pca_vars = tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES, pca_scope.name)
            pca_saver = tf.train.Saver(pca_vars)
    with tf.variable_scope("auto") as auto_scope, hm.global_hyper_push(auto_hypers):
        model_func = getattr(ae, hm.hypers['model'])
        enc_f, dec_f = model_func(x)
        if is_tight:
            enc = enc_f(tf.concat([x, info], axis=3))
        else:
            enc = enc_f(x)
        dec = dec_f(enc)
        model = ae.Autoencoder(auto_scope, x, enc, dec, x)
        w = workout.Workout(my('auto'), model, combined_axes)

        w.sess.run(tf.global_variables_initializer())
        if 'auto' in loads:
            w.load('auto')

        if hm.hypers['pca_x']:
            picture_op = [x, dec, pca.decode(x), pca.decode(dec), ev.expand_scalars_pile(enc)]
            pca_saver.restore(w.sess, ml_record.check_form.format(hm.hypers['out_dir'], 
                                                                  pca_name))
        else:
            picture_op = [x, dec, ev.expand_scalars_pile(enc)]
        w.train_through_cycles(src, picture_op, ev.picture_f)

        if hm.hypers['analyze_latent']:
            feed_x, feed_enc = w.sess.run([x, enc], feed_dict=src.v_feed)
            feed_enc = np.array(feed_enc)
            it = np.nditer(feed_enc[0], flags=['multi_index'])
            count = 0
            out_dir = w.recorder.out_dir()
            frozen_feed = dict(src.v_feed)
            frozen_feed[x] = feed_x

            lat_total = len(feed_enc[0].flatten())
            lat_row = ev.ceil_root(lat_total)
            lat_col = (lat_total - 1) // lat_row + 1
            lat_fig = plt.figure(figsize=(6 * lat_col, 6 * lat_row))

            interped = []
            for l in feed_enc[:hm.hypers['batch_size']]:
                between_encs = []
                for r in feed_enc[:hm.hypers['batch_size']]:
                    mid_enc = ev.slerp(l, r, 0.5)
                    between_encs.append(mid_enc)
                inter_dec = dec
                if hm.hypers['pca_x']:
                    inter_dec = pca.decode(dec)
                inter_enc = np.stack(between_encs)
                inter_feed = dict(frozen_feed)
                inter_feed[enc] = inter_enc
                interped.append(w.sess.run(inter_dec, feed_dict=inter_feed))
            interp_image, _ = ev.picture_f(interped, 0)
            ev.imsave_ignore(out_dir + 'latent_spherical_interp.png', interp_image)

            interped = []
            halfway = feed_enc.shape[-1] // 2
            for l in feed_enc[:hm.hypers['batch_size']]:
                l = np.array(l)
                between_encs = []
                top_half = l[...,:halfway]
                for r in feed_enc[:hm.hypers['batch_size']]:
                    r = np.array(r)
                    mid_enc = np.concatenate([top_half, r[...,halfway:]], -1)
                    between_encs.append(mid_enc)
                inter_dec = dec
                if hm.hypers['pca_x']:
                    inter_dec = pca.decode(dec)
                inter_enc = np.stack(between_encs)
                inter_feed = dict(frozen_feed)
                inter_feed[enc] = inter_enc
                interped.append(w.sess.run(inter_dec, feed_dict=inter_feed))
            interp_image, _ = ev.picture_f(interped, 0)
            ev.imsave_ignore(out_dir + 'latent_cross_interp.png', interp_image)

            latent = tf.truncated_normal(feed_enc.shape)
            generated = dec_f(latent)
            if hm.hypers['pca_x']:
                generated = pca.decode(generated)
            gen_batch = w.sess.run(generated, feed_dict=frozen_feed)
            gen_image, _ = ev.picture_f([gen_batch], 0)
            ev.imsave_ignore(out_dir + 'latent_generation.png', gen_image)

            while not it.finished:
                new_decs = [feed_x]
                change = np.zeros(feed_enc.shape[1:])
                change[it.multi_index] = hm.hypers['latent_change']
                explore_changes = [-3, -2, -1, -0.5, 0, 0.5, 1, 2, 3]
                new_encs = [feed_enc + change * i for i in explore_changes]
                for new_enc in new_encs: 
                    changed_feed = dict(frozen_feed)
                    changed_feed[enc] = new_enc
                    changed_dec = dec
                    if hm.hypers['pca_x']:
                        changed_dec = pca.decode(dec)
                    new_dec = w.sess.run(changed_dec, feed_dict=changed_feed)
                    new_decs.append(new_dec)

                lat_axes = lat_fig.add_subplot(lat_row, lat_col, count + 1)
                lat_flat = np.reshape(feed_enc[..., count], [-1])
                lat_axes.hist(lat_flat) 
                lat_axes.set_title('latent {} {:.2f} {:.2f}'.format(
                    count, np.mean(lat_flat), np.std(lat_flat)))

                count += 1 
                it.iternext()
                comparison, pic_name = ev.picture_f(new_decs, count)
                ev.imsave_ignore(out_dir + 'latent_' + pic_name, comparison)

            lat_fig.subplots_adjust(hspace=0.2)
            lat_fig.tight_layout()
            lat_fig.savefig(out_dir + 'latent_distribution.png')
            plt.close(lat_fig)

        w.recorder.close()

if __name__ == '__main__':
    researcher.run_experiments(experiment, overrides)