from random import shuffle
import glob
import parse
import skimage.io
import skimage.transform
import tensorflow as tf
import evaluation

shuffle_data = True
img_location = 'F:\\Data\\adult_dataset\\raw'
img_glob = img_location + '\\images\\*'
img_fmt1 = img_location + '\\images\\{}.mp4_128.mp4_{}.jpg'
img_fmt2 = img_location + '\\images\\{}_128.mp4_{}.jpg'
img_fmt3 = img_location + '\\images\\{}_{}.png'
record_name = img_location + '\\tfrecords\\'

filenames = glob.glob(img_glob)

def parse_adult128(name):
    res = parse.parse(img_fmt1, name)
    if res == None:
        res = parse.parse(img_fmt2, name)
    if res == None:
        res = parse.parse(img_fmt3, name)
    return int(res[0]), int(res[1])

def load_adult128(name):
    vid, frame = parse_adult128(name)
    img = skimage.io.imread(name)
    return img, vid, frame 

def tf_feature_int64(x):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[x]))

def tf_feature_bytes(x):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[x]))

def tf_feature_floatlist(x):
    return tf.train.Feature(float_list=tf.train.FloatList(value=x.reshape(-1)))


types = (("train", (lambda i : i % 10 < 8)),
         ("validate", (lambda i : i %10 == 8)),
         ("test", (lambda i : i % 10 == 9)))
writers = [tf.python_io.TFRecordWriter(record_name + t[0]) for t in types]

for i, name in enumerate(filenames):
    img, vid, frame = load_adult128(name)
    og_shape = img.shape
    resized = skimage.transform.resize(img, [128, og_shape[1], 3])
    tight, info = evaluation.tighten_img(resized, 128)
    if i % 1000 == 0:
        print("Loading data {}".format(i))
    for j, t in enumerate(types):
        if t[1](frame):
            feature = {'label':tf_feature_int64(vid),
                       'tightened':tf_feature_floatlist(tight),
                       'info':tf_feature_floatlist(info),
                       'height':tf_feature_int64(128),
                       'width':tf_feature_int64(128),
                       'depth':tf_feature_int64(3),}
            example = tf.train.Example(features=tf.train.Features(feature=feature))
            writers[j].write(example.SerializeToString())

for w in writers:
    w.close()


