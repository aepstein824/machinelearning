import numpy as np
import argparse
from matplotlib import pyplot as plt

import skimage.io
import skimage.transform
import hashlib
import warnings

def save_perspectives(hypers):
    if hypers['type'] in ('SBS'):
        img = skimage.io.imread(hypers['img'])
        width = img.shape[1] #number of columns
        imgL = img[:, :width//2]
        imgR = img[:, width//2:]
    if hypers['type'] == 'TB':
        img = skimage.io.imread(hypers['img'])
        height = img.shape[0] #number of rows 
        imgL = img[:height//2, :]
        imgR = img[height//2:, :]
    if False:
        imgL = cv2.imread('left.png', 0)
        imgR = cv2.imread('right.png', 0)
        print (imgR.shape)

    views = hypers['views']

    tex_shape = np.array([hypers['size'] * 2, hypers['size'] * views, 3])
    face_shape = np.array([hypers['size'], hypers['size'] + hypers['extend'], 3])

    if hypers['hdeg'] == 0:
        tex_shape[0] //= 2

    imgL = skimage.transform.resize(imgL, tex_shape)
    imgR = skimage.transform.resize(imgR, tex_shape)

    hasher = hashlib.shake_128()
    hasher.update(hypers['img'].encode())
    uid = hasher.hexdigest(8)

    if hypers['hdeg'] == 0:
        skimage.io.imsave('{}/{}_{}_{}.png'.format(hypers['out'], uid, 0, 0), 
                          np.concatenate((imgL, imgR), axis=1))
        return


    dt = np.dtype('f4')
    iters = np.mgrid[:face_shape[0],:face_shape[1]]
    uvR = (face_shape[0] - 2 * iters[0]) / face_shape[0] 
    uvC = (face_shape[1] - 2 * iters[1]) / face_shape[1] 
    radius = np.sqrt(np.square(uvR) + np.square(uvC) + 1)
    theta = np.arctan2(-1 * uvC, 1) 
    phi = np.arccos(uvR / radius) 

    def mappingForOffset(t_off, p_off):
        map_r = np.zeros(face_shape, dtype=dt)
        map_c = np.zeros(face_shape, dtype=dt)
        map_r = (phi + p_off) / np.pi * imgL.shape[0]
        theta_factor = 360.0 / hypers['hdeg'] / (2 * np.pi)
        map_c =  (theta_factor * (theta + t_off) + hypers['offset']) % 1 * imgL.shape[1]
        map_r = np.repeat(map_r[:, :, np.newaxis], 3, axis=2)
        map_c = np.repeat(map_c[:, :, np.newaxis], 3, axis=2)
        map_d = np.mgrid[:face_shape[0], :face_shape[1], :3][2] 
        return map_r, map_c, map_d


    for i in range(views):
        map_r, map_c, map_d = mappingForOffset((np.pi / 2) * i, 0)
        mapping = np.stack([map_r, map_c, map_d]) 
        remapped_l = skimage.transform.warp(imgL, mapping, output_shape=face_shape, mode='edge')
        remapped_r = skimage.transform.warp(imgR, mapping, output_shape=face_shape, mode='edge')
        if views == 1 and hypers['keep_name']:
            out_name = '{}/{}'.format(hypers['out'], hypers['img'])
        else:
            out_name = '{}/{}_{}_{}.png'.format(hypers['out'], uid, hypers['hdeg'], i)
        skimage.io.imsave(out_name, np.concatenate((remapped_l, remapped_r), axis=1))
        print(out_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--img', type=str)
    parser.add_argument('--out', type=str)
    parser.add_argument('--extend', type=int, default=0)
    parser.add_argument('--views', type=int, default=1)
    parser.add_argument('--hdeg', type=int, default=90)
    parser.add_argument('--offset', type=float, default=0.0)
    parser.add_argument('--size', type=int, default=128)
    parser.add_argument('--type', type=str, default='SBS')
    parser.add_argument('--keep_name', default=False, action='store_true')

    FLAGS, unparsed = parser.parse_known_args()
    hypers = vars(FLAGS)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        save_perspectives(hypers)
