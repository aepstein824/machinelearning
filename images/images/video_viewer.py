import tensorflow as tf
import numpy as np
import skimage
import skimage.io
import skvideo.io
import matplotlib.pyplot as plt
from anytree import Node, RenderTree
import anytree

import primitives as prim
import tissue
import autoencoding as ae
import depth_perception as dp
import evaluation as ev
import hyper_manager as hm
import ml_record
import workout
import image_tfr
import datamanager
import researcher

import argparse
import time
import logging
import os
import logging
import warnings
import math
from fractions import Fraction

def experiment(name, combined_axes):
    hypers = hm.hypers
    loads = hm.hyper_loads()
    
    test_name = hypers['data_dir']

    data_shape = [hm.hypers['square_resolution'],] * 2 + [3]
    #src = datamanager.VideoSource(hypers['data_dir'], data_shape)
    src = datamanager.DescriptionSource(datamanager.one_name(hypers['data_dir']), data_shape)
    sess = workout.sess_growable()

    #recorder
    recorder = ml_record.MlRecord()
    #after this, no modification to the recorder
    record_digest = recorder.digest()
    logger = recorder.get_logger()

    '''
    rate_name = str(float(Fraction(src.vprobe['video']['@r_frame_rate'])))
    for k in src.vprobe['video'].keys():
        pass
        #print(k, src.vprobe['video'][k])
    '''

    #vwriter = skvideo.io.FFmpegWriter(recorder.out_dir() + hypers['data_dir'] + '.mp4',
    #                                  inputdict={'-r':rate_name},
    #                                  outputdict={'-r':rate_name})

    if 'depth' in loads:
        depth_hypers = ml_record.load_hard_hypers(loads['depth']) 
        with tf.variable_scope('depth') as depth_scope, hm.global_hyper_push(depth_hypers):
            model_class = getattr(dp, hm.hypers['model'])
            x = dp.Stereo(src.features['x'], src.features['xr'])
            disp, recon, loop = model_class(x)
            model = dp.DepthPerception(depth_scope, x, disp, recon, loop)
            video_op = model.video_op
            load_key = 'depth'
    else:
        load_key = None
        video_op = [src.features['x']]

    if load_key is not None:
        persistent_vars = (model.optimize_op.training_vars
                           + tf.get_collection(tf.GraphKeys.MOVING_AVERAGE_VARIABLES,
                                               model.optimize_op.train_scope.name))
        recorder.setup_save_and_board(persistent_vars, model.optimize_op.training_vars, 
                                      sess.graph)
        sess.run(tf.global_variables_initializer())
        recorder.saver.restore(sess, ml_record.check_form.format(hm.hypers['out_dir'], 
                                                                 loads[load_key]))

    src.init_feeds(sess)
    fetches = {'names':src.features['name'], 'pics':video_op}
    for i in range(math.ceil(hypers['train_chunk'] / hypers['batch_size'])):
        src.advance_feeds(sess)
        fetched = sess.run(fetches, src.t_feed)
        views = [ev.clip_and_tri(view_img) for view_img in fetched['pics']]
        horizontal = np.concatenate(views, 2)
        for j, name in enumerate(fetched['names']):
            print(str(name))
            pic_save = recorder.out_dir() + os.path.basename(str(name))[:-1]
            pic = horizontal[j]
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                skimage.io.imsave(pic_save, skimage.img_as_ubyte(pic))
        #vwriter.writeFrame(view_batch[i] * 255)

    logger.debug('Finished viewing')
    recorder.close()
    #vwriter.close()

if __name__ == '__main__':
    overrides = {
        'batch_size':64,
        'train_chunk':64,
        'max_lr':0,
        'data_dir':'tetris',
        'video':True,
        }
    researcher.run_experiments(experiment, overrides)
