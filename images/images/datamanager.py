"""Format agnostic access to image data.

Notes:
I am least proud of this module. I find it very easy to make performance
destroying mistakes with the tf data api. I will return to this module
with some kind of profiling tool and figure out what I'm doing wrong.
In addition, I'm specifying all kinds of meta data within this file,
rather than loading it off disk.

Variables:
file_pattern_form - format with data_dir to get a glob for all imgs
record_form - format with data_dir to get tf_records directory
video_form - format with data_dir to get the video file
descriptions - all current datasets

Classes:
Description - a named tuple with the necessary features for a dataset
DescriptionSource - a data source created from some of the descriptions
VideoSource - a data source created from a video file

Functions:
one_name - find the dataset for a given name
one_genre - find all datasets of a given genre
"""

import tensorflow as tf
import numpy as np
import collections
import os
import glob
import skimage
import skimage.io
import skimage.transform
import skvideo.io

import hyper_manager as hm

Description = collections.namedtuple("dataset", "name genre eyes type label_quantity ")

#TODO load dataset directory from cmd arg
file_pattern_form = "../datasets/{}/*"
record_form = "../datasets/{}/tfrecords/"
video_form = "../datasets/{}.video"

def one_name(name):
    """Return a list containing description for a named dataset"""
    return [d for d in descriptions if d.name == name][:1]

def one_genre(genre):
    """Return a list containing descriptions for a named genre"""
    return [d for d in descriptions if d.genre == genre]

def _image_load_f(fname):
    """Load an individual sample from an img file into dictionary"""
    encoded = tf.read_file(fname)
    img = tf.cast(tf.image.decode_png(encoded), dtype=tf.float32)
    return {'x':(img / 255.0), 'label':tf.zeros([1]), 'name':fname}

def _tfr_load_f(record, label_quantity):
    """Parse a tf record into a feature dictionary"""
    keys_to_features = {
        'label':tf.FixedLenFeature([], tf.int64),
        'image_raw':tf.FixedLenSequenceFeature([], tf.float32, allow_missing=True),
        'height':tf.FixedLenFeature([], tf.int64),
        'width':tf.FixedLenFeature([], tf.int64),
        'depth':tf.FixedLenFeature([], tf.int64),
        }

    parsed = tf.parse_single_example(record, keys_to_features)
    shape = tf.stack([parsed['height'], parsed['width'], parsed['depth']])
    shape = tf.squeeze(shape)
    img = tf.reshape(parsed['image_raw'], shape)
    img = tf.cond(tf.equal(parsed['depth'], 1), lambda:tf.image.grayscale_to_rgb(img), lambda:img)
    img /= 255.0
    labels = tf.reshape(tf.one_hot(parsed['label'], label_quantity), [label_quantity,])
    return {'x':img, 'labels':labels}

def _tight_load_f(record, label_quantity):
    """Unfinished: load a tf record with a tight file"""
    keys_to_features = {
        'label':tf.FixedLenFeature([], tf.int64),
        'tightened':tf.FixedLenSequenceFeature([], tf.float32, allow_missing=True),
        'info':tf.FixedLenSequenceFeature([], tf.float32, allow_missing=True),
        'height':tf.FixedLenFeature([], tf.int64),
        'width':tf.FixedLenFeature([], tf.int64),
        'depth':tf.FixedLenFeature([], tf.int64),
        }

    parsed = tf.parse_single_example(record, keys_to_features)
    shape = tf.stack([parsed['height'], parsed['width'], parsed['depth']])
    shape = tf.squeeze(shape)
    img = tf.reshape(parsed['tightened'], shape)
    img = tf.cond(tf.equal(parsed['depth'], 1), lambda:tf.image.grayscale_to_rgb(img), lambda:img)
    info_shape = tf.stack([parsed['height'], parsed['width'], 2])
    info = tf.reshape(parsed['info'], info_shape)
    factor = tf.constant([1, 255.0])[tf.newaxis, tf.newaxis, :,]
    info /= factor
    labels = tf.reshape(tf.one_hot(parsed['label'], label_quantity), [label_quantity,])
    return {'x':img, 'info':info, 'labels':labels}

def _stereo_split(features):
    """Split x feature into two stereo images"""
    replaced = dict(features)
    img = features['x']
    midpoint = tf.shape(img)[1] // 2
    left = img[:, 0:midpoint, :]
    right = img[:, midpoint:2 * midpoint, :]
    replaced['x'] = left
    replaced['xr'] = right
    return replaced

def _description_to_datasets(description):
    """Convert a list of descriptions to tf data api streams"""
    if description.type == 'images':
        dir_name = file_pattern_form.format(description.name)
        t_and_v = tf.data.Dataset.list_files(dir_name, shuffle=True)
        # There is a world where it would be desirable to have a deterministic split between 
        # the training and validation set. However, since list_files is non_deterministic even
        # without the shuffle option, for now I'm taking the easy way out and shuffling.
        total = len(glob.glob(dir_name))
        if not hm.hypers['video']:
            division_point = int(total * 0.9)
            tset = t_and_v.take(division_point)
            vset = t_and_v.skip(division_point)
        else:
            tset = t_and_v
            vset = t_and_v
        load_f = lambda x: _image_load_f(x)
    elif description.type == 'tfr':
        dir_name = record_form.format(description.name)
        tset = tf.data.TFRecordDataset(dir_name + 'train')
        vset = tf.data.TFRecordDataset(dir_name + 'validate')
        load_f = lambda x: _tfr_load_f(x, description.label_quantity)
    elif description.type == 'tight':
        dir_name = record_form.format(description.name)
        tset = tf.data.TFRecordDataset(dir_name + 'train')
        vset = tf.data.TFRecordDataset(dir_name + 'validate')
        load_f = lambda x: _tight_load_f(x, description.label_quantity)
    def file_to_image_f(ds):
        loaded = ds.map(load_f)
        if description.eyes == 'stereo':
            loaded = loaded.map(_stereo_split)
        return loaded
    return tset, vset, file_to_image_f 


class DescriptionSource:
    """A source created from a list of descriptions""" 

    def __init__(self, descriptions, shape):
        """Initialize from a list of descriptions

        Arguments:
        descriptions - a list of descriptions
        shape - output shape per sample

        Notes:
        The real meat of the result is the features dictionary, which
        contains keys for input images as well as labels and other
        meta data.
        """
        self.shape = shape
        self.label_quantity = descriptions[0].label_quantity
        self.type = descriptions[0].type

        t_acc = None
        v_acc = None
        for d in descriptions:
            if ((d.label_quantity != self.label_quantity)
                or (d.type != self.type)):
                raise ValueError('all descriptions must have same label and file type')
            tset, vset, file_to_image_f = _description_to_datasets(d)
            if t_acc is None:
                t_acc = tset
                v_acc = vset
            else:
                t_acc.concatenate(tset)
                v_acc.concatenate(vset)

        def flat_to_shuffled_repeat(ds):
            """Apply the shuffle and repeat transforms"""
            return ds.shuffle(buffer_size=hm.hypers['batch_size']).repeat()

        def load_batch_resize(ds):
            """Apply resize, batching, and prefetch transformations"""
            def resize_f(features):
                imgs = features['x']
                # TODO align corners? consider switching implementations because TFs is bad
                def resize_individual(x):
                    if hm.hypers['resize']:
                        x = tf.image.resize_bilinear(x, shape[:2])
                    return tf.reshape(x, (-1,) + tuple(shape))
                replaced = dict(features)
                if 'xr' in features:
                    # tuple of xl, xr
                    replaced['x'], replaced['xr'] = [resize_individual(x) 
                                                     for x in (features['x'], features['xr'])]
                else:
                    replaced['x'] = resize_individual(imgs)
                return replaced
            batched = file_to_image_f(ds).batch(
                hm.hypers['batch_size']).map(resize_f).prefetch(1)
            # if I understand correctly, one batch should be enough to keep pipelines busy
            return batched 
        
        def prepare_f(ds):
            return load_batch_resize(flat_to_shuffled_repeat(ds))

        self.t_set = prepare_f(t_acc)
        self.v_set = prepare_f(v_acc)

        self.t_iter = self.t_set.make_one_shot_iterator()
        self.v_iter = self.v_set.make_one_shot_iterator()

        self.iter_handle = tf.placeholder(tf.string, shape=[])

        self.iter = tf.data.Iterator.from_string_handle(self.iter_handle, 
                                                        self.t_set.output_types, 
                                                        self.t_set.output_shapes)
        self.features = self.iter.get_next()

    def init_feeds(self, sess):
        """Initialize a the feed dictionary once a session exists"""
        self.t_handle = sess.run(self.t_iter.string_handle())
        self.v_handle = sess.run(self.v_iter.string_handle())
        self.t_feed = {self.iter_handle:self.t_handle}
        self.v_feed = {self.iter_handle:self.v_handle}

    def advance_feeds(self, sess):
        """Do nothing"""
        pass

class VideoSource():
    """Unfinished"""
    def __init__(self, name, shape):
        self.video_name = video_form.format(name) 
        skvideo._FFMPEG_SUPPORTED_DECODERS.append(str.encode('.video'))
        self.vreader = skvideo.io.FFmpegReader(self.video_name)
        self.vprobe = skvideo.io.ffprobe(self.video_name)
        self.frame_generator = self.vreader.nextFrame()
        self.img_shape = shape
        self.batch_shape = [hm.hypers['batch_size']] + list(shape)
        self.x = tf.placeholder(dtype=tf.float32, shape=self.batch_shape)
        self.t_feed = {}
        self.v_feed = {}
        self.more_video = True
    def init_feeds(self, sess):
        pass
    def advance_feeds(self, sess):
        next_batch = np.zeros(self.batch_shape, dtype=np.dtype(float))
        for i in range(hm.hypers['batch_size']):
            try:
                next_frame = next(self.frame_generator)
                fit_frame = skimage.transform.resize(next_frame, self.img_shape,
                                                     mode='reflect')
                next_batch[i] = fit_frame
            except(StopIteration):
                self.more_video = False
                break
        self.t_feed = {self.x: next_batch}
        self.v_feed = self.t_feed


descriptions = [
Description("adult128img", "live", "mono", "images", 0),
Description("adult128tight", "live", "mono", "tight", 0),
Description("jojo128", "anime", "mono", "images", 0),
Description("mnist", "mnist", "mono", "tfr", 10),
Description("fashion", "mnist", "mono", "tfr", 10),
Description("cifar10", "tiny", "mono", "tfr", 10),
Description("sbs128", "live", "stereo", "images", 0),
Description("guinea", "live", "stereo", "images", 0),
Description("guinea2", "live", "stereo", "images", 0),
Description("sbs256", "live", "stereo", "images", 0),
Description("apple_cider_spider", "apple2", "mono", "images", 0),
Description("the_oregaon_trail", "apple2", "mono", "images", 0),
Description("bionic_commando", "acracde", "mono", "images", 0),
Description("donkey_kong", "arcade", "mono", "images", 0),
Description("double_dragon", "arcade", "mono", "images", 0),
Description("dragon_unit", "arcade", "mono", "images", 0),
Description("final_fight", "arcade", "mono", "images", 0),
Description("ganryu", "arcade", "mono", "images", 0),
Description("ghosts_n_goblins", "arcade", "mono", "images", 0),
Description("ghouls_n_ghosts", "arcade", "mono", "images", 0),
Description("golden_axe", "arcade", "mono", "images", 0),
Description("jail_break", "arcade", "mono", "images", 0),
Description("karate_champ", "arcade", "mono", "images", 0),
Description("magician_lord", "arcade", "mono", "images", 0),
Description("marvel_super_heroes_vs_street_fighter", "arcade", "mono", "images", 0),
Description("marvel_vs_capcom", "arcade", "mono", "images", 0),
Description("mercs", "arcade", "mono", "images", 0),
Description("metal_slug", "arcade", "mono", "images", 0),
Description("metal_slug_3", "arcade", "mono", "images", 0),
Description("metal_slug_4", "arcade", "mono", "images", 0),
Description("metal_slug_5", "arcade", "mono", "images", 0),
Description("metal_slug_x", "arcade", "mono", "images", 0),
Description("michael_jacksons_moonwalker", "arcade", "mono", "images", 0),
Description("neo_turf_masters", "arcade", "mono", "images", 0),
Description("osman", "arcade", "mono", "images", 0),
Description("punch_out", "arcade", "mono", "images", 0),
Description("shinobi", "arcade", "mono", "images", 0),
Description("street_fighter_3_alpha", "arcade", "mono", "images", 0),
Description("street_fighter_2", "arcade", "mono", "images", 0),
Description("street_fighter_zero_3", "arcade", "mono", "images", 0),
Description("super_puzzle_fighter_2_turbo", "arcade", "mono", "images", 0),
Description("super_street_fighter_2_turbo", "arcade", "mono", "images", 0),
Description("tapper", "arcade", "mono", "images", 0),
Description("teenage_mutant_ninja_turtles", "arcade", "mono", "images", 0),
Description("the_king_of_fighters_2001", "arcade", "mono", "images", 0),
Description("xmen_vs_street_fighter", "arcade", "mono", "images", 0),
Description("dodge_em", "a2600", "mono", "images", 0),
Description("hero", "a2600", "mono", "images", 0),
Description("pitfall_2", "a2600", "mono", "images", 0),
Description("princess_rescue", "a2600", "mono", "images", 0),
Description("seaquest", "a2600", "mono", "images", 0),
Description("choplifter", "a7800", "mono", "images", 0),
Description("batman_returns", "lynx", "mono", "images", 0),
Description("blockout", "lynx", "mono", "images", 0),
Description("gordo_106", "lynx", "mono", "images", 0),
Description("krazy_ace_miniature_golf", "lynx", "mono", "images", 0),
Description("pac_land", "lynx", "mono", "images", 0),
Description("scrapyard_dog", "lynx", "mono", "images", 0),
Description("bcs_quest_for_tires", "coleco", "mono", "images", 0),
Description("dragonfire", "coleco", "mono", "images", 0),
Description("frogger_2", "coleco", "mono", "images", 0),
Description("jumpman_junior", "coleco", "mono", "images", 0),
Description("oils_well", "coleco", "mono", "images", 0),
Description("zaxxon", "coleco", "mono", "images", 0),
Description("batman", "c64", "mono", "images", 0),
Description("c64anabalt", "c64", "mono", "images", 0),
Description("congo_bongo", "c64", "mono", "images", 0),
Description("diamond_mine", "c64", "mono", "images", 0),
Description("frogger", "c64", "mono", "images", 0),
Description("jungle_hunt", "c64", "mono", "images", 0),
Description("the_great_giana_sisters", "c64", "mono", "images", 0),
Description("doom_2", "doom", "mono", "images", 0),
Description("final_doom", "doom", "mono", "images", 0),
Description("the_ultimate_doom_episode_2", "doom", "mono", "images", 0),
Description("the_ultimate_doom_episode_3", "doom", "mono", "images", 0),
Description("the_ultimate_doom_episode_4", "doom", "mono", "images", 0),
Description("alien_carnage", "dos", "mono", "images", 0),
Description("bio_menace", "dos", "mono", "images", 0),
Description("cd_man", "dos", "mono", "images", 0),
Description("commander_keen", "dos", "mono", "images", 0),
Description("commander_keen_episode_2", "dos", "mono", "images", 0),
Description("commander_keen_episode_3", "dos", "mono", "images", 0),
Description("commander_keen_episode_4", "dos", "mono", "images", 0),
Description("commander_keen_episode_5", "dos", "mono", "images", 0),
Description("commander_keen_episode_6", "dos", "mono", "images", 0),
Description("crystal_cave", "dos", "mono", "images", 0),
Description("dgeneration", "dos", "mono", "images", 0),
Description("duke_nukem_episode_1", "dos", "mono", "images", 0),
Description("duke_nukem_episode_2", "dos", "mono", "images", 0),
Description("duke_nukem_episode_3", "dos", "mono", "images", 0),
Description("epic_pinball_enigma", "dos", "mono", "images", 0),
Description("epic_pinball_super_android", "dos", "mono", "images", 0),
Description("heros_quest", "dos", "mono", "images", 0),
Description("hocus_pocus_episode_1", "dos", "mono", "images", 0),
Description("hocus_pocus_episode_2", "dos", "mono", "images", 0),
Description("jazz_jackrabbit", "dos", "mono", "images", 0),
Description("jazz_jackrabbit_holiday_hare", "dos", "mono", "images", 0),
Description("jetpack", "dos", "mono", "images", 0),
Description("jill_of_the_jungle_volume_1", "dos", "mono", "images", 0),
Description("jill_of_the_jungle_volume_2", "dos", "mono", "images", 0),
Description("mystic_towers", "dos", "mono", "images", 0),
Description("nitemare_3d", "dos", "mono", "images", 0),
Description("quest_for_glory_2", "dos", "mono", "images", 0),
Description("sid_meiers_railroad_tycoon", "dos", "mono", "images", 0),
Description("space_quest_chapter_1", "dos", "mono", "images", 0),
Description("space_quest_2_chapter_2", "dos", "mono", "images", 0),
Description("space_quest_4", "dos", "mono", "images", 0),
Description("the_amazing_spiderman", "dos", "mono", "images", 0),
Description("wolfenstein_3d_episode_1", "dos", "mono", "images", 0),
Description("wolfenstein_3d_episode_2", "dos", "mono", "images", 0),
Description("wolfenstein_3d_episode_3", "dos", "mono", "images", 0),
Description("wolfenstein_3d_episode_4", "dos", "mono", "images", 0),
Description("wolfenstein_3d_episode_5", "dos", "mono", "images", 0),
Description("wolfenstein_3d_episode_6", "dos", "mono", "images", 0),
Description("action_man", "gbc", "mono", "images", 0),
Description("asterix", "gbc", "mono", "images", 0),
Description("baby_felix_halloween", "gbc", "mono", "images", 0),
Description("bomberman_quest", "gbc", "mono", "images", 0),
Description("croc_2", "gbc", "mono", "images", 0),
Description("daffy_duck", "gbc", "mono", "images", 0),
Description("evel_knievel", "gbc", "mono", "images", 0),
Description("harry_potter_and_the_chamber_of_secrets", "gbc", "mono", "images", 0),
Description("keitai_denjuu_telefang", "gbc", "mono", "images", 0),
Description("looney_tunes", "gbc", "mono", "images", 0),
Description("mega_man_xtreme", "gbc", "mono", "images", 0),
Description("mega_man_xtreme_2", "gbc", "mono", "images", 0),
Description("pocket_bomberman", "gbc", "mono", "images", 0),
Description("pokemon_gold", "gbc", "mono", "images", 0),
Description("pokemon_yellow", "gbc", "mono", "images", 0),
Description("pokemon_puzzle_challenge", "gbc", "mono", "images", 0),
Description("rockman_dx3", "gbc", "mono", "images", 0),
Description("shantae", "gbc", "mono", "images", 0),
Description("spongebob_squarepants", "gbc", "mono", "images", 0),
Description("super_mario_bros_deluxe", "gbc", "mono", "images", 0),
Description("tetris_dx", "gbc", "mono", "images", 0),
Description("the_legend_of_zelda_links_awakening", "gbc", "mono", "images", 0),
Description("the_legend_of_zelda_oracle_of_ages", "gbc", "mono", "images", 0),
Description("the_legend_of_zelda_oracle_of_seasons", "gbc", "mono", "images", 0),
Description("tintin_prisoner_of_the_sun", "gbc", "mono", "images", 0),
Description("tintin_in_tibet", "gbc", "mono", "images", 0),
Description("toki_tori", "gbc", "mono", "images", 0),
Description("wario_land_2", "gbc", "mono", "images", 0),
Description("zook_hero_z", "gbc", "mono", "images", 0),
Description("a_boy_and_his_blob", "gb", "mono", "images", 0),
Description("avenging_spirit", "gb", "mono", "images", 0),
Description("batman_return_of_the_joker", "gb", "mono", "images", 0),
Description("battletoads", "gb", "mono", "images", 0),
Description("bubble_ghost", "gb", "mono", "images", 0),
Description("castlevania_2", "gb", "mono", "images", 0),
Description("contra", "gb", "mono", "images", 0),
Description("disneys_duck_tales", "gb", "mono", "images", 0),
Description("donkey_kong_land", "gb", "mono", "images", 0),
Description("final_fantasy_adventure", "gb", "mono", "images", 0),
Description("fortified_zone", "gb", "mono", "images", 0),
Description("go_go_tank", "gb", "mono", "images", 0),
Description("gremlins_2", "gb", "mono", "images", 0),
Description("jurassic_park_part_2", "gb", "mono", "images", 0),
Description("kid_dracula", "gb", "mono", "images", 0),
Description("kirbys_dream_land", "gb", "mono", "images", 0),
Description("kirbys_dream_land_2", "gb", "mono", "images", 0),
Description("kirbys_pinball_land", "gb", "mono", "images", 0),
Description("last_action_hero", "gb", "mono", "images", 0),
Description("mega_man_dr_wilys_revenge", "gb", "mono", "images", 0),
Description("mega_man_2", "gb", "mono", "images", 0),
Description("mega_man_4", "gb", "mono", "images", 0),
Description("mega_man_5", "gb", "mono", "images", 0),
Description("metroid_2", "gb", "mono", "images", 0),
Description("mickeys_dangerous_chase", "gb", "mono", "images", 0),
Description("mortal_kombat_2", "gb", "mono", "images", 0),
Description("ninja_gaiden_shadow", "gb", "mono", "images", 0),
Description("operatioc_c", "gb", "mono", "images", 0),
Description("pac_in_time", "gb", "mono", "images", 0),
Description("pingu", "gb", "mono", "images", 0),
Description("popeye_2", "gb", "mono", "images", 0),
Description("super_mario_land", "gb", "mono", "images", 0),
Description("super_mario_land_2", "gb", "mono", "images", 0),
Description("teenage_mutant_ninja_turtles_3", "gb", "mono", "images", 0),
Description("tetris", "gb", "mono", "images", 0),
Description("the_adventure_of_star_saver", "gb", "mono", "images", 0),
Description("the_blues_brothers", "gb", "mono", "images", 0),
Description("the_smurfs", "gb", "mono", "images", 0),
Description("tiny_toons_adventures", "gb", "mono", "images", 0),
Description("trip_world", "gb", "mono", "images", 0),
Description("uchuu_no_kishi_tekkaman_blade", "gb", "mono", "images", 0),
Description("wario_land", "gb", "mono", "images", 0),
Description("yuu_yuu_hakusho", "gb", "mono", "images", 0),
Description("mole_mania", "sgb", "mono", "images", 0),
Description("tetris_blast", "sgb", "mono", "images", 0),
Description("007_agent_under_fire", "gc", "mono", "images", 0),
Description("007_nightfire", "gc", "mono", "images", 0),
Description("billy_hatcher", "gc", "mono", "images", 0),
Description("disneys_extreme_skate_adventure", "gc", "mono", "images", 0),
Description("ed_edd_n_eddy", "gc", "mono", "images", 0),
Description("final_fantasy_crystal_chronicles", "gc", "mono", "images", 0),
Description("froggers_adventure", "gc", "mono", "images", 0),
Description("ikaruga", "gc", "mono", "images", 0),
Description("luigis_mansion", "gc", "mono", "images", 0),
Description("mortal_kombat", "gc", "mono", "images", 0),
Description("pac_man_world_2", "gc", "mono", "images", 0),
Description("paper_mario", "gc", "mono", "images", 0),
Description("rayman_3", "gc", "mono", "images", 0),
Description("resident_evil_4", "gc", "mono", "images", 0),
Description("sonic_adventure_2", "gc", "mono", "images", 0),
Description("sonic_adventure", "gc", "mono", "images", 0),
Description("spyro", "gc", "mono", "images", 0),
Description("super_monkey_ball", "gc", "mono", "images", 0),
Description("super_monkey_ball_2", "gc", "mono", "images", 0),
Description("super_smash_bros_melee", "gc", "mono", "images", 0),
Description("tony_hawk_pro_skater_3", "gc", "mono", "images", 0),
Description("tony_hawk_underground", "gc", "mono", "images", 0),
Description("tony_hawk_underground_2", "gc", "mono", "images", 0),
Description("zapper", "gc", "mono", "images", 0),
Description("advance_wars_2", "gba", "mono", "images", 0),
Description("alien_hominid", "gba", "mono", "images", 0),
Description("asterix_and_obelisk", "gba", "mono", "images", 0),
Description("banjo_kazooie", "gba", "mono", "images", 0),
Description("castlevania_aria_of_sorrow", "gba", "mono", "images", 0),
Description("digimon_sapphire", "gba", "mono", "images", 0),
Description("lion_king", "gba", "mono", "images", 0),
Description("dk_king_of_swing", "gba", "mono", "images", 0),
Description("db_advanced_adventure", "gba", "mono", "images", 0),
Description("dbz_legacy_goku", "gba", "mono", "images", 0),
Description("drill_dozer", "gba", "mono", "images", 0),
Description("ffta", "gba", "mono", "images", 0),
Description("fire_emblem_fnt", "gba", "mono", "images", 0),
Description("fire_emblem_sacred_stones", "gba", "mono", "images", 0),
Description("futari_wa_precure_max_heart", "gba", "mono", "images", 0),
Description("garfield_nine_lives", "gba", "mono", "images", 0),
Description("gunstar_super_heroes", "gba", "mono", "images", 0),
Description("hp_sorcerer_stone", "gba", "mono", "images", 0),
Description("hey_arnold", "gba", "mono", "images", 0),
Description("hikaru_no_go", "gba", "mono", "images", 0),
Description("kao_the_kangaroo", "gba", "mono", "images", 0),
Description("keitai_denjuu_telefang_2", "gba", "mono", "images", 0),
Description("kingdom_hearts_chain_of_memories", "gba", "mono", "images", 0),
Description("kingdom_hearts_chain_of_memories_rebirth", "gba", "mono", "images", 0),
Description("kirby_amazing_mirror", "gba", "mono", "images", 0),
Description("kuru_kuru_kururin", "gba", "mono", "images", 0),
Description("kururin_paradise", "gba", "mono", "images", 0),
Description("lego_bionicle", "gba", "mono", "images", 0),
Description("madascar", "gba", "mono", "images", 0),
Description("mario_pinball_land", "gba", "mono", "images", 0),
Description("mario_vs_donkey_kong", "gba", "mono", "images", 0),
Description("mega_man_battle_network", "gba", "mono", "images", 0),
Description("mega_man_zero", "gba", "mono", "images", 0),
Description("mega_man_zero_2", "gba", "mono", "images", 0),
Description("mega_man_zero_3", "gba", "mono", "images", 0),
Description("mega_man_zero_4", "gba", "mono", "images", 0),
Description("metal_slug_advance_100", "gba", "mono", "images", 0),
Description("zero_mission_100", "gba", "mono", "images", 0),
Description("mortal_kombat_tournament", "gba", "mono", "images", 0),
Description("mortal_kombat_advance", "gba", "mono", "images", 0),
Description("mr_driller_2", "gba", "mono", "images", 0),
Description("ninja_five_o", "gba", "mono", "images", 0),
Description("oddworld", "gba", "mono", "images", 0),
Description("pokemon_emerald", "gba", "mono", "images", 0),
Description("pokemon_fire_red", "gba", "mono", "images", 0),
Description("pokemon_ruby", "gba", "mono", "images", 0),
Description("prince_of_persia", "gba", "mono", "images", 0),
Description("hoodlums_revenge", "gba", "mono", "images", 0),
Description("rayman_advance", "gba", "mono", "images", 0),
Description("shining_soul", "gba", "mono", "images", 0),
Description("silent_scope", "gba", "mono", "images", 0),
Description("snood", "gba", "mono", "images", 0),
Description("sonic_advance", "gba", "mono", "images", 0),
Description("sonic_advance_tails", "gba", "mono", "images", 0),
Description("sonic_advacne_2", "gba", "mono", "images", 0),
Description("sonic_advance_3", "gba", "mono", "images", 0),
Description("spider_man_3", "gba", "mono", "images", 0),
Description("spongebob_supersonge", "gba", "mono", "images", 0),
Description("spyro_attack_of_the_rhynocs", "gba", "mono", "images", 0),
Description("super_mario_advance_peach_mode", "gba", "mono", "images", 0),
Description("super_mario_advance", "gba", "mono", "images", 0),
Description("super_mario_advance_2", "gba", "mono", "images", 0),
Description("super_mario_advance_4", "gba", "mono", "images", 0),
Description("super_monkey_ball_jr", "gba", "mono", "images", 0),
Description("tekken_advance", "gba", "mono", "images", 0),
Description("legend_of_spyro_eternal_night", "gba", "mono", "images", 0),
Description("legend_of_zelda_link_to_the_past_palace_of_four_swords", "gba", "mono", "images", 0),
Description("spongebob_movie", "gba", "mono", "images", 0),
Description("tony_hawk_pro_skater_2", "gba", "mono", "images", 0),
Description("turbo_turtle_adventure", "gba", "mono", "images", 0),
Description("turok_evolution", "gba", "mono", "images", 0),
Description("wario_land_4", "gba", "mono", "images", 0),
Description("xmen_official_game", "gba", "mono", "images", 0),
Description("x2_wolverines_revenge", "gba", "mono", "images", 0),
Description("zook_man_zx4", "gba", "mono", "images", 0),
Description("arumana_no_kiseki", "fds", "mono", "images", 0),
Description("otocky", "fds", "mono", "images", 0),

]