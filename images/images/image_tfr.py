import tensorflow as tf
import numpy  as np

record_form = "../{}/tfrecords/{}"

def dataset_from_record_name(fname, shape, batch_size, label_quantity):
    dataset = tf.data.TFRecordDataset(fname)

    def parser(record):
        keys_to_features = {
            'label':tf.FixedLenFeature(1, tf.int64),
            'image_raw':tf.FixedLenFeature(np.prod(shape), tf.float32),
            'height':tf.FixedLenFeature((), tf.int64, default_value=shape[0]),
            'width':tf.FixedLenFeature((), tf.int64, default_value=shape[1]),
            'depth':tf.FixedLenFeature((), tf.int64, default_value=shape[2]),
            }
        parsed = tf.parse_single_example(record, keys_to_features)
        img = parsed['image_raw'] / 255.0
        img = tf.reshape(img, shape)
        labels = tf.reshape(tf.one_hot(parsed['label'], label_quantity), [label_quantity,])
        return (img, labels)

    dataset = (dataset.map(parser)
               .cache()
               .repeat()
               .shuffle(buffer_size=20000)
               .batch(batch_size)
               .prefetch(10)
               )


    return dataset

def set_and_iter(set_name, data_shape, hypers, label_quantity=1):
    record = record_form.format(hypers['data_dir'], set_name)
    ds = dataset_from_record_name(record, data_shape, hypers["batch_size"], label_quantity)
    iter = ds.make_one_shot_iterator()

    return ds, iter
