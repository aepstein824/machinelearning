"""Methods for viewing images.

Functions:
imsave_ignore - save an image, ignoring the precision loss warning
slerp - spherical interpolation
rootest_factor - find the largest factor of a number
ceil_factor - sqrt rounded up
tower_to_square - split a column of images to reshape into a square
clip_and_tri - ensure that an image has three channels and fits in 0-1
expand_scalars_pile - 
"""

import tensorflow as tf
import numpy as np
import skimage.io
from numpy.linalg import norm
import warnings
import math

import hyper_manager as hm

def imsave_ignore(path_name, image):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        skimage.io.imsave(path_name, skimage.img_as_ubyte(image))

def slerp(p0, p1, t):
    flat0 = np.ravel(p0)
    flat1 = np.ravel(p1)
    product = np.dot(flat0 / norm(flat0), flat1 / norm(flat1))
    omega = np.arccos(product * 0.9999) + 1e-5
    so = np.sin(omega)
    slerped = np.sin((1.0-t)*omega) / so * p0 + np.sin(t*omega)/so * p1
    return slerped

def rootest_factor(n):
    f = int(math.floor(math.sqrt(n)))
    while f > 1:
        if n % f == 0:
            return f
        f = f - 1
    return 1

def ceil_root(x):
    return math.ceil(math.sqrt(x))

def tower_to_squareish(x, split_dim):
    sqr_factor = ceil_root(split_dim)
    atom = sqr_factor * hm.hypers['square_resolution']
    target = math.ceil(x.shape[0] / atom) * atom 
    padded = np.pad(x, [[0, target - x.shape[0]], [0, 0], [0, 0]], 'constant')
    cut_rows = np.split(padded, sqr_factor, axis=0)
    return np.concatenate(cut_rows, 1)

def clip_and_tri(x):
    x = np.clip(x, 0, 1)
    if len(x.shape) == 3:
        x = x[..., np.newaxis]
    return np.pad(x, [(0, 0), (0, 0), (0, 0), (0, 3 - x.shape[3])], mode='maximum')

def expand_scalars_pile(x):
    x = tf.abs(x)
    while len(x.get_shape()) < 4:
        x = tf.expand_dims(x, -1)
    h, w = x.get_shape().as_list()[1:3]
    if x.get_shape().as_list()[3] > 3:
        x = tf.reduce_mean(x, axis=3, keepdims=True)
    aspect =  w / h
    target_h = hm.hypers['square_resolution']
    size = [target_h, math.ceil(aspect * target_h)]
    return tf.image.resize_nearest_neighbor(x, size)

def rescale_unit(unscaled):
    #TODO replace with skimage.exposure
    scale = unscaled.max() - unscaled.min()
    if scale == 0:
        # doesn't really matter what happens here, output should be all 0s
        scale = 1
    return (unscaled - unscaled.min()) / scale

def picture_f(pres, i):
    views = [clip_and_tri(view_img) for view_img in pres]
    comparison = np.concatenate(views, 2)

    comparison = np.reshape(comparison, [-1, comparison.shape[2], 3])
    square_scale = int(math.ceil(hm.hypers['batch_size'] / len(views)))
    comparison = tower_to_squareish(comparison, square_scale)
    name = "{}_{}.png".format(hm.hypers["data_dir"], i)
    return comparison, name

def tighten_img(test_img, w, keep=0.25):
    test_shape = test_img.shape
    differences = np.sum(np.abs(test_img[:,1:,:] - test_img[:,:-1,:]), axis=2)
    acc_diff = np.cumsum(differences, axis=1)
    def cut_range(i, j, r, n):
        if (n > (j - i + 1)):
            print(n, j, i)
            assert(False)
        # i and j are inclusive
        # alloc is the quanity each recieves
        # i and j can be equal if n == 1
        small = acc_diff[r, i]
        big = acc_diff[r, j]
        mid = (big + small) / 2
        first_alloc = n // 2
        last_alloc = n // 2 + n % 2
        last_i = j - last_alloc + 1
        for s in range(i + first_alloc + 1, last_i + 1):
            if acc_diff[r, s] > mid:
                if n == 1:
                    return [s]
                else:
                    return (cut_range(i, s - 1, r, first_alloc) 
                            + cut_range(s, j, r, last_alloc))
        if n == 1:
            # we searched, but did not find a good place to place the one divider, meaning
            # that all the energy is focused at the end
            return [last_i]
        else:
            return cut_range(i, last_i - 1, r, first_alloc) + list(range(last_i, j + 1))
    sampled = np.zeros([test_shape[0], w, test_shape[2]])
    info = np.zeros([test_shape[0], w, 2])
    for r in range(test_shape[0]):
        if r > 0:
            acc_diff[r] =  keep * acc_diff[r] + (1 - keep) * acc_diff[r - 1]
        cut = cut_range(0, test_shape[1] - 2, r, w - 1)
        last = 0
        for i, c in enumerate(cut + [test_shape[1]]):
            averaged = np.mean(test_img[r, last:c, :], axis=0)
            sampled[r, i, :] = averaged
            info[0] = np.std(test_img[r, last:c, :]) # all channels
            info[1] = (last + c) / (2 * test_shape[1]) * 2 - 1
            # increment 
            last = c
    return sampled, info

def hoz_adapt(test_batch, w):
    return np.stack([tighten_img(b, w)[0] for b in test_batch])

