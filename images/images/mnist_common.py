import tensorflow as tf
import primitives as prim

def best_allconv(input):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)

    filters1 = 32 
    filters2 = 64 
    filtersFC = filters2 

    all_conv = [lambda x: tf.reshape(x, [-1, 28, 28, 1]),
                lambda x: tf.nn.dropout(x, kdrop_input),
                prim.conv_proto((5, 5, 1, filters1), "conv1"),
                prim.conv_proto((3, 3, filters1, filters1), "cpool1", stride=2),
                lambda x: tf.nn.dropout(x, kdrop_pool),
                prim.conv_proto((5, 5, filters1, filters2), "conv2"),
                prim.conv_proto((3, 3, filters2, filters2), "cpool2", stride=2),
                lambda x: tf.nn.dropout(x, kdrop_pool),
                prim.conv_proto((3, 3, filters2, filtersFC), "conv3"),
                prim.conv_proto((1, 1, filtersFC, filtersFC), "cfc1"),
                prim.conv_proto((1, 1, filtersFC, 10), "cfc2"),
                prim.avgpool_proto(7, "avg"),
                lambda x: tf.reshape(x, (-1, 10)), 
                ]

    y = prim.chain_layers(x_image, all_conv) 

    return y, kdrop_input, kdrop_pool

def fashion_allconv(x_image):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)

    filters1 =  64 
    filters2 = 128 
    filtersFC = filters2 

    all_conv = [lambda x: tf.reshape(x, [-1, 28, 28, 1]),
                lambda x: tf.nn.dropout(x, kdrop_input),
                prim.conv_proto((5, 5, 1, filters1), "conv1"),
                prim.conv_proto((3, 3, filters1, filters1), "cpool1", stride=2),
                lambda x: tf.nn.dropout(x, kdrop_pool),
                prim.conv_proto((5, 5, filters1, filters2), "conv2"),
                prim.conv_proto((3, 3, filters2, filters2), "cpool2", stride=2),
                lambda x: tf.nn.dropout(x, kdrop_pool),
                prim.conv_proto((3, 3, filters2, filtersFC), "conv3"),
                prim.conv_proto((1, 1, filtersFC, filtersFC), "cfc1"),
                prim.conv_proto((1, 1, filtersFC, 10), "cfc2"),
                prim.avgpool_proto(7, "avg"),
                lambda x: tf.reshape(x, (-1, 10)), 
                ]

    y = prim.chain_layers(x_image, all_conv) 

    return y, kdrop_input, kdrop_pool

def fashion_allconv_bn(x_image):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)
    in_training = tf.placeholder(tf.bool)

    filters1 =  64 
    filters2 = 128 
    filtersFC = filters2 

    all_conv = [lambda x: tf.reshape(x, [-1, 28, 28, 1]),
                lambda x: tf.nn.dropout(x, kdrop_input),
                in_training, prim.conv_proto((5, 5, 1, filters1), "conv1"),
                prim.conv_proto((3, 3, filters1, filters1), "cpool1", stride=2),
                lambda x: tf.nn.dropout(x, kdrop_pool),
                in_training, prim.conv_proto((5, 5, filters1, filters2), "conv2"),
                prim.conv_proto((3, 3, filters2, filters2), "cpool2", stride=2),
                lambda x: tf.nn.dropout(x, kdrop_pool),
                in_training, prim.conv_proto((3, 3, filters2, filtersFC), "conv3"),
                prim.conv_proto((1, 1, filtersFC, filtersFC), "cfc1"),
                prim.conv_proto((1, 1, filtersFC, 10), "cfc2"),
                prim.avgpool_proto(7, "avg"),
                lambda x: tf.reshape(x, (-1, 10)), 
                ]

    y = prim.chain_layers(x_image, all_conv) 

    return y, kdrop_input, kdrop_pool, in_training

def basic_auto(x_image):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)
    
    mid_layer = 50 
    sparse_layer = 3

    encoder = [prim.nn_proto(784, mid_layer, "fc1"),
               prim.nn_proto(mid_layer, mid_layer, "fc2"),
               prim.nn_proto(mid_layer, sparse_layer, "sparse"),
               ]
    enc = prim.chain_layers(x_image, encoder)

    decoder = [prim.nn_proto(sparse_layer, mid_layer, "sparset"),
               prim.nn_proto(mid_layer, mid_layer, "fc2t"),
               prim.nn_proto(mid_layer, 784, "fc1t"),
               ]
    dec = prim.chain_layers(enc, decoder)

    return enc, dec, kdrop_input, kdrop_pool 

def basic_auto_bn(x_image):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)
    bn_training = tf.placeholder(tf.bool)
    
    mid_layer = 50 
    sparse_layer = 3

    encoder = [prim.nn_proto(784, mid_layer, "fc1"),
               bn_training, prim.nn_proto(mid_layer, mid_layer, "fc2"),
               prim.nn_proto(mid_layer, sparse_layer, "sparse"),
               ]
    enc = prim.chain_layers(x_image, encoder)

    decoder = [prim.nn_proto(sparse_layer, mid_layer, "sparset"),
               prim.nn_proto(mid_layer, mid_layer, "fc2t"),
               prim.nn_proto(mid_layer, 784, "fc1t"),
               ]
    dec = prim.chain_layers(enc, decoder)

    return enc, dec, kdrop_input, kdrop_pool, bn_training


def conv_auto(x_image):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)

    filters1 = 32 
    filters2 = 32 
    filtersFC = filters2 
    sparse_depth = 8

    encoder = [
           lambda x: tf.reshape(x, [-1, 28, 28, 1]),
           lambda x: tf.nn.dropout(x, kdrop_input),
           prim.conv_proto((3, 3, 1, filters1), "conv1"),
           prim.conv_proto((3, 3, filters1, filters1), "cpool1", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           prim.conv_proto((3, 3, filters1, filters2), "conv2"),
           prim.conv_proto((3, 3, filters2, filters2), "cpool2", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           prim.conv_proto((3, 3, filters2, filtersFC), "conv3"),
           prim.conv_proto((1, 1, filtersFC, filtersFC), "cfc1"),
           prim.conv_proto((1, 1, filtersFC, sparse_depth), "cfc2"),
           prim.avgpool_proto(7, "pool_down"),
           lambda x: tf.reshape(x, [-1, 3]),
           ]
    enc = prim.chain_layers(x_image, encoder)

    decoder = [
           prim.nn_proto(sparse_depth, sparse_depth * 7 * 7, "fc_up"),
           lambda x: tf.reshape(x, (-1, 7, 7, sparse_depth)),
           prim.convT_proto((1, 1, sparse_depth, filtersFC), "cfc2t"),
           prim.convT_proto((1, 1, filtersFC, filtersFC), "cfc1t"),
           prim.convT_proto((3, 3, filtersFC, filters2), "conv3t"),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           prim.convT_proto((3, 3, filters2, filters2), "cpool2t", stride=2),
           prim.convT_proto((3, 3, filters2, filters1), "conv2t"),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           prim.convT_proto((3, 3, filters1, filters1), "cpool1t", stride=2),
           prim.convT_proto((3, 3, filters1, 1), "conv1t"),
           lambda x: tf.reshape(x, [-1, 784]),
           ]
    dec = prim.chain_layers(enc, decoder)

    return enc, dec, kdrop_input, kdrop_pool

def conv_auto_bn(x_image, hypers):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)
    bn_training = tf.placeholder(tf.bool)

    filters1 = 64 if not "f1" in hypers else hypers["f1"] 
    filters2 = 128 if not "f2" in hypers else hypers["f2"] 
    filtersFC = filters2 
    sparse_depth = 3 if not "sparse" in hypers else hypers["sparse"]

    encoder = [
           lambda x: tf.reshape(x, [-1, 28, 28, 1]),
           lambda x: tf.nn.dropout(x, kdrop_input),
           bn_training, prim.conv_proto((3, 3, 1, filters1), "conv1"),
           bn_training, prim.conv_proto((3, 3, filters1, filters1), "cpool1", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.conv_proto((3, 3, filters1, filters2), "conv2"),
           bn_training, prim.conv_proto((3, 3, filters2, filters2), "cpool2", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.conv_proto((3, 3, filters2, filtersFC), "conv3"),
           bn_training, prim.conv_proto((1, 1, filtersFC, filtersFC), "cfc1"),
           bn_training, prim.conv_proto((1, 1, filtersFC, sparse_depth), "cfc2"),
           prim.avgpool_proto(7, "pool_down"),
           lambda x: tf.reshape(x, [-1, sparse_depth]),
           ]
    enc = prim.chain_layers(x_image, encoder)

    decoder = [
           prim.nn_proto(sparse_depth, sparse_depth * 7 * 7, "fc_up"),
           lambda x: tf.reshape(x, (-1, 7, 7, sparse_depth)),
           bn_training, prim.convT_proto((1, 1, sparse_depth, filtersFC), "cfc2t"),
           bn_training, prim.convT_proto((1, 1, filtersFC, filtersFC), "cfc1t"),
           bn_training, prim.convT_proto((3, 3, filtersFC, filters2), "conv3t"),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.convT_proto((3, 3, filters2, filters2), "cpool2t", stride=2),
           bn_training, prim.convT_proto((3, 3, filters2, filters1), "conv2t"),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.convT_proto((3, 3, filters1, filters1), "cpool1t", stride=2),
           bn_training, prim.convT_proto((3, 3, filters1, 1), "conv1t"),
           lambda x: tf.reshape(x, [-1, 784]),
           ]
    dec = prim.chain_layers(enc, decoder)

    return enc, dec, kdrop_input, kdrop_pool, bn_training 

def local_conv_auto(x_image, hypers):
    kdrop_input = tf.placeholder(tf.float32)
    kdrop_pool = tf.placeholder(tf.float32)
    bn_training = tf.placeholder(tf.bool)

    filters1 = 64 if not "f1" in hypers else hypers["f1"] 
    filters2 = 128 if not "f2" in hypers else hypers["f2"] 
    filtersFC = filters2 

    encoder = [
           lambda x: tf.reshape(x, [-1, 28, 28, 1]),
           lambda x: tf.nn.dropout(x, kdrop_input),
           bn_training, prim.conv_proto((5, 5, 1, filters1), "cpool1", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.conv_proto((5, 5, filters1, filters2), "cpool2", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.conv_proto((1, 1, filters2, filtersFC), "cfc1"),
           bn_training, prim.conv_proto((1, 1, filtersFC, 1), "cfc2"),
           ]
    enc = prim.chain_layers(x_image, encoder)

    decoder = [
           bn_training, prim.convT_proto((1, 1, 1, filtersFC), "cfc2t"),
           bn_training, prim.convT_proto((1, 1, filtersFC, filters2), "cfc1t"),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.convT_proto((5, 5, filters2, filters1), "cpool2t", stride=2),
           lambda x: tf.nn.dropout(x, kdrop_pool),
           bn_training, prim.convT_proto((5, 5, filters1, 1), "cpool1t", stride=2),
           lambda x: tf.reshape(x, [-1, 784]),
           ]
    dec = prim.chain_layers(enc, decoder)

    return enc, dec, kdrop_input, kdrop_pool, bn_training 