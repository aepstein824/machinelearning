import primitives as prim
import tissue
import organs
import evaluation as ev
import ml_record
import hyper_manager as hm
import workout

import tensorflow as tf
import numpy as np
import skimage.io
from anytree import Node, RenderTree

import string

class Autoencoder:

    def __init__(self, scope, x, enc, dec, y):
        self.scope = scope
        self.x = x
        self.enc = enc
        self.dec = dec 
        self.y = y

        loss_tree = Node('loss')
        # x and y could be different, not sure we even store x
        perc = tissue.perc_diff(self.dec, y)
        encoding = Node('encoding', parent=loss_tree)
        perc.parent = encoding
        l2_factor = hm.hypers['l2_factor']
        regular = Node('regularization', parent=loss_tree, 
                       weight=l2_factor, val=prim.weight_penalty(scope))
        self.optimize_op = workout.OptimizeOp(loss_tree, scope)

def small_fc(x_image, out_channels=3):
    mid_layer    = hm.hypers.get('filter_count', 64)
    sparse_layer = hm.hypers.get('sparse', 3)

    og_count = prim.batch_to_count(x_image)
    og_shape = x_image.get_shape().as_list()
    og_shape[0] = -1
    out_count = og_shape[1] * og_shape[2] * out_channels
    out_shape = [-1, og_shape[1], og_shape[2], out_channels]

    encoder = [prim.nn_proto(mid_layer, "fc1", strat='act_only'),
               prim.nn_proto(mid_layer, "fc2"),
               prim.nn_proto(sparse_layer, "sparse", act=hm.hypers['rep_actf'], strat='act_only'),
               lambda x: x[..., tf.newaxis, tf.newaxis], # must be an image
               ]
    enc_proto = lambda x: prim.chain_layers(x, encoder)


    decoder = [prim.nn_proto(mid_layer, "sparse_t", strat='act_only'),
               prim.nn_proto(mid_layer, "fc2t"),
               prim.nn_proto(out_count, "fc1t", act=hm.hypers['image_actf'], strat='act_only'),
               lambda t: tf.reshape(t, out_shape),
               ]
    dec_proto = lambda enc: prim.chain_layers(enc, decoder)

    return enc_proto, dec_proto

def organs_test(x_image, out_channels=3):
    enc_proto = organs.u_extract_proto("test_organ", out_channels)
    dec_proto = lambda enc: tf.identity(enc)
    return enc_proto, dec_proto

def pca_fc(x_image, out_channels=3):
    mid_layer    = hm.hypers.get('filter_count', 64)
    sparse_layer = hm.hypers.get('sparse', 3)

    og_count = prim.batch_to_count(x_image)
    og_shape = x_image.get_shape().as_list()
    og_shape[0] = -1
    out_count = og_shape[1] * og_shape[2] * out_channels
    out_shape = [-1, og_shape[1], og_shape[2], out_channels]

    sparser = prim.PCA(mid_layer)
    encoder = [prim.nn_proto(mid_layer, "fc1", strat='act_only'),
               prim.nn_proto(mid_layer, "fc2"),
               sparser.encode,
               lambda x: x[..., tf.newaxis, tf.newaxis], # must be an image
               ]
    enc_proto = lambda x: prim.chain_layers(x, encoder)


    decoder = [lambda x: x[..., 0, 0],
               sparser.decode,
               prim.nn_proto(mid_layer, "fc2t"),
               prim.nn_proto(out_count, "fc1t", act=hm.hypers['image_actf'], strat='act_only'),
               lambda t: tf.reshape(t, out_shape),
               ]
    dec_proto = lambda enc: prim.chain_layers(enc, decoder)

    return enc_proto, dec_proto

def simple_downsample_conv(x_image, out_channels=3):
    fc = hm.hypers['filter_count']
    cd = hm.hypers['conv_depth']
    sparse = hm.hypers['sparse'] 


    encoder = [prim.conv_proto((3, 3, fc), 'e_filters', stride=2, groups=1)]
    encoder.extend([prim.conv_proto((3, 3, fc * (2 ** i)), "e" + str(i), stride=2) 
                    for i in range(1,cd)])
    encoder.append(prim.conv_proto((1, 1, sparse), 'e_sparse', strat='act_only', 
                                   act=hm.hypers['rep_actf']))
    enc_proto = lambda x: prim.chain_layers(x, encoder)

    decoder = [prim.convT_proto((3, 3, fc * (2 ** i)), "d" + str(i), stride=2) for i in range(cd)]
    decoder.append(prim.conv_proto((1, 1, fc * (2 ** (cd - 1))), 'd_sparse', groups=1))
    decoder = list(reversed(decoder))
    decoder.append(prim.conv_proto((1, 1, out_channels), 'd_out', strat='act_only',
                                   act=hm.hypers['image_actf']))
    dec_proto = lambda x: prim.chain_layers(x, decoder)

    return enc_proto, dec_proto

def deep_downsample_conv(x_image, out_channels=3):
    fc = hm.hypers['filter_count']
    cd = hm.hypers['conv_depth']
    rc = hm.hypers['resblock_count']
    sparse = hm.hypers['sparse'] 


    encoder = [prim.conv_proto((3, 3, fc), 'e_filters', groups=1)]
    for i in range(cd):
        encoder.append(tissue.resid_squeeze(fc * (2 ** i), "ed" + str(i)))
        encoder.append(tissue.resid_chain(fc * (2 ** i), "ep" + str(i), rc))
    encoder.append(prim.conv_proto((1, 1, sparse), 'e_sparse', strat='act_only', 
                                   act=hm.hypers['rep_actf']))
    enc_proto = lambda x: prim.chain_layers(x, encoder)

    backwards_d = []
    for i in range(cd):
        backwards_d.append(tissue.resid_chain(fc * (2 ** i), "du" + str(i), rc))
        backwards_d.append(tissue.resid_inflate(fc * (2 ** i), "dp" + str(i)))
    backwards_d.append(prim.conv_proto((1, 1, fc * (2 ** (cd - 1))), 'd_sparse', groups=1))
    decoder = list(reversed(backwards_d))
    decoder.append(prim.conv_proto((1, 1, out_channels), 'd_out', strat='act_only',
                                   act=hm.hypers['image_actf']))
    dec_proto = lambda x: prim.chain_layers(x, decoder)

    return enc_proto, dec_proto

def deep_conv_pca(x_image, out_channels=3):
    fc = hm.hypers['filter_count']
    cd = hm.hypers['conv_depth']
    rc = hm.hypers['resblock_count']
    og_shape = x_image.get_shape().as_list()
    sparse = hm.hypers['sparse'] 
    pot = lambda i : 2 ** (i)

    pca = prim.PCA([og_shape[1] // pot(cd), og_shape[2] // pot(cd), fc * pot(cd)], 
                   name='pca_deep_conv')
    encoder = [prim.conv_proto((3, 3, fc), 'e_filters', groups=1)]
    for i in range(cd):
        encoder.append(tissue.resid_squeeze(fc * pot(i+1), "ed" + str(i)))
        encoder.append(tissue.resid_chain(fc * pot(i+1), "ep" + str(i), rc))
    encoder.append(pca.encode)
    enc_proto = lambda x: prim.chain_layers(x, encoder)

    decoder = [pca.decode]
    for i in range(cd):
        decoder.append(tissue.resid_chain(fc * pot(cd - i), "du" + str(i), rc))
        decoder.append(tissue.resid_inflate(fc * pot(cd - i - 1), "dp" + str(i)))
    decoder.append(prim.conv_proto((1, 1, out_channels), 'd_out', strat='act_only',
                                   act=hm.hypers['image_actf']))
    dec_proto = lambda x: prim.chain_layers(x, decoder)

    return enc_proto, dec_proto

def deep_conv_unconstrained(x_image, out_channels=3):
    fc = hm.hypers['filter_count']
    cd = hm.hypers['conv_depth']
    rc = hm.hypers['resblock_count']
    og_shape = x_image.get_shape().as_list()
    sparse = hm.hypers['sparse'] 
    pot = lambda i : 2 ** (i)

    encoder = [prim.conv_proto((3, 3, fc), 'e_filters', groups=1)]
    for i in range(cd):
        encoder.append(tissue.resid_squeeze(fc * pot(i+1), "ed" + str(i)))
        encoder.append(tissue.resid_chain(fc * pot(i+1), "ep" + str(i), rc))
    enc_proto = lambda x: prim.chain_layers(x, encoder)

    decoder = []
    for i in range(cd):
        decoder.append(tissue.resid_chain(fc * pot(cd - i), "du" + str(i), rc))
        decoder.append(tissue.resid_inflate(fc * pot(cd - i - 1), "dp" + str(i)))
    decoder.append(prim.conv_proto((1, 1, out_channels), 'd_out', strat='act_only',
                                   act=hm.hypers['image_actf']))
    dec_proto = lambda x: prim.chain_layers(x, decoder)

    return enc_proto, dec_proto


def deep_u_fc(x_image, out_channels=3):
    #max lr ~0.01
    filters1 = hm.hypers['filter_count']
    sparse_depth = hm.hypers['sparse'] 
    resblock_count = hm.hypers['resblock_count']
    og_shape = x_image.get_shape().as_list()

    pot = lambda i : 2 ** (i+1)
    L = lambda i: string.ascii_uppercase[i]
    F = lambda i: (filters1 * pot(i))
    S = lambda i: [-1, og_shape[1] // pot(i+1), og_shape[2] // pot(i+1), F(i)]
    D = lambda i: [sparse_depth * np.prod(S(i)[1:-1])]

    def down_f(x, i):
        return tissue.resid_squeeze(F(i), "downS" + L(i))(x)
    def up_f(x, i):
        return tissue.resid_inflate(F(i - 1), "upS" + L(i))(x)

    def enc_f(x, i):
       return prim.chain_layers(x, [
           tissue.resid_chain(F(i), 'bottle' + L(i), resblock_count),
           tissue.resid_squeeze(F(i) * 2, 'enc_down1' + L(i)),
           prim.conv_proto((1, 1, sparse_depth), 'enc_down2' + L(i)),
           prim.nn_proto(sparse_depth, 'enc_fc' + L(i), act=hm.hypers['rep_actf'], 
                         strat='act_only'),
        ])
    def dec_f(x, i):
        s = S(i)
        return prim.chain_layers(x, [
           prim.nn_proto(D(i)[0], 'dec_fc' + L(i)),
           lambda x: tf.reshape(x, [-1,] + s[1:-1] + [sparse_depth,]),
           prim.conv_proto((1, 1, s[-1]*2), 'dec_up1' + L(i)),
           tissue.resid_inflate(s[-1], "dec_up2" + L(i)),
           tissue.resid_chain(F(i), 'unbottle' + L(i), resblock_count),
          ])

    down_proto, up_proto = tissue.u_proto(down_f, up_f, enc_f, dec_f)

    def enc_proto(x): 
        chained = prim.chain_layers(x, [
            prim.conv_proto((3, 3, filters1), "filter_up", strat='act_only', groups=1),
            down_proto,
            lambda middles: tf.stack(middles, axis=2)[..., tf.newaxis],
          ])
        return chained

    def dec_proto(x): 
        unstacked = tf.unstack(x[:,:,:,0], axis=2)
        dec_layers = [up_proto, 
                      prim.conv_proto((1, 1, out_channels), "filter_down", 
                                      strat='act_only', act=hm.hypers['image_actf'], groups=1) ]
        return prim.chain_layers(unstacked, dec_layers) 

    return enc_proto, dec_proto 


def deep_u_pca(x_image, out_channels=3):
    #max lr ~0.01
    filters1 = hm.hypers['filter_count']
    sparse_depth = hm.hypers['sparse'] 
    resblock_count = hm.hypers['resblock_count']
    og_shape = x_image.get_shape().as_list()

    pot = lambda i : 2 ** (i+1)
    L = lambda i: string.ascii_uppercase[i]
    F = lambda i: (filters1 * pot(i))
    S = lambda i: [-1, og_shape[1] // pot(i+2), og_shape[2] // pot(i+2), F(i)]
    D = lambda i: [sparse_depth * np.prod(S(i)[1:-1])]

    def down_f(x, i):
        return tissue.resid_squeeze(F(i), "downS" + L(i))(x)
    def up_f(x, i):
        return tissue.resid_inflate(F(i - 1), "upS" + L(i))(x)

    ipcas = [prim.PCA(D(i), sparse_depth, 'pca_' + L(i)) for i in range(hm.hypers['u_levels'])]

    def enc_f(x, i):
       return prim.chain_layers(x, [
           tissue.resid_chain(F(i), 'bottle' + L(i), resblock_count),
           tissue.resid_squeeze(F(i), 'enc_down1' + L(i)),
           tissue.resid_squeeze(sparse_depth, 'enc_down2' + L(i)),
           ipcas[i].encode,
        ])
    def dec_f(x, i):
        s = S(i)
        return prim.chain_layers(x, [
           ipcas[i].decode,
           lambda x: tf.reshape(x, [-1,] + s[1:-1] + [sparse_depth,]),
           tissue.resid_inflate(sparse_depth, "dec_up1" + L(i)),
           tissue.resid_inflate(s[-1], "dec_up2" + L(i)),
           tissue.resid_chain(F(i), 'unbottle' + L(i), resblock_count),
          ])

    down_proto, up_proto = tissue.u_proto(down_f, up_f, enc_f, dec_f)

    def enc_proto(x): 
        chained = prim.chain_layers(x, [
            prim.conv_proto((3, 3, filters1), "filter_up", strat='act_only', groups=1),
            down_proto,
            lambda middles: tf.stack(middles, axis=2)[..., tf.newaxis],
          ])
        return chained

    def dec_proto(x): 
        unstacked = tf.unstack(x[:,:,:,0], axis=2)
        dec_layers = [up_proto, 
                      prim.conv_proto((1, 1, out_channels), "filter_down", 
                                      strat='act_only', act=hm.hypers['image_actf'], groups=1) ]
        return prim.chain_layers(unstacked, dec_layers) 

    return enc_proto, dec_proto 

def clean_u(x_image, out_channels=3):
    #max lr ~0.01
    filters1 = hm.hypers['filter_count']
    sparse_depth = hm.hypers['sparse'] 
    resblock_count = hm.hypers['resblock_count']
    og_shape = x_image.get_shape().as_list()

    pot = lambda i : 2 ** (i+1)
    L = lambda i: string.ascii_uppercase[i]
    F = lambda i: (filters1 * pot(i))
    S = lambda i: [-1, og_shape[1] // pot(i), og_shape[2] // pot(i), F(i)]
    D = lambda i: [np.prod(S(i)[1:])]

    def down_f(x, i):
        return tissue.resid_squeeze(F(i), "downS" + L(i))(x)
    def up_f(x, i):
        return tissue.resid_inflate(F(i - 1), "upS" + L(i))(x)

    ipcas = [prim.PCA(D(i), sparse_depth, 'pca_' + L(i)) for i in range(hm.hypers['u_levels'])]

    def enc_f(x, i):
       return prim.chain_layers(x, [
           tissue.resid_chain(F(i), 'bottle' + L(i), resblock_count),
           ipcas[i].encode,
        ])
    def dec_f(x, i):
        s = S(i)
        return prim.chain_layers(x, [
           ipcas[i].decode,
           lambda x: tf.reshape(x, s),
           tissue.resid_chain(F(i), 'unbottle' + L(i), resblock_count),
          ])

    down_proto, up_proto = tissue.u_proto(down_f, up_f, enc_f, dec_f)

    def enc_proto(x): 
        chained = prim.chain_layers(x, [
            prim.conv_proto((3, 3, filters1), "filter_up", strat='act_only', groups=1),
            down_proto,
            lambda middles: tf.stack(middles, axis=2)[..., tf.newaxis],
          ])
        return chained

    def dec_proto(x): 
        unstacked = tf.unstack(x[:,:,:,0], axis=2)
        dec_layers = [up_proto, 
                      prim.conv_proto((1, 1, out_channels), "filter_down", 
                                      strat='act_only', act=hm.hypers['image_actf'], groups=1) ]
        return prim.chain_layers(unstacked, dec_layers) 

    return enc_proto, dec_proto 