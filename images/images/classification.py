import primitives as prim
import tissue
import evaluation as ev
import ml_record
import hyper_manager as hm
import workout

import tensorflow as tf
import numpy as np
import skimage.io
import string
from anytree import Node, RenderTree

class Classifier:
    def __init__(self, scope, x, y, labels):
        self.scope = scope
        self.y = y
        self.labels = labels

        loss_tree = Node('loss')
        classification = Node('classification', parent=loss_tree, 
                              val=prim.classification_loss(self.y, self.labels))
        l2_factor = hm.hypers['l2_factor']
        regular = Node('regularization', parent=loss_tree, 
                       weight=l2_factor, val=prim.weight_penalty(scope))

        model_predict =  tf.argmax(self.y, 1)
        correct_predict = tf.argmax(self.labels, 1)
        score = tf.equal(model_predict, correct_predict)
        accuracy = tf.reduce_mean(tf.cast(score, tf.float32)) * 100.0

        mean_node = Node('confidence', parent=loss_tree, weight=None, 
                         val=100 * tf.reduce_mean(self.y))
        accuracy_node = Node('accuracy', parent=loss_tree, weight=None, val=accuracy)
        self.optimize_op = workout.OptimizeOp(loss_tree, scope)

# Small FC Net
def small_fc(x_image, label_quantity):
    mid_layer = hm.hypers['filter_count']

    def classify(x):           
        layers = [prim.nn_proto(mid_layer, "fc1"),
                  prim.nn_proto(mid_layer, "fc2"),
                  prim.nn_proto(label_quantity, "decide", 
                                strat='act_only', act=hm.hypers['decision_actf']),
                 ]
        predictions = prim.chain_layers(x, layers)
        return predictions

    return classify 

# Basic all convolutional net
def all_conv(x_image, label_quantity):
    filters1 = hm.hypers.get('filter_count', 32)
    filters2 = filters1 * 2
    filters3 = filters2 * 2
    filtersFC = filters2 * 2

    def classify(x):
        layers = [
               prim.conv_proto((3, 3, filters1), "conv1a", strat='act_only'),
               prim.conv_proto((3, 3, filters1), "conv1b"),
               prim.conv_proto((3, 3, filters1), "cpool1", stride=2),
               prim.conv_proto((3, 3, filters2), "conv2a"),
               prim.conv_proto((3, 3, filters2), "conv2b"),
               prim.conv_proto((3, 3, filters2), "cpool2", stride=2),
               prim.conv_proto((3, 3, filters3), "conv3a"),
               prim.conv_proto((3, 3, filters3), "conv3b"),
               prim.conv_proto((1, 1, filtersFC), "cfc1a"),
               prim.conv_proto((1, 1, filtersFC), "cfc1b"),
               prim.conv_proto((1, 1, label_quantity), "cfc2", 
                               strat='act_only', act=hm.hypers['decision_actf']),
               lambda x: tf.reduce_mean(x, axis=[1, 2]),
               lambda x: tf.reshape(x, [-1, label_quantity]),
               ]
        return prim.chain_layers(x, layers)


    return classify 

def vanilla_conv(x_image, label_quantity):
    filters1 = hm.hypers['filter_count']
    filters2 = filters1 * 2 
    filtersFC =  32

    def classify(x):
        layers = [
               prim.conv_proto((3, 3, filters1), "conv1a"),
               prim.conv_proto((3, 3, filters1), "conv1b"),
               prim.conv_proto((3, 3, filters1), "conv1c"),
               prim.maxpool_proto("pool1"),
               prim.conv_proto((3, 3, filters2), "conv2a"),
               prim.conv_proto((3, 3, filters2), "conv2b"),
               prim.conv_proto((3, 3, filters2), "conv2c"),
               prim.maxpool_proto("pool2"),
               prim.nn_proto(filtersFC, "fc"),
               prim.nn_proto(label_quantity, "output"),
               lambda x: tf.reshape(x, [-1, label_quantity]),
               ]
        return prim.chain_layers(x, layers)

    return classify


# Bottleneck all convolutional net
def bottleneck_conv(x_image, label_quantity):

    filters1 = hm.hypers['filter_count']
    filters2 = filters1 * 2 
    filters3 = filters2 * 2 
    filtersFC = filters3 

    def classify(x):
        layers = [
               prim.bottle_conv_proto((3, 3, filters1), "conv1a"),
               prim.bottle_conv_proto((3, 3, filters1), "cpool1", stride=2),
               prim.bottle_conv_proto((3, 3, filters2), "conv2a"),
               prim.bottle_conv_proto((3, 3, filters2), "cpool2", stride=2),
               prim.bottle_conv_proto((3, 3, filters3), "conv3a"),
               prim.bottle_conv_proto((1, 1, filtersFC), "cfc1a"),
               prim.bottle_conv_proto((1, 1, filtersFC), "cfc1b"),
               prim.bottle_conv_proto((1, 1, label_quantity), "cfc2"),
               lambda x: tf.reduce_mean(x, axis=[1, 2]),
               lambda x: tf.reshape(x, [-1, label_quantity]),
               ]
        return prim.chain_layers(x, layers)

    return classify

# Deep residual net
# about the size described in og res net paper
def deep_residual_conv(x_image, label_quantity):
    conv_depth = hm.hypers['conv_depth']
    filters1 = hm.hypers['filter_count']
    filters2 = filters1 * 2 
    filters3 = filters2 * 2 
    filtersFC = filters3 

    def classify(x):
        layers = [ 
            prim.conv_proto((1, 1, filters1), "feature_up", strat='act_only'),
            tissue.resid_chain(filters1, "conv1", conv_depth),
            tissue.resid_squeeze(filters2, "squeeze1"),
            tissue.resid_chain(filters2, "conv2", conv_depth),
            tissue.resid_squeeze(filters3, "squeeze2"),
            tissue.resid_chain(filters3, "conv3", conv_depth),
            prim.bottle_conv_proto((1, 1, filtersFC), "cfc1"),
            prim.nn_proto(label_quantity, "fc_classify", 
            act=hm.hypers['decision_actf'], strat='act_only'),
               ]
        return prim.chain_layers(x, layers)

    return classify 

# Ensemble residual net
# Attempt 2 
def ensemble_residual_conv(x_image, label_quantity, hypers):
    mid_total = hm.hypers.get['mid_total']
    mid_choices = hm.hypers.get['mid_choices']
    filters1 = hm.hypers['filter_count']
    filters2 = filters1 * 2 
    filters3 = filters2 * 2 
    filtersFC = filters3 

    def ensemble_chain(filter_count, name):
        def ensemble_proto(input_tensor):
            cache = {}
            # n >= k > 0
            def en_helper(n, k):
                if k <= 0 or n <= 0 or k >= n:
                    # k == 0 is the base case, the others are all invalid 
                    return input_tensor
                if (n, k) in cache:
                    return cache[(n, k)]
                l = string.ascii_lowercase[n] 
                take_n_layers = [ 
                    prim.bottle_conv_proto((3, 3, filter_count), name + l), bn_training,  
                    ] 
                take_n = prim.chain_layers(en_helper(n - 1, k - 1), take_n_layers)

                if n - 1 >= k:
                    varnk = tf.add(take_n, en_helper(n - 1, k))
                else:
                    varnk = take_n
                cache[(n, k)] = varnk
                return varnk
            helped = en_helper(mid_total, mid_choices)
            return helped
        return lambda x: ensemble_proto(x)

    def squeeze(filter_count, name):
        return prim.bottle_conv_proto((2, 2, filter_count), name + 'S', stride=2)
            
    def classify(x):
        layers = [
            prim.conv_proto((1, 1, filters1), "feature_up"),
            ensemble_chain(filters1, "conv1"),
            squeeze(filters2, "squeeze1"), bn_training,
            ensemble_chain(filters2, "conv2"),
            squeeze(filters3, "squeeze2"), bn_training, 
            ensemble_chain(filters3, "conv3"),
            prim.bottle_conv_proto((1, 1, filtersFC), "cfc1"), bn_training,
            prim.nn_proto(label_quantity, "fc2", tf.identity)
           ]
        return prim.chain_layers(x, layers)
    return classify
