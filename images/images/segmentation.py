import primitives as prim
import tissue
import evaluation as ev
import ml_record

import tensorflow as tf
import numpy as np
import skimage.io
import string
from anytree import Node

class Segmentation:
    def __init__(self, x, hypers):
        # TODO a way to hyper this
        self.preprocessor = u_preprocess
        self.segmenter = linear_early_distill_segmenter
        self.distiller = simple_distiller 
        self.synthesizer = fc_compromise_4up_synthesizer 
        self.segment_grader = linear_grader 

        with tf.variable_scope("seg_scope") as self.top_scope:
            segments = self.segmenter(x, self.preprocessor, self.distiller, hypers)
            synths = []
            for i, seg in enumerate(segments):
                with tf.variable_scope("synth" + str(i) + "_scope"):
                    synths.append(self.synthesizer(seg, hypers))
            synthesized = tf.add_n([synth[0] for synth in synths])
        all = list(zip(segments, synths))
        line_of_images = [x, synthesized] + visualize_segments(all, hypers)
        self.picture = tf.concat(line_of_images, axis=2, name='segment_line')
        
        self.loss_tree = segmenter_loss(x, synthesized, self.segment_grader, all, hypers)

    def add_to_report(self, report):
        self.recorder = report
        report.add_function('preprocessor', self.preprocessor)
        report.add_function('segmenter', self.segmenter)
        report.add_function('distiller', self.distiller)
        report.add_function('synthesizer', self.synthesizer)
        report.add_function('segment_grader', self.segment_grader)

    def training_vars(self):
        return self.top_scope.trainable_variables()

    def persistent_vars(self):
        return self.top_scope.trainable_variables()

    def train_dict_base(self):
        return {}

    def val_dict_base(self):
        return {}

    def out(self):
        return self.dec

    def loss(self):
        return self.loss_tree

def segmenter_loss(x, y, segment_grader, segments, hypers):
    loss_tree = Node('loss')
    encoding_node = Node('encoding', parent=loss_tree)
    perc_node = tissue.perc_diff(x, y)
    perc_node.parent = encoding_node 
    regular_node = Node('regularization', parent=loss_tree, 
                        weight=hypers['l2_factor'], val=prim.weight_penalty())
    stencil_parent = Node('stencil', parent=loss_tree, weight=hypers['segment_grade_factor']) 

    stencils = []
    for i, seg in enumerate(segments):
        ((pigment_distilled, stencil_distilled, real_stencil), (cut, stencil, pigment)) = seg
        stencils.append(stencil)
        stencil_node = Node('s' + str(i), parent=stencil_parent) 
        fear_node = Node('fear', val=tf.reduce_mean(0.5 - tf.abs(stencil - 0.5)),
                         parent=stencil_node, weight=hypers['confidence_factor'])
        noise_node = Node('noise', weight=hypers['segment_loss_smoothness'], parent=stencil_node,
                          val = tf.sqrt(tf.reduce_mean(tissue.sobel_edges(stencil)) + 1e-8))
        grade = segment_grader(seg, hypers)
        grade_node = Node('grade', weight=grade[0], val=grade[1], parent=stencil_node)
    sum_of_stencils = tf.add_n(stencils, 'sum_of_stencils')
    sum_node = Node('sum', val=prim.difference_loss(sum_of_stencils, tf.ones_like(sum_of_stencils)),
                    parent=stencil_parent)
    return loss_tree

def visualize_segments(segments, hypers):
    sparse_layer = hypers.get('sparse', 3)

    imgs = []
    for segment in segments:
        ((pigment_distilled, stencil_distilled, real_stencil), (cut, stencil, pigment)) = segment
        imgs.append(cut)
        imgs.append(tf.concat([stencil, real_stencil, stencil], axis=3))
        imgs.append(view_single(stencil_distilled, hypers))
        imgs.append(pigment)
        imgs.append(view_single(pigment_distilled, hypers))
    return imgs

def linear_early_distill_segmenter(x_image, preprocessor, distiller, hypers):
    filters1 = hypers.get('filter_count', 32)
    og_shape = x_image.get_shape().as_list()
    resblock_count = hypers.get('resblock_count', 2)
    segment_count = hypers.get('segment_count', 4)

    features = preprocessor(x_image, hypers)
    segments = []

    # since the layers are just protos, they can be defined out here
    stencil_layers = (
       [prim.resid_path([prim.bottle_conv_proto((3, 3, filters1), 'stencil_proc' + L)])
       for L in string.ascii_uppercase[:resblock_count]] 
        + [prim.conv_proto((1, 1, 1), "cut", act=prim.almost_sigmoid, strat='act_only')]
       )

    for i in range(segment_count):
        with tf.variable_scope('segment_' + str(i)):
            real_stencil = prim.chain_layers(features, stencil_layers)
            w_stencil = tf.concat([features, real_stencil], 3, name='stencil_info')
            #TODO here we have two distillers, while synthesizing uses layers chained twice
            pigment_distilled = distiller(w_stencil, 'pigment', hypers)
            stencil_distilled = distiller(real_stencil, 'stencil', hypers)
        segments.append((pigment_distilled, stencil_distilled, real_stencil))

    return segments

def linear_grader(segment, hypers):
    ((pigment_distilled, stencil_distilled, real_stencil), (cut, stencil, pigment)) = segment
    # using abs diff loss so that squares and batches don't add noise
    # since both the stencils and the mean should be near 1/n already, I don't
    # think I need to multiply the loss
    inequality = prim.abs_diff_loss(tf.reduce_mean(stencil), 1.0 / hypers['segment_count'])
    return hypers['division_factor'], inequality

#distiller
def view_single(distill, hypers):
    vis = tf.tile(distill[:, :, tf.newaxis, tf.newaxis], [1, 1, 1, 3])
    return tf.image.resize_images(vis, [hypers['square_resolution'], 10], 
                                  method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

def simple_distiller(x_image, distill_name, hypers):
    filters1 = hypers.get('filter_count', 32)
    sparse_layer = hypers.get('sparse', 3)

    distill_layers = [
        prim.conv_proto((3, 3, 6), distill_name + '_1', stride=2),
        prim.conv_proto((3, 3, 3), distill_name + '_2', stride=2),
        prim.nn_proto(sparse_layer, distill_name + '_fc', act=hypers['decision_actf'], 
                      strat='act_only'),
        ]

    return prim.chain_layers(x_image, distill_layers)

#synthesizers 
def fc_compromise_4up_synthesizer(segment, hypers):
    (pigment_distilled, stencil_distilled, _)= segment
    og_flat_shape = [hypers['square_resolution']] * 2
    sparse_layer = hypers.get('sparse', 3)

    # fully connected layer 1 deep with a residual tiling layer
    down_shape = [-1, og_flat_shape[0] // 4, og_flat_shape[1] // 4, 1]
    down_count = np.prod(down_shape[1:])

    synth_layer_gen = lambda n: [
        lambda x: tf.concat(
            [tf.tile(x[:, tf.newaxis, tf.newaxis, :], [1] + down_shape[1:3] + [1]),
             prim.chain_layers(x, [
                prim.nn_proto(down_count, 'mixer'),
                lambda x: tf.reshape(x, down_shape),
                 ]),
             ], axis=3), #concat along filter dimension
        prim.convT_proto((3, 3, 6), 'up1', stride=2),
        prim.convT_proto((3, 3, n), 'up2', stride=2, 
                         act=tf.sigmoid, strat='act_only'),
        ]

    with tf.variable_scope('s'):
        stencil = prim.chain_layers(stencil_distilled, synth_layer_gen(1))
    with tf.variable_scope('p'):
        pigment = prim.chain_layers(pigment_distilled, synth_layer_gen(3))

    #cut = tf.multiply(pigment, stencil, name='restencil_synth')
    cut = tf.minimum(pigment, stencil, name='restencil_synth')

    return cut, stencil, pigment 

# preprocessors

def u_preprocess(x_image, hypers):
    filters1 = hypers.get('filter_count', 32)
    u_levels = hypers.get('u_levels', 3)
    og_shape = x_image.get_shape().as_list()
    resblock_count = hypers.get('resblock_count', 2)

    L = lambda i: string.ascii_uppercase[i]
    F = lambda i: (filters1 * 2 ** (i+1))
    down_f = lambda x, i: prim.conv_proto((3, 3, F(i)), 'downS' + L(i), stride=2)(x)
    enc_f = lambda x, i: prim.chain_layers(x, [
        prim.resid_path([prim.bottle_conv_proto((3, 3, F(i)), 'process' + L(i) + L2)])
        for L2 in string.ascii_lowercase[:resblock_count]
        ])
    dec_f = lambda x, i, s: tf.identity(x)
    up_f = lambda x, i: prim.convT_proto((3, 3, F(i - 1)), "upS" + L(i), stride=2)(x)
    reduce_f = lambda x: None

    features = prim.chain_layers(x_image, [
        prim.conv_proto((3, 3, filters1), "filter_up", strat='act_only'),
        tissue.u_proto(down_f, up_f, enc_f, dec_f, reduce_f, hypers),
        ])
    return features