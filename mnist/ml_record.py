import json
import hashlib

class MlRecord:
    """Hold the settings for one run"""
    def __init__(self):
        self.settings = { }

    def add_function(self, func_key, func_val):
        self.settings[func_key] = func_val.__name__

    def add_value(self, key, value):
        self.settings[key] = value

    def __hash__(self):
        return hash(self.settings)

    def contents(self):
        return json.dumps(self.settings)

    def digest(self):
        contents = self.contents()
        hasher = hashlib.shake_128()
        hasher.update(contents.encode())
        cont_hash = hasher.hexdigest(4)
        return cont_hash

    def export(self):
        digest = self.digest()
        fname = "output/record_{}.json".format(digest)
        with open(fname, 'w') as f:
            f.write(self.contents())

