import tensorflow as tf
import numpy as np
import skimage.io
import matplotlib as plt
import math

def most_of_each(sess, predict, test_feed, images, labels):
    img_shape = (28, 28)
    look_alikes = np.zeros((280, 280))
    end_y = sess.run(predict, feed_dict = test_feed) #TODO batch
    normed_predictions = np.linalg.norm(end_y, axis=1, keepdims=True) 
    end_y /= normed_predictions
    correct_predictions = np.argmax(labels,axis=1)
    for actual in range(10):
        is_labled = correct_predictions == actual
        labled_as = np.argwhere(is_labled) 
        for looks_like in range(10):
            most_of_labled = np.argmax(end_y[labled_as, looks_like])
            real_most = labled_as[most_of_labled]
            tlx = looks_like*img_shape[0]
            tly = actual*img_shape[1]
            most_image = images[real_most]
            if not np.argmax(end_y[real_most]) == actual:
                most_image = 1.0 - most_image
            look_alikes[tlx:tlx + img_shape[0], 
                        tly:tly + img_shape[1]] = np.reshape(most_image, img_shape)

    skimage.io.imsave("look_alikes.png", look_alikes)

def plot_accuracy(acc_progress):
    print(acc_progress[::(len(acc_progress) // 30 + 1)])
    fig, axes = plt.subplots(2, 2)
    axes[0, 0].set_title("test accuracy")
    axes[0, 0].plot(acc_progress)
    plt.show()

def as_square(x):
    total = np.size(x)
    side = int(math.ceil(math.sqrt(total)))
    y = np.zeros(side * side)
    y[:total] = x.flatten()
    return y.reshape((side, side))