import tensorflow as tf

def remake_log_dir(log_dir):
    if tf.gfile.Exists(log_dir):
        tf.gfile.DeleteRecursively(log_dir)
    tf.gfile.MakeDirs(log_dir)    
  