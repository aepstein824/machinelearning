import primitives as prim
import basictraining as bt
import mnist_common
import evaluation as ev
import ml_record

import tensorflow as tf
import numpy as np
import skimage.io

class Autoencoder:
    def __init__(self, model_generator, loss_function, x, hypers):
        self.model_generator = model_generator
        with tf.variable_scope("auto_scope") as self.auto_scope:
            self.enc, self.dec, self.kdp, self.kdi, self.bn_training = model_generator(x, hypers)
        self.pool_drop = hypers.get('pool_drop', Autoencoder.default_pool_drop)
        self.input_drop = hypers.get('input_drop', Autoencoder.default_input_drop)
        
        self.encoding_loss = loss_function(self.dec, x)
        l2_factor = hypers.get('l2_factor', Autoencoder.default_l2_factor)
        self.regular_loss = l2_factor * prim.l2_reg(self.auto_scope.trainable_variables())

    def training_vars(self):
        return self.auto_scope.trainable_variables()

    def persistent_vars(self):
        # TODO can this pick up variables that don't belong?
        extras = tf.train.ExponentialMovingAverage(0.0).variables_to_restore().values()
        auto_vars = self.auto_scope.trainable_variables()
        auto_save_vars = list(set(auto_vars + list(extras)))
        return auto_save_vars

    def train_dict_base(self):
        return {self.kdp:self.pool_drop, self.kdi:self.input_drop, self.bn_training:True}

    def val_dict_base(self):
        return {self.kdp:1.0, self.kdi:1.0, self.bn_training:False}

    def out(self):
        return self.dec

    def comparison_loss(self):
        return self.encoding_loss

    def training_loss(self):
        return self.encoding_loss + self.regular_loss

    default_pool_drop = 0.5
    default_input_drop = 0.95
    default_l2_factor = 0.001

def conv_auto_encoder(x, hypers):
    return Autoencoder(mnist_common.conv_auto_bn, prim.difference_loss, x, hypers)

def fc_auto_encoder(x, hypers):
    return Autoencoder(mnist_common.basic_auto, prim.difference_loss, x, hypers)