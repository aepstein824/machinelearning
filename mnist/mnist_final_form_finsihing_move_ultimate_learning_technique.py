import tensorflow as tf
import numpy as np
import skimage.io

import primitives as prim
import basictraining as bt
import mnist_common
import evaluation as ev
import ml_record
import autoencoding as ae

import argparse
import sys 
import random
import time
import os
import logging

from tensorflow.examples.tutorials.mnist import input_data

should_load = False 
should_summarize = False 
board_form = "tensorboard/run{}"

FLAGS = None
def experiment(hypers):
    recorder = ml_record.MlRecord()
    recorder.add_value("hypers", hypers)

    x = tf.placeholder(tf.float32, [None, 784])

    problem = ae.fc_auto_encoder(x, hypers)

    train_step = tf.train.AdamOptimizer().minimize(problem.training_loss(), 
                                                   var_list=(problem.training_vars(),))
    if should_summarize:
        tf.summary.scalar('encoding_loss', problem.comparison_loss())
        tf.summary.scalar('regular_loss', problem.training_loss())

    # graph configured

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.95)
    config=tf.ConfigProto(log_device_placement=False, gpu_options=gpu_options)
    sess = tf.Session(config=config)

    # session configured

    record_digest = recorder.digest()
    logger = logging.getLogger(record_digest)
    logger.setLevel("DEBUG")
    log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s', "%Y-%m-%d %H:%M:%S")
    file_handler = logging.FileHandler('logs/{}.txt'.format(record_digest))
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)
    logger.debug(recorder.contents())
    auto_saver = tf.train.Saver(problem.persistent_vars())
    auto_save_filename = os.path.join(os.getcwd(), "checkpoints/auto_{}".format(record_digest))

    board_dir = board_form.format(record_digest)
    os.makedirs(board_dir, exist_ok=True)
    if should_summarize:
        board_writer = tf.summary.FileWriter(board_dir, sess.graph)
        for v in auto_vars:
            prim.variable_summaries(v)
        merged_summaries = tf.summary.merge_all()

    # load as late as possible to catch errors
    mnist = input_data.read_data_sets(hypers["data_dir"], one_hot=True)
    val_feed = problem.val_dict_base()
    val_feed[x] = mnist.validation.images
    val_count = len(val_feed[x])
    acc_progress = []

    if should_load:
        logger.debug("Loading")
        auto_saver.restore(sess, auto_save_filename)
    else:
        logger.debug("Training")
        sess.run(tf.global_variables_initializer())
        start_time = time.time()
        for i in range(hypers["total_iter"]):
            # train
            batch_xs, batch_ys = mnist.train.next_batch(hypers["batch_size"])
            batch_feed = problem.train_dict_base()
            batch_feed[x] = batch_xs
            sess.run(train_step, feed_dict=batch_feed)
            #summarize
            if i == 100:
                prim.log_estimate_time(start_time, hypers["total_iter"] / i, logger)
            if i % hypers["print_iter"] != hypers["print_iter"] - 1:
                continue
            a = sess.run(problem.comparison_loss(), feed_dict=val_feed)
            acc_progress.append(a)
            logger.debug("Iter: {:>7} Epoch: {:>5} Prog: {:>4.0%} Score: {:<9.5F}"
                  .format(i, mnist.train.epochs_completed, i / hypers["total_iter"], 
                          a))     
            if should_summarize:
                s = sess.run(merged_summaries, val_feed)
                board_writer.add_summary(s, i)

        logger.debug("Finished training")
        auto_saver.save(sess, auto_save_filename)
        if should_summarize:
            board_writer.close()

    def normalize_img(longish):
        return np.clip(np.reshape(longish, (28, 28)), -1, 1)
    pairs = []
    for i, ind in enumerate(np.random.choice(mnist.validation.images.shape[0], 20)):
        choice = mnist.validation.images[ind]
        view_feed = dict(val_feed)
        view_feed[x] = np.reshape(choice, (1, 784))
        compressed, img = sess.run((problem.enc, problem.dec), feed_dict=view_feed)
        compressed_scale = np.max(compressed)
        if compressed_scale > 1 or compressed_scale < -1:
           compressed /= compressed_scale 
        compressed = ev.as_square(compressed)
        img = ev.as_square(img)
        img[: compressed.shape[0], : compressed.shape[1]] = compressed
        pairs.append([normalize_img(choice), normalize_img(img)])
    comparison = np.block(pairs)
    skimage.io.imsave("output/{}_autocheck_{}.png".format(hypers["data_dir"], record_digest), 
                      comparison)
    file_handler.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='mnist')
    parser.add_argument('--total_iter', type=int, default=400)
    parser.add_argument('--print_iter', type=int, default=10)
    parser.add_argument('--learning_rate', type=float, default=1e-3)
    parser.add_argument('--l2_factor', type=float, default=1e-10)
    parser.add_argument('--kdrop_input', type=float, default=0.90)
    parser.add_argument('--kdrop_pool', type=float, default=0.50)
    parser.add_argument('--batch_size', type=int, default=256)

    FLAGS, unparsed = parser.parse_known_args()
    hypers = vars(FLAGS)

    if should_load:
        should_summarize = False

    exp_graph = tf.Graph()
    with exp_graph.as_default():
        experiment(hypers)
