import tensorflow as tf
import matplotlib.pyplot as plt

import argparse
import sys 

from tensorflow.examples.tutorials.mnist import input_data

def variable_summaries(tvar):
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(tvar)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stdev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(tvar - mean)))
            tf.summary.scalar('stddev', stddev)
        tf.summary.histogram('histogram', tvar)

# breaks symmetry 
def noisy_weight(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

# relu benefits from a bit o
def biased_bias(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def nn_layer(input_tensor, input_dim, output_dim, layer_name, act=tf.nn.relu):
    with tf.name_scope(layer_name):
        with tf.name_scope('weights'):
            weights = noisy_weight((input_dim, output_dim))
            variable_summaries(weights)
        with tf.name_scope('biases'):
            biases = biased_bias((output_dim,))
            variable_summaries(biases)
        with tf.name_scope('Wx_plus_b'):
            weighted_sum = tf.matmul(input_tensor, weights) + biases
            with tf.name_scope('summaries'):
                tf.summary.histogram('preactivated_weighted_sum', weighted_sum)
        activations = act(weighted_sum, name='activation')
        with tf.name_scope('summaries'):
            tf.summary.histogram('activations', activations)
    return activations

FLAGS = None
def main (_):
    if tf.gfile.Exists(FLAGS.log_dir):
        tf.gfile.DeleteRecursively(FLAGS.log_dir)
    tf.gfile.MakeDirs(FLAGS.log_dir)

    mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

    in_size = 784
    out_size = 10

    dims = []
    dims.append(in_size)
    for i in range(FLAGS.hidden_count - 1):
        dims.append(FLAGS.hidden_size)
    dims.append(out_size)

    k_drop = tf.placeholder(tf.float32)
    x = tf.placeholder(tf.float32, [None, 784])
    prev_layer_tensor = x
    # I think the first is actually the input layer and the last is the output layer, but I'll 
    #  just call them all hidden for now
    for i in range(FLAGS.hidden_count):
        hidden = nn_layer(prev_layer_tensor, dims[i], dims[i + 1], 'hidden' + str(i), tf.nn.relu)
        if not i == 0 and not i == FLAGS.hidden_count:
            prev_layer_tensor = tf.nn.dropout(hidden, k_drop, name=('dropout'+str(i)))
        else:
            prev_layer_tensor = hidden 
    y = prev_layer_tensor
    y_ = tf.placeholder(tf.float32, [None, 10])

    with tf.name_scope('cross_entropy'):
        diff =  tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y)
        cross_entropy = tf.reduce_mean(diff)
        tf.summary.scalar('cross_entropy_summary', cross_entropy)
    with tf.name_scope('loss_function'):
        loss_function = cross_entropy 
        all_trainables = tf.trainable_variables()
        for trainable in all_trainables:
            loss_function += FLAGS.l2_factor * tf.nn.l2_loss(trainable)
        tf.summary.scalar('loss_summary', loss_function)

    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(loss_function)

    sess = tf.InteractiveSession(config=tf.ConfigProto(log_device_placement=False))

    # correct_prediction is the accuracy expression 
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.summary.scalar('accuracy', accuracy)
    acc_progress = []

    merged_summaries = tf.summary.merge_all()
    test_writer = tf.summary.FileWriter(FLAGS.log_dir + '/test', sess.graph)

    tf.global_variables_initializer().run()
    for i in range(FLAGS.total_iter):
        batch_xs, batch_ys = mnist.train.next_batch(FLAGS.batch_size)
        sess.run(train_step, feed_dict={x:batch_xs, y_:batch_ys, k_drop:FLAGS.dropout_factor})
        if i % FLAGS.print_iter == 0:
            s, a = sess.run([merged_summaries, accuracy], 
                feed_dict={x: mnist.test.images, y_:mnist.test.labels, k_drop:1.0})
            acc_progress.append(a)
            print("Iter: {} Prog: {:>2} Score: {}".format(i, i / FLAGS.total_iter, a))
            test_writer.add_summary(s, i)
            
    test_writer.close()

    print(acc_progress)
    plt.plot(acc_progress)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='mnist/input_data')
    parser.add_argument('--total_iter', type=int, default=3000)
    parser.add_argument('--print_iter', type=int, default=100)
    parser.add_argument('--hidden_size', type=int, default=16)
    parser.add_argument('--hidden_count', type=int, default=3)
    parser.add_argument('--learning_rate', type=float, default=0.01)
    parser.add_argument('--l2_factor', type=float, default=0.001)
    parser.add_argument('--dropout_factor', type=float, default=0.5)
    parser.add_argument('--batch_size', type=int, default=512)
    parser.add_argument('--log_dir', type=str, default='logs')

    FLAGS, unparsed = parser.parse_known_args()

    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)