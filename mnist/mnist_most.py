import primitives as prim
import basictraining as bt
import mnist_common
import evaluation as ev

import tensorflow as tf
import numpy as np
import skimage.io

import argparse
import sys 
import random
import time
import os

def main (_):
    x = prim.noisy_weight((784,))
    with tf.variable_scope("model_scope") as model_scope:
        y, kdrop_input, kdrop_pool = mnist_common.fashion_allconv(x)
    
    saver = tf.train.Saver(model_scope.trainable_variables())

    blank_labels = np.zeros(10)
    blank_labels[7] = 1.0
    label0 = tf.constant(blank_labels)
    best0Loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=label0, logits=y)
    train_step = tf.train.AdamOptimizer().minimize(best0Loss, var_list=(x,))

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.95)
    config=tf.ConfigProto(log_device_placement=False, gpu_options=gpu_options)
    
    sess = tf.Session(config=config)
    sess.run(tf.global_variables_initializer())
    test_feed = {kdrop_pool:1.0, kdrop_input:1.0}

    save_filename = os.path.join(os.getcwd(), "checkpoints/classifier")
    saver.restore(sess, save_filename)

    for i in range(100000):
        if i % 100 == 0:
            print("At {} percent, y is {}".format(i, sess.run(y, feed_dict=test_feed)))
        sess.run(train_step, feed_dict=test_feed)

    xvalue = sess.run(x)
    skimage.io.imsave("most1.png", np.reshape(xvalue, (28, 28)))

if __name__ == '__main__':
    tf.app.run(main=main)
