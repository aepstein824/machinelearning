import primitives as prim
import basictraining as bt
import mnist_common
import evaluation as ev
import ml_record

import tensorflow as tf
import numpy as np
import skimage.io

import argparse
import sys 
import random
import time
import os

from tensorflow.examples.tutorials.mnist import input_data

should_load = False 
log_form = "tensorboard/run{}"
FLAGS = None
def main (_):
    recorder = ml_record.MlRecord(FLAGS)

    in_size = 784
    out_size = 10

    model_func = mnist_common.fashion_allconv_bn
    x = tf.placeholder(tf.float32, [None, 784])
    with tf.variable_scope("model_scope") as model_scope:
        y, kdrop_input, kdrop_pool, in_training = model_func(x)
    y_ = tf.placeholder(tf.float32, [None, 10])
    all_trainables = tf.trainable_variables()
    loss_func = prim.classification_loss
    class_loss = loss_func(y, y_, all_trainables, FLAGS.l2_factor)

    recorder.add_function("model_func", model_func)
    recorder.add_function("loss_func", loss_func)
    record_digest = recorder.digest()

    saver = tf.train.Saver(model_scope.trainable_variables())

    real_count_vars = lambda vlist: np.sum([np.prod(v.shape) for v in vlist])
    trainable_count = real_count_vars(tf.trainable_variables())

    train_step = tf.train.AdamOptimizer(FLAGS.learning_rate).minimize(class_loss)
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.95)
    config=tf.ConfigProto(log_device_placement=False, gpu_options=gpu_options)
    log_dir = log_form.format(record_digest)
    os.makedirs(log_dir, exist_ok=True)
    sess = tf.Session(config=config)
    test_writer = tf.summary.FileWriter(log_dir, sess.graph)
    mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)
    # correct_prediction is the accuracy expression 
    model_predict = tf.argmax(y, 1)
    correct_predict = tf.argmax(y_, 1)
    compare_predict = tf.equal(model_predict, correct_predict)
    accuracy = tf.reduce_mean(tf.cast(compare_predict, tf.float32))
    tf.summary.scalar('accuracy', accuracy)
    acc_progress = []
    merged_summaries = tf.summary.merge_all()

    test_feed = {x: mnist.test.images, y_:mnist.test.labels, 
                 kdrop_pool:1.0, kdrop_input:1.0, in_training:True} 

    save_filename = os.path.join(os.getcwd(), "checkpoints/classifier_{}".format(record_digest))

    if should_load:
        print("Loading {}".format(record_digest))
        saver.restore(sess, save_filename)
    else:
        #begin training
        print("Training {}".format(record_digest))
        sess.run(tf.global_variables_initializer())
        start_time = time.time()
        print("Starting Training...")
        for i in range(FLAGS.total_iter):
            batch_xs, batch_ys = mnist.train.next_batch(FLAGS.batch_size)
            feed_dict ={x:batch_xs, y_:batch_ys, kdrop_pool:FLAGS.kdrop_pool, 
                        kdrop_input:FLAGS.kdrop_input, in_training:True} 
            sess.run(train_step, feed_dict=feed_dict)
            if i % FLAGS.print_iter == FLAGS.print_iter - 1:
                subset_inds = np.random.randint(0, mnist.test.images.shape[0], 1000)
                subset_images = mnist.test.images[subset_inds]
                subset_labels = mnist.test.labels[subset_inds]
                batch_dict={x: subset_images, y_: subset_labels, 
                            kdrop_pool:1.0, kdrop_input:1.0, in_training:False}
                a = sess.run(accuracy, feed_dict=batch_dict)
                acc_progress.append(a)
                print("Iter: {:>7} Epoch: {:>5} Prog: {:>4.0%} Score: {:>6.2%}"
                      .format(i, mnist.train.epochs_completed, i / FLAGS.total_iter, a))            
                '''
                s = sess.run(merged_summaries, 
                             feed_dict={x: subset_images, y_: subset_labels, 
                                        kdrop_pool:1.0, kdrop_input:1.0})
                test_writer.add_summary(s, i)
                '''
                if i == FLAGS.print_iter - 1:
                    elapsed_time = time.time() - start_time 
                    total_expected = elapsed_time * (FLAGS.total_iter / FLAGS.print_iter)
                    print("Estimated time: ", 
                          time.asctime(time.localtime(time.time() + total_expected)))
        test_writer.close()
        saver.save(sess, save_filename)
        #end training

    print ("Trainable Count:{}".format(trainable_count))
    print ("Final accuracy:{}".format(sess.run(accuracy, feed_dict=test_feed)))
    recorder.export()


    # ev.most_of_each(sess, y, test_feed, mnist.test.images, mnist.test.labels)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='fashion')
    parser.add_argument('--total_iter', type=int, default=38500)
    parser.add_argument('--print_iter', type=int, default=1000)
    parser.add_argument('--learning_rate', type=float, default=1e-5)
    parser.add_argument('--l2_factor', type=float, default=1e-4) # 1e-4
    parser.add_argument('--kdrop_input', type=float, default=0.80)
    parser.add_argument('--kdrop_pool', type=float, default=0.50)
    parser.add_argument('--batch_size', type=int, default=50)

    FLAGS, unparsed = parser.parse_known_args()

    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)