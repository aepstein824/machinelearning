import primitives as prim
import basictraining as bt
import mnist_common
import evaluation as ev
import ml_record

import tensorflow as tf
import numpy as np
import skimage.io

import argparse
import sys 
import random
import time
import os
import logging

from tensorflow.examples.tutorials.mnist import input_data

should_load = False 
should_summarize = False 
board_form = "tensorboard/run{}"

FLAGS = None
def experiment(flags, params):
    recorder = ml_record.MlRecord(flags)
    recorder.add_value("params", params)

    x = tf.placeholder(tf.float32, [None, 784])

    model_func = mnist_common.basic_auto_bn
    with tf.variable_scope("auto_scope") as auto_scope:
        enc, dec, kdi_auto, kdp_auto, in_training = model_func(x, params)
    extras = tf.train.ExponentialMovingAverage(0.0).variables_to_restore().values()
    auto_vars = auto_scope.trainable_variables()
    auto_save_vars = list(set(auto_vars + list(extras)))

    #with tf.variable_scope("classifier_scope") as classifier_scope:
    #    y, kdrop_input, kdrop_pool = mnist_common.fashion_allconv(dec)    

    #class_saver = tf.train.Saver(classifier_scope.trainable_variables())
    #class_save_filename = os.path.join(os.getcwd(), "checkpoints/classifier")
    #class_saver.restore(sess, class_save_filename)

    loss_func = prim.abs_diff_loss
    encoding_loss = loss_func(dec, x)
    regular_loss = sum(flags.l2_factor * tf.nn.l2_loss(t) for t in auto_vars)
    train_loss = encoding_loss + regular_loss
    train_step = tf.train.AdamOptimizer().minimize(train_loss, var_list=(auto_vars,))
    if should_summarize:
        tf.summary.scalar('encoding_loss',encoding_loss)
        tf.summary.scalar('regular_loss', regular_loss)

    # graph configured

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.95)
    config=tf.ConfigProto(log_device_placement=True, gpu_options=gpu_options)
    sess = tf.Session(config=config)


    # session configured

    recorder.add_function("model_func", model_func)
    recorder.add_function("loss_func", loss_func)
    record_digest = recorder.digest()
    logger = logging.getLogger(record_digest)
    logger.setLevel("DEBUG")
    log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s', "%Y-%m-%d %H:%M:%S")
    file_handler = logging.FileHandler('logs/{}.txt'.format(record_digest))
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(log_formatter)
    logger.addHandler(file_handler)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(log_formatter)
    logger.addHandler(console_handler)
    logger.debug(recorder.contents())
    auto_saver = tf.train.Saver(auto_save_vars)
    auto_save_filename = os.path.join(os.getcwd(), "checkpoints/auto_{}".format(record_digest))

    board_dir = board_form.format(record_digest)
    os.makedirs(board_dir, exist_ok=True)
    if should_summarize:
        board_writer = tf.summary.FileWriter(board_dir, sess.graph)
        for v in auto_vars:
            prim.variable_summaries(v)
        merged_summaries = tf.summary.merge_all()

    # load as late as possible to catch errors
    mnist = input_data.read_data_sets(flags.data_dir, one_hot=True)
    val_feed = {x:mnist.validation.images, kdp_auto:1.0, kdi_auto:1.0, in_training:False}
    val_count = len(val_feed[x])
    test_feed = {x:mnist.test.images, kdp_auto:1.0, kdi_auto:1.0, in_training:False}
    test_count = len(test_feed[x])
    acc_progress = []

    if should_load:
        logger.debug("Loading")
        auto_saver.restore(sess, auto_save_filename)
    else:
        logger.debug("Training")
        sess.run(tf.global_variables_initializer())
        start_time = time.time()
        for i in range(flags.total_iter):
            # train
            batch_xs, batch_ys = mnist.train.next_batch(flags.batch_size)
            batch_feed={x:batch_xs, kdp_auto:flags.kdrop_pool, kdi_auto:flags.kdrop_input,
                        in_training:True} 
            sess.run(train_step, feed_dict=batch_feed)
            #summarize
            if i == 100:
                prim.log_estimate_time(start_time, flags.total_iter / i, logger)
            if i % flags.print_iter != flags.print_iter - 1:
                continue
            a = sess.run(encoding_loss, feed_dict=val_feed)
            regulation_ratio = sess.run(encoding_loss / train_loss, feed_dict=batch_feed)
            acc_progress.append(a)
            logger.debug("Iter: {:>7} Epoch: {:>5} Prog: {:>4.0%} Score: {:<9.5F}"
                  .format(i, mnist.train.epochs_completed, i / flags.total_iter, 
                          a, regulation_ratio))     
            if should_summarize:
                s = sess.run(merged_summaries, val_feed)
                board_writer.add_summary(s, i)

        logger.debug("Finished training")
        auto_saver.save(sess, auto_save_filename)
        if should_summarize:
            board_writer.close()

    def normalize_img(longish):
        return np.clip(np.reshape(longish, (28, 28)), -1, 1)
    pairs = []
    for i, ind in enumerate(np.random.choice(mnist.validation.images.shape[0], 20)):
        choice = mnist.validation.images[ind]
        compressed, img = sess.run((enc, dec), 
                            feed_dict={x:np.reshape(choice, (1, 784)), 
                                       kdp_auto:1.0, kdi_auto:1.0, in_training:False})
        compressed_scale = np.max(compressed)
        if compressed_scale > 1 or compressed_scale < -1:
           compressed /= compressed_scale 
        compressed = ev.as_square(compressed)
        img = ev.as_square(img)
        img[: compressed.shape[0], : compressed.shape[1]] = compressed
        pairs.append([normalize_img(choice), normalize_img(img)])
    comparison = np.block(pairs)
    skimage.io.imsave("output/{}_autocheck_{}.png".format(FLAGS.data_dir, record_digest), 
                      comparison)
    file_handler.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='fashion')
    parser.add_argument('--total_iter', type=int, default=400)
    parser.add_argument('--print_iter', type=int, default=10)
    parser.add_argument('--learning_rate', type=float, default=1e-3)
    parser.add_argument('--l2_factor', type=float, default=1e-10)
    parser.add_argument('--kdrop_input', type=float, default=0.90)
    parser.add_argument('--kdrop_pool', type=float, default=0.50)
    parser.add_argument('--batch_size', type=int, default=256)

    FLAGS, unparsed = parser.parse_known_args()

    if should_load:
        should_summarize = False

    exp_graph = tf.Graph()
    with exp_graph.as_default():
        experiment(FLAGS, params)
