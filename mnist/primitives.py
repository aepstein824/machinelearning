import tensorflow as tf
import matplotlib.pyplot as plt
import time
import logging

# breaks symmetry 
def noisy_weight(shape, stddev=0.01):
    initial = tf.truncated_normal(shape, stddev=stddev)
    return tf.Variable(initial)

# relu benefits from a bit o
def biased_bias(shape, constant=0.1):
    initial = tf.constant(constant, shape=shape)
    return tf.Variable(initial)

def variable_summaries(tvar):
    fixed_var_name = "".join([c if c.isalnum() else "_" for c in tvar.name])
    with tf.name_scope('{}'.format(fixed_var_name)):
        mean = tf.reduce_mean(tvar)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stdev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(tvar - mean)))
            tf.summary.scalar('stddev', stddev)
        tf.summary.histogram('histogram', tvar)
        tf.summary.tensor_summary('tensor', tvar)

def batch_norm(x, in_training):
    dim = x.get_shape()[-1]
    with tf.variable_scope('bn'):
        beta = tf.Variable(tf.constant(0, shape=[dim], dtype=tf.float32), name='beta')
        gamma = tf.Variable(tf.constant(1, shape=[dim], dtype=tf.float32), name='gamma')
        axes = list(range(len(x.get_shape()) - 1))
        bn_mean, bn_var = tf.nn.moments(x, axes, name='moments')
        with tf.variable_scope('moving_average'):
            ema = tf.train.ExponentialMovingAverage(decay=0.99)
        def mean_var_with_update():
            ema_apply_op = ema.apply([bn_mean, bn_var])
            with tf.control_dependencies([ema_apply_op]):
                return tf.identity(bn_mean), tf.identity(bn_var)
        use_pop_stats_func = lambda: (ema.average(bn_mean), ema.average(bn_var))
        use_mean, use_var = tf.cond(in_training,
                                    mean_var_with_update,
                                    use_pop_stats_func)
        bn_normed = tf.nn.batch_normalization(x, use_mean, use_var, beta, gamma, 1e-5)
    return bn_normed

def chain_layers(input_tensor, layers):
    output_tensor = input_tensor
    for l in layers:
        if not callable(l):
            output_tensor = batch_norm(output_tensor, l)
        else:
            output_tensor = l(output_tensor)
    return output_tensor 

def nn_proto(input_dim, output_dim, layer_name, act=tf.nn.relu):
    def nn_layer(input_tensor):
        with tf.name_scope(layer_name):
            with tf.name_scope('weights'):
                weights = noisy_weight((input_dim, output_dim))
            with tf.name_scope('biases'):
                biases = biased_bias((output_dim,))
            with tf.name_scope('Wx_plus_b'):
                weighted_sum = tf.matmul(input_tensor, weights) + biases
            activations = act(weighted_sum, name='activation')
        return activations
    return (lambda x: nn_layer(x))

def conv_proto(dims, layer_name, stride=1, act=tf.nn.relu):
    def conv_layer(input_tensor):
        cw, ch, channels, filters = dims 
        with tf.name_scope(layer_name):
            with tf.name_scope('weights'):
                weights = noisy_weight(dims)
            with tf.name_scope('biases'):
                biases = biased_bias((filters,))
            with tf.name_scope('convolution'):
                convolved = (tf.nn.conv2d(input_tensor, weights, 
                                          strides=(1, stride, stride, 1), padding='SAME') 
                             + biases)
            activations = act(convolved, name='activation')
        return activations
    return (lambda x: conv_layer(x))

def convT_proto(dims, layer_name, stride=1, act=tf.nn.relu):
    def convT_layer(input_tensor):
        cw, ch, channels, filters = dims 
        #the output shape could technically be more smaller than this calculated value
        input_shape = tf.shape(input_tensor)
        output_shape = tf.stack((input_shape[0], input_shape[1] * stride, 
                                input_shape[2] * stride, filters))
        with tf.name_scope(layer_name):
            with tf.name_scope('weights'):
                weights = noisy_weight((cw, ch, filters, channels))
            with tf.name_scope('biases'):
                biases = biased_bias((filters,))
            with tf.name_scope('convolution'):
                convolved = (tf.nn.conv2d_transpose(input_tensor, weights, output_shape,
                                          strides=(1, stride, stride, 1), padding='SAME') 
                             + biases)
            activations = act(convolved, name='activation')
        return activations
    return (lambda x: convT_layer(x))       

def maxpool_proto(layer_name):
    def maxpool_layer(input_tensor):
        with tf.name_scope(layer_name):
            poolStrides = (1, 2, 2, 1)
            pooled = tf.nn.max_pool(input_tensor, ksize=poolStrides, 
                                    strides=poolStrides, padding='SAME') 
        return pooled 
    return (lambda x:maxpool_layer(x))

def avgpool_proto(width, layer_name):
    def avgpool_layer(input_tensor):
        with tf.name_scope(layer_name):
            poolKernel = (1, width, width, 1)
            pooled = tf.nn.avg_pool(input_tensor, ksize=poolKernel,
                                    strides=(1, 1, 1, 1), padding='VALID')
            with tf.name_scope('summaries'):
                tf.summary.histogram('pooled', pooled)
        return pooled
    return (lambda x:avgpool_layer(x))

def l2_reg(var_list):
    return sum(1 / len(var_list) * tf.reduce_mean(t ** 2) for t in var_list)

def classification_loss(net_out, real_out):
    with tf.name_scope('cross_entropy'):
        diff =  tf.nn.softmax_cross_entropy_with_logits(labels=real_out, logits=net_out)
        cross_entropy = tf.reduce_mean(diff)
    return cross_entropy

def difference_loss(correct, decoded):
    return tf.sqrt(tf.reduce_mean(tf.squared_difference(correct, decoded)))

def abs_diff_loss(correct, decoded):
    return tf.reduce_mean(tf.abs(correct - decoded))


def log_estimate_time(start_time, ratio, logger):
    elapsed_time = time.time() - start_time 
    total_expected = elapsed_time * ratio
    logger.debug("Estimated Finish time: " +
          time.asctime(time.localtime(time.time() + total_expected)))
